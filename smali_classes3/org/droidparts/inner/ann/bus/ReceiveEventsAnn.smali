.class public final Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;
.super Lorg/droidparts/inner/ann/Ann;
.source "ReceiveEventsAnn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/droidparts/inner/ann/Ann",
        "<",
        "Lorg/droidparts/annotation/bus/ReceiveEvents;",
        ">;"
    }
.end annotation


# instance fields
.field public final names:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/droidparts/annotation/bus/ReceiveEvents;)V
    .locals 4
    .param p1, "annotation"    # Lorg/droidparts/annotation/bus/ReceiveEvents;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 28
    invoke-direct {p0, p1}, Lorg/droidparts/inner/ann/Ann;-><init>(Ljava/lang/annotation/Annotation;)V

    .line 30
    invoke-virtual {p0}, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;->hackSuccess()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    const-string v2, "name"

    invoke-virtual {p0, v2}, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;->getElement(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object v0, v2

    check-cast v0, [Ljava/lang/String;

    .line 32
    .local v0, "names":[Ljava/lang/String;
    invoke-virtual {p0}, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;->cleanup()V

    .line 36
    :goto_0
    array-length v2, v0

    if-ne v2, v1, :cond_1

    aget-object v2, v0, v3

    invoke-static {v2}, Lorg/droidparts/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    .local v1, "none":Z
    :goto_1
    if-eqz v1, :cond_2

    .line 38
    new-array v2, v3, [Ljava/lang/String;

    iput-object v2, p0, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;->names:[Ljava/lang/String;

    .line 42
    :goto_2
    return-void

    .line 34
    .end local v0    # "names":[Ljava/lang/String;
    .end local v1    # "none":Z
    :cond_0
    invoke-interface {p1}, Lorg/droidparts/annotation/bus/ReceiveEvents;->name()[Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "names":[Ljava/lang/String;
    goto :goto_0

    :cond_1
    move v1, v3

    .line 36
    goto :goto_1

    .line 40
    .restart local v1    # "none":Z
    :cond_2
    iput-object v0, p0, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;->names:[Ljava/lang/String;

    goto :goto_2
.end method
