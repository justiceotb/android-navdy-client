.class public Lorg/droidparts/inner/fragments/SecretFragmentsStockUtil;
.super Lorg/droidparts/inner/fragments/SecretFragmentsUtil;
.source "SecretFragmentsStockUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/droidparts/inner/fragments/SecretFragmentsUtil;-><init>()V

    return-void
.end method

.method public static dialogFragmentShowDialogFragment(Landroid/app/Activity;Landroid/app/DialogFragment;)V
    .locals 5
    .param p0, "fragmentActivity"    # Landroid/app/Activity;
    .param p1, "dialogFragment"    # Landroid/app/DialogFragment;

    .prologue
    .line 50
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 51
    .local v1, "fm":Landroid/app/FragmentManager;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    .line 52
    .local v3, "tag":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 53
    .local v2, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v1, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 54
    .local v0, "f":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 57
    :cond_0
    invoke-virtual {p1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 58
    return-void
.end method

.method public static varargs fragmentActivitySetFragmentVisible(Landroid/app/Activity;Z[Landroid/app/Fragment;)V
    .locals 6
    .param p0, "fragmentActivity"    # Landroid/app/Activity;
    .param p1, "visible"    # Z
    .param p2, "fragments"    # [Landroid/app/Fragment;

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 29
    .local v1, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 30
    .local v3, "ft":Landroid/app/FragmentTransaction;
    move-object v0, p2

    .local v0, "arr$":[Landroid/app/Fragment;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, v0, v4

    .line 31
    .local v2, "fragment":Landroid/app/Fragment;
    if-eqz p1, :cond_0

    .line 32
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 30
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1

    .line 37
    .end local v2    # "fragment":Landroid/app/Fragment;
    :cond_1
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 38
    return-void
.end method

.method public static singleFragmentActivityAddFragmentToContentView(Landroid/app/Activity;Landroid/app/Fragment;)V
    .locals 3
    .param p0, "fragmentActivity"    # Landroid/app/Activity;
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 42
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 43
    .local v0, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 44
    .local v1, "ft":Landroid/app/FragmentTransaction;
    const v2, 0x22528    # 1.97E-40f

    invoke-virtual {v1, v2, p1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 45
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 46
    return-void
.end method
