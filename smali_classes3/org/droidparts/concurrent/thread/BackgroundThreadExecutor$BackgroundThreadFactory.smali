.class Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor$BackgroundThreadFactory;
.super Ljava/lang/Object;
.source "BackgroundThreadExecutor.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BackgroundThreadFactory"
.end annotation


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor$BackgroundThreadFactory;->name:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 41
    new-instance v0, Lorg/droidparts/concurrent/thread/BackgroundThread;

    iget-object v1, p0, Lorg/droidparts/concurrent/thread/BackgroundThreadExecutor$BackgroundThreadFactory;->name:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lorg/droidparts/concurrent/thread/BackgroundThread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-object v0
.end method
