.class public abstract Lorg/droidparts/concurrent/service/IntentService;
.super Landroid/app/IntentService;
.source "IntentService.java"


# static fields
.field public static final EXTRA_ACTION:Ljava/lang/String; = "__action__"

.field public static final EXTRA_EXCEPTION:Ljava/lang/String; = "__exception__"

.field private static final EXTRA_RESULT_RECEIVER:Ljava/lang/String; = "__result_receiver__"

.field public static final RESULT_FAILURE:I = 0x0

.field public static final RESULT_SUCCESS:I = -0x1

.field private static mServiceHandlerField:Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 5

    .prologue
    .line 104
    const/4 v2, 0x0

    .line 106
    .local v2, "handler":Landroid/os/Handler;
    :try_start_0
    sget-object v3, Lorg/droidparts/concurrent/service/IntentService;->mServiceHandlerField:Ljava/lang/reflect/Field;

    if-nez v3, :cond_0

    .line 107
    const-class v3, Landroid/app/IntentService;

    const-string v4, "mServiceHandler"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    sput-object v3, Lorg/droidparts/concurrent/service/IntentService;->mServiceHandlerField:Ljava/lang/reflect/Field;

    .line 109
    sget-object v3, Lorg/droidparts/concurrent/service/IntentService;->mServiceHandlerField:Ljava/lang/reflect/Field;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 111
    :cond_0
    sget-object v3, Lorg/droidparts/concurrent/service/IntentService;->mServiceHandlerField:Ljava/lang/reflect/Field;

    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/os/Handler;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-object v2

    .line 112
    :catch_0
    move-exception v1

    .line 113
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static final getIntent(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/concurrent/service/IntentService;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/concurrent/service/IntentService;>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    return-object v0
.end method

.method public static final getIntent(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;Landroid/os/ResultReceiver;)Landroid/content/Intent;
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "resultReceiver"    # Landroid/os/ResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/concurrent/service/IntentService;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/os/ResultReceiver;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/concurrent/service/IntentService;>;"
    invoke-static {p0, p1, p2}, Lorg/droidparts/concurrent/service/IntentService;->getIntent(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 53
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "__result_receiver__"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 54
    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 0

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 64
    invoke-static {p0}, Lorg/droidparts/Injector;->inject(Landroid/app/Service;)V

    .line 65
    return-void
.end method

.method protected abstract onExecute(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 69
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 71
    .local v1, "data":Landroid/os/Bundle;
    if-nez v1, :cond_0

    .line 72
    new-instance v1, Landroid/os/Bundle;

    .end local v1    # "data":Landroid/os/Bundle;
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 74
    .restart local v1    # "data":Landroid/os/Bundle;
    :cond_0
    const-string v4, "__result_receiver__"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/os/ResultReceiver;

    .line 76
    .local v3, "resultReceiver":Landroid/os/ResultReceiver;
    const-string v4, "__action__"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :try_start_0
    invoke-virtual {p0, v0, v1}, Lorg/droidparts/concurrent/service/IntentService;->onExecute(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 79
    if-eqz v3, :cond_1

    .line 80
    const/4 v4, -0x1

    invoke-virtual {v3, v4, v1}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :cond_1
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v2

    .line 83
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 84
    if-eqz v3, :cond_1

    .line 85
    const-string v4, "__exception__"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 86
    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Landroid/os/ResultReceiver;->send(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public removePendingIntents()V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Lorg/droidparts/concurrent/service/IntentService;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 93
    .local v0, "handler":Landroid/os/Handler;
    if-eqz v0, :cond_0

    .line 94
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 96
    :cond_0
    return-void
.end method
