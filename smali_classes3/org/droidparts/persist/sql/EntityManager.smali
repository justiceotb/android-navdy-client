.class public Lorg/droidparts/persist/sql/EntityManager;
.super Lorg/droidparts/persist/sql/AbstractEntityManager;
.source "EntityManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EntityType:",
        "Lorg/droidparts/model/Entity;",
        ">",
        "Lorg/droidparts/persist/sql/AbstractEntityManager",
        "<TEntityType;>;"
    }
.end annotation


# instance fields
.field private final cls:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TEntityType;>;"
        }
    .end annotation
.end field

.field private final ctx:Landroid/content/Context;

.field private final db:Landroid/database/sqlite/SQLiteDatabase;

.field private eagerForeignKeyColumnNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Landroid/content/Context;)V
    .locals 1
    .param p2, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TEntityType;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TEntityType;>;"
    const-class v0, Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p2, v0}, Lorg/droidparts/Injector;->getDependency(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, p1, p2, v0}, Lorg/droidparts/persist/sql/EntityManager;-><init>(Ljava/lang/Class;Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 52
    return-void
.end method

.method protected constructor <init>(Ljava/lang/Class;Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TEntityType;>;",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TEntityType;>;"
    invoke-direct {p0}, Lorg/droidparts/persist/sql/AbstractEntityManager;-><init>()V

    .line 56
    iput-object p1, p0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    .line 57
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/persist/sql/EntityManager;->ctx:Landroid/content/Context;

    .line 58
    iput-object p3, p0, Lorg/droidparts/persist/sql/EntityManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 59
    invoke-static {p2, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 60
    return-void
.end method

.method private subManager(Ljava/lang/Class;)Lorg/droidparts/persist/sql/EntityManager;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lorg/droidparts/persist/sql/EntityManager",
            "<",
            "Lorg/droidparts/model/Entity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p1, "entityType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Lorg/droidparts/persist/sql/EntityManager;

    iget-object v1, p0, Lorg/droidparts/persist/sql/EntityManager;->ctx:Landroid/content/Context;

    iget-object v2, p0, Lorg/droidparts/persist/sql/EntityManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, p1, v1, v2}, Lorg/droidparts/persist/sql/EntityManager;-><init>(Ljava/lang/Class;Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    return-object v0
.end method


# virtual methods
.method protected createForeignKeys(Lorg/droidparts/model/Entity;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEntityType;)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p1, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v6

    .line 169
    .local v6, "columnSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    move-object v3, v6

    .local v3, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v12, v3

    .local v12, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    move v11, v10

    .end local v3    # "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    .end local v10    # "i$":I
    .end local v12    # "len$":I
    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_7

    aget-object v14, v3, v11

    .line 170
    .local v14, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    iget-object v0, v14, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v8

    .line 171
    .local v8, "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v8}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 172
    iget-object v0, v14, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/droidparts/model/Entity;

    .line 173
    .local v9, "foreignEntity":Lorg/droidparts/model/Entity;
    if-eqz v9, :cond_0

    iget-wide v0, v9, Lorg/droidparts/model/Entity;->id:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-nez v16, :cond_0

    .line 174
    iget-object v0, v14, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/droidparts/persist/sql/EntityManager;->subManager(Ljava/lang/Class;)Lorg/droidparts/persist/sql/EntityManager;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lorg/droidparts/persist/sql/EntityManager;->create(Lorg/droidparts/model/Entity;)Z

    .line 169
    .end local v9    # "foreignEntity":Lorg/droidparts/model/Entity;
    .end local v11    # "i$":I
    :cond_0
    :goto_1
    add-int/lit8 v10, v11, 0x1

    .restart local v10    # "i$":I
    move v11, v10

    .end local v10    # "i$":I
    .restart local v11    # "i$":I
    goto :goto_0

    .line 176
    :cond_1
    invoke-static {v8}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v16

    if-nez v16, :cond_2

    invoke-static {v8}, Lorg/droidparts/inner/TypeHelper;->isCollection(Ljava/lang/Class;)Z

    move-result v16

    if-eqz v16, :cond_0

    :cond_2
    iget-object v0, v14, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 178
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v15, "toCreate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/model/Entity;>;"
    invoke-static {v8}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 180
    iget-object v0, v14, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/droidparts/model/Entity;

    .line 181
    .local v2, "arr":[Lorg/droidparts/model/Entity;
    if-eqz v2, :cond_6

    .line 182
    move-object v4, v2

    .local v4, "arr$":[Lorg/droidparts/model/Entity;
    array-length v13, v4

    .local v13, "len$":I
    const/4 v10, 0x0

    .end local v11    # "i$":I
    .restart local v10    # "i$":I
    :goto_2
    if-ge v10, v13, :cond_6

    aget-object v7, v4, v10

    .line 183
    .local v7, "ent":Lorg/droidparts/model/Entity;
    if-eqz v7, :cond_3

    iget-wide v0, v7, Lorg/droidparts/model/Entity;->id:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-nez v16, :cond_3

    .line 184
    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 189
    .end local v2    # "arr":[Lorg/droidparts/model/Entity;
    .end local v4    # "arr$":[Lorg/droidparts/model/Entity;
    .end local v7    # "ent":Lorg/droidparts/model/Entity;
    .end local v10    # "i$":I
    .end local v13    # "len$":I
    .restart local v11    # "i$":I
    :cond_4
    iget-object v0, v14, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Collection;

    .line 190
    .local v5, "coll":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/droidparts/model/Entity;>;"
    if-eqz v5, :cond_6

    .line 191
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .end local v11    # "i$":I
    .local v10, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/droidparts/model/Entity;

    .line 192
    .restart local v7    # "ent":Lorg/droidparts/model/Entity;
    if-eqz v7, :cond_5

    iget-wide v0, v7, Lorg/droidparts/model/Entity;->id:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-nez v16, :cond_5

    .line 193
    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 198
    .end local v5    # "coll":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/droidparts/model/Entity;>;"
    .end local v7    # "ent":Lorg/droidparts/model/Entity;
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_0

    .line 199
    iget-object v0, v14, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/droidparts/persist/sql/EntityManager;->subManager(Ljava/lang/Class;)Lorg/droidparts/persist/sql/EntityManager;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lorg/droidparts/persist/sql/EntityManager;->create(Ljava/util/Collection;)I

    goto/16 :goto_1

    .line 204
    .end local v8    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v14    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    .end local v15    # "toCreate":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/model/Entity;>;"
    .restart local v11    # "i$":I
    :cond_7
    return-void
.end method

.method protected fillEagerForeignKeys(Lorg/droidparts/model/Entity;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEntityType;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p1, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    invoke-virtual {p0}, Lorg/droidparts/persist/sql/EntityManager;->getEagerForeignKeyColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "eagerColumnNames":[Ljava/lang/String;
    array-length v1, v0

    if-eqz v1, :cond_0

    .line 210
    invoke-virtual {p0, p1, v0}, Lorg/droidparts/persist/sql/EntityManager;->fillForeignKeys(Lorg/droidparts/model/Entity;[Ljava/lang/String;)V

    .line 212
    :cond_0
    return-void
.end method

.method public varargs fillForeignKeys(Lorg/droidparts/model/Entity;[Ljava/lang/String;)V
    .locals 24
    .param p2, "columnNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEntityType;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p1, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    new-instance v7, Ljava/util/HashSet;

    invoke-static/range {p2 .. p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 91
    .local v7, "columnNameSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/util/HashSet;->isEmpty()Z

    move-result v12

    .line 92
    .local v12, "fillAll":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v8

    .line 94
    .local v8, "columnSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    move-object v5, v8

    .local v5, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v0, v5

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    move/from16 v16, v15

    .end local v15    # "i$":I
    .local v16, "i$":I
    :goto_0
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_8

    aget-object v20, v5, v16

    .line 95
    .local v20, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    if-nez v12, :cond_0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    move-object/from16 v21, v0

    check-cast v21, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 96
    :cond_0
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v11

    .line 97
    .local v11, "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v11}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 98
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/droidparts/model/Entity;

    .line 99
    .local v13, "foreignEntity":Lorg/droidparts/model/Entity;
    if-eqz v13, :cond_1

    .line 100
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/droidparts/persist/sql/EntityManager;->subManager(Ljava/lang/Class;)Lorg/droidparts/persist/sql/EntityManager;

    move-result-object v18

    .line 102
    .local v18, "manager":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<Lorg/droidparts/model/Entity;>;"
    iget-wide v0, v13, Lorg/droidparts/model/Entity;->id:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lorg/droidparts/persist/sql/EntityManager;->read(J)Lorg/droidparts/model/Entity;

    move-result-object v19

    .line 103
    .local v19, "obj":Lorg/droidparts/model/Entity;
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lorg/droidparts/inner/ReflectionUtils;->setFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V

    .line 94
    .end local v11    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v13    # "foreignEntity":Lorg/droidparts/model/Entity;
    .end local v16    # "i$":I
    .end local v18    # "manager":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<Lorg/droidparts/model/Entity;>;"
    .end local v19    # "obj":Lorg/droidparts/model/Entity;
    :cond_1
    :goto_1
    add-int/lit8 v15, v16, 0x1

    .restart local v15    # "i$":I
    move/from16 v16, v15

    .end local v15    # "i$":I
    .restart local v16    # "i$":I
    goto :goto_0

    .line 105
    .restart local v11    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    invoke-static {v11}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v21

    if-nez v21, :cond_3

    invoke-static {v11}, Lorg/droidparts/inner/TypeHelper;->isCollection(Ljava/lang/Class;)Z

    move-result v21

    if-eqz v21, :cond_1

    :cond_3
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 107
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/droidparts/persist/sql/EntityManager;->subManager(Ljava/lang/Class;)Lorg/droidparts/persist/sql/EntityManager;

    move-result-object v18

    .line 108
    .restart local v18    # "manager":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<Lorg/droidparts/model/Entity;>;"
    invoke-static {v11}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 109
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lorg/droidparts/model/Entity;

    .line 110
    .local v4, "arr":[Lorg/droidparts/model/Entity;
    if-eqz v4, :cond_1

    .line 111
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    array-length v0, v4

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v14, v0, :cond_1

    .line 112
    aget-object v9, v4, v14

    .line 113
    .local v9, "ent":Lorg/droidparts/model/Entity;
    if-eqz v9, :cond_4

    .line 114
    iget-wide v0, v9, Lorg/droidparts/model/Entity;->id:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lorg/droidparts/persist/sql/EntityManager;->read(J)Lorg/droidparts/model/Entity;

    move-result-object v21

    aput-object v21, v4, v14

    .line 111
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 119
    .end local v4    # "arr":[Lorg/droidparts/model/Entity;
    .end local v9    # "ent":Lorg/droidparts/model/Entity;
    .end local v14    # "i":I
    :cond_5
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Collection;

    .line 120
    .local v6, "coll":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/droidparts/model/Entity;>;"
    if-eqz v6, :cond_1

    .line 121
    new-instance v10, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v21

    move/from16 v0, v21

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 123
    .local v10, "entities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/model/Entity;>;"
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .end local v16    # "i$":I
    .local v15, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/droidparts/model/Entity;

    .line 124
    .restart local v9    # "ent":Lorg/droidparts/model/Entity;
    if-eqz v9, :cond_6

    .line 125
    iget-wide v0, v9, Lorg/droidparts/model/Entity;->id:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lorg/droidparts/persist/sql/EntityManager;->read(J)Lorg/droidparts/model/Entity;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 128
    .end local v9    # "ent":Lorg/droidparts/model/Entity;
    :cond_7
    invoke-interface {v6}, Ljava/util/Collection;->clear()V

    .line 129
    invoke-interface {v6, v10}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 136
    .end local v6    # "coll":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/droidparts/model/Entity;>;"
    .end local v10    # "entities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/model/Entity;>;"
    .end local v11    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v18    # "manager":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<Lorg/droidparts/model/Entity;>;"
    .end local v20    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    .restart local v16    # "i$":I
    :cond_8
    return-void
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 63
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    iget-object v0, p0, Lorg/droidparts/persist/sql/EntityManager;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method protected getDB()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 140
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    iget-object v0, p0, Lorg/droidparts/persist/sql/EntityManager;->db:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method protected getEagerForeignKeyColumnNames()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 215
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    iget-object v6, p0, Lorg/droidparts/persist/sql/EntityManager;->eagerForeignKeyColumnNames:[Ljava/lang/String;

    if-nez v6, :cond_2

    .line 216
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 217
    .local v2, "eagerColumnNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v6, p0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    invoke-static {v6}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v1

    .line 219
    .local v1, "columnSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    move-object v0, v1

    .local v0, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    .line 220
    .local v5, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    iget-object v6, v5, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v6, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-boolean v6, v6, Lorg/droidparts/inner/ann/sql/ColumnAnn;->eager:Z

    if-eqz v6, :cond_0

    .line 221
    iget-object v6, v5, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v6, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v6, v6, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 224
    .end local v5    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    :cond_1
    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    iput-object v6, p0, Lorg/droidparts/persist/sql/EntityManager;->eagerForeignKeyColumnNames:[Ljava/lang/String;

    .line 227
    .end local v0    # "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    .end local v1    # "columnSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    .end local v2    # "eagerColumnNames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    iget-object v6, p0, Lorg/droidparts/persist/sql/EntityManager;->eagerForeignKeyColumnNames:[Ljava/lang/String;

    return-object v6
.end method

.method protected getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    iget-object v0, p0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    invoke-static {v0}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected putToContentValues(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 6
    .param p1, "cv"    # Landroid/content/ContentValues;
    .param p2, "key"    # Ljava/lang/String;
    .param p5, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/ContentValues;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TV;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 236
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p3, "valueType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p4, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    if-nez p5, :cond_0

    .line 237
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 243
    :goto_0
    return-void

    .line 239
    :cond_0
    invoke-static {p3}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    move-object v1, p3

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    .line 240
    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/inner/converter/Converter;->putToContentValues(Ljava/lang/Class;Ljava/lang/Class;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected readFromCursor(Landroid/database/Cursor;ILjava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "columnIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            "I",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Class",
            "<TV;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 247
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p3, "valType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p4, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<TV;>;"
    invoke-interface {p1, p2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    const/4 v1, 0x0

    .line 251
    :goto_0
    return-object v1

    .line 250
    :cond_0
    invoke-static {p3}, Lorg/droidparts/inner/ConverterRegistry;->getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;

    move-result-object v0

    .line 251
    .local v0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<TT;>;"
    invoke-virtual {v0, p3, p4, p1, p2}, Lorg/droidparts/inner/converter/Converter;->readFromCursor(Ljava/lang/Class;Ljava/lang/Class;Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public readRow(Landroid/database/Cursor;)Lorg/droidparts/model/Entity;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TEntityType;"
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    iget-object v9, p0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    invoke-static {v9}, Lorg/droidparts/inner/ReflectionUtils;->newInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/droidparts/model/Entity;

    .line 69
    .local v5, "entity":Lorg/droidparts/model/Entity;, "TEntityType;"
    iget-object v9, p0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    invoke-static {v9}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v2

    .line 71
    .local v2, "columnSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    move-object v0, v2

    .local v0, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_2

    aget-object v8, v0, v6

    .line 72
    .local v8, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    iget-object v9, v8, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v9, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v9, v9, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 73
    .local v1, "colIdx":I
    if-ltz v1, :cond_1

    .line 75
    :try_start_0
    iget-object v9, v8, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v9}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v9

    iget-object v10, v8, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    invoke-virtual {p0, p1, v1, v9, v10}, Lorg/droidparts/persist/sql/EntityManager;->readFromCursor(Landroid/database/Cursor;ILjava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .line 77
    .local v3, "columnVal":Ljava/lang/Object;
    if-nez v3, :cond_0

    iget-object v9, v8, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v9, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-boolean v9, v9, Lorg/droidparts/inner/ann/sql/ColumnAnn;->nullable:Z

    if-eqz v9, :cond_1

    .line 78
    :cond_0
    iget-object v9, v8, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-static {v5, v9, v3}, Lorg/droidparts/inner/ReflectionUtils;->setFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    .end local v3    # "columnVal":Ljava/lang/Object;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 80
    :catch_0
    move-exception v4

    .line 81
    .local v4, "e":Ljava/lang/Exception;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 85
    .end local v1    # "colIdx":I
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v8    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    :cond_2
    return-object v5
.end method

.method protected toContentValues(Lorg/droidparts/model/Entity;)Landroid/content/ContentValues;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEntityType;)",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .line 150
    .local p0, "this":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p1, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 151
    .local v1, "cv":Landroid/content/ContentValues;
    iget-object v0, p0, Lorg/droidparts/persist/sql/EntityManager;->cls:Ljava/lang/Class;

    invoke-static {v0}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v7

    .line 153
    .local v7, "columnSpecs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    move-object v6, v7

    .local v6, "arr$":[Lorg/droidparts/inner/ann/FieldSpec;
    array-length v10, v6

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v10, :cond_0

    aget-object v11, v6, v9

    .line 154
    .local v11, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    iget-object v0, v11, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-static {p1, v0}, Lorg/droidparts/inner/ReflectionUtils;->getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v5

    .line 156
    .local v5, "columnVal":Ljava/lang/Object;
    :try_start_0
    iget-object v0, v11, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v0, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-object v2, v0, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    iget-object v0, v11, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    iget-object v4, v11, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/droidparts/persist/sql/EntityManager;->putToContentValues(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 158
    :catch_0
    move-exception v8

    .line 159
    .local v8, "e":Ljava/lang/Exception;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 162
    .end local v5    # "columnVal":Ljava/lang/Object;
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v11    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    :cond_0
    return-object v1
.end method
