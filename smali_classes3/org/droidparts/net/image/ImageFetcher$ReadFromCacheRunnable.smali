.class Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;
.super Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;
.source "ImageFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/net/image/ImageFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ReadFromCacheRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/droidparts/net/image/ImageFetcher;


# direct methods
.method public constructor <init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;J)V
    .locals 1
    .param p2, "spec"    # Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
    .param p3, "submitted"    # J

    .prologue
    .line 430
    iput-object p1, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    .line 431
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpecRunnable;-><init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;J)V

    .line 432
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 436
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    iget-object v3, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    invoke-virtual {v2, v3}, Lorg/droidparts/net/image/ImageFetcher;->readCached(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 437
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 438
    new-instance v1, Lorg/droidparts/net/image/ImageFetcher$FetchAndCacheRunnable;

    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    iget-object v3, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    iget-wide v4, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->submitted:J

    invoke-direct {v1, v2, v3, v4, v5}, Lorg/droidparts/net/image/ImageFetcher$FetchAndCacheRunnable;-><init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;J)V

    .line 440
    .local v1, "r":Lorg/droidparts/net/image/ImageFetcher$FetchAndCacheRunnable;
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    iget-object v2, v2, Lorg/droidparts/net/image/ImageFetcher;->fetchExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 444
    .end local v1    # "r":Lorg/droidparts/net/image/ImageFetcher$FetchAndCacheRunnable;
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    iget-object v3, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    iget-wide v4, p0, Lorg/droidparts/net/image/ImageFetcher$ReadFromCacheRunnable;->submitted:J

    invoke-virtual {v2, v3, v4, v5, v0}, Lorg/droidparts/net/image/ImageFetcher;->attachIfMostRecent(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;JLandroid/graphics/Bitmap;)V

    goto :goto_0
.end method
