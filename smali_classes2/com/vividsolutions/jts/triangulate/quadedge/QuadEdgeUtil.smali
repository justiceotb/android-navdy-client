.class public Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeUtil;
.super Ljava/lang/Object;
.source "QuadEdgeUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findEdgesIncidentOnOrigin(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Ljava/util/List;
    .locals 2
    .param p0, "start"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v0, "incEdge":Ljava/util/List;
    move-object v1, p0

    .line 64
    .local v1, "qe":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 66
    if-ne v1, p0, :cond_0

    .line 68
    return-object v0
.end method
