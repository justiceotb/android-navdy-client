.class public Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;
.super Ljava/lang/Object;
.source "VoronoiDiagramBuilder.java"


# instance fields
.field private clipEnv:Lcom/vividsolutions/jts/geom/Envelope;

.field private diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

.field private siteCoords:Ljava/util/Collection;

.field private subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

.field private tolerance:D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->tolerance:D

    .line 54
    iput-object v2, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 55
    iput-object v2, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->clipEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 56
    iput-object v2, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 64
    return-void
.end method

.method private static clipGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "clipEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toGeometry(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 161
    .local v0, "clipPoly":Lcom/vividsolutions/jts/geom/Geometry;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v1, "clipped":Ljava/util/List;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v5

    if-ge v3, v5, :cond_3

    .line 163
    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 164
    .local v2, "g":Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v4, 0x0

    .line 166
    .local v4, "result":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 167
    move-object v4, v2

    .line 174
    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 175
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 169
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/Geometry;->intersection(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    .line 171
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getUserData()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/Geometry;->setUserData(Ljava/lang/Object;)V

    goto :goto_1

    .line 178
    .end local v2    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v4    # "result":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v5

    invoke-static {v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toGeometryArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v5

    return-object v5
.end method

.method private create()V
    .locals 10

    .prologue
    .line 115
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    if-eqz v5, :cond_0

    .line 129
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->siteCoords:Ljava/util/Collection;

    invoke-static {v5}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->envelope(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    .line 118
    .local v2, "siteEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iput-object v2, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 120
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v6

    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 121
    .local v0, "expandBy":D
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v5, v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->expandBy(D)V

    .line 122
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->clipEnv:Lcom/vividsolutions/jts/geom/Envelope;

    if-eqz v5, :cond_1

    .line 123
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->clipEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 125
    :cond_1
    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->siteCoords:Ljava/util/Collection;

    invoke-static {v5}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->toVertices(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    .line 126
    .local v4, "vertices":Ljava/util/List;
    new-instance v5, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    iget-wide v6, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->tolerance:D

    invoke-direct {v5, v2, v6, v7}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;-><init>(Lcom/vividsolutions/jts/geom/Envelope;D)V

    iput-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 127
    new-instance v3, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;

    iget-object v5, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-direct {v3, v5}, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;-><init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)V

    .line 128
    .local v3, "triangulator":Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;
    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->insertSites(Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method public getDiagram(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->create()V

    .line 152
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getVoronoiDiagram(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 155
    .local v0, "polys":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->diagramEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->clipGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public getSubdivision()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->create()V

    .line 139
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    return-object v0
.end method

.method public setClipEnvelope(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 0
    .param p1, "clipEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->clipEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 100
    return-void
.end method

.method public setSites(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 75
    invoke-static {p1}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->extractUniqueCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateList;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->siteCoords:Ljava/util/Collection;

    .line 76
    return-void
.end method

.method public setSites(Ljava/util/Collection;)V
    .locals 1
    .param p1, "coords"    # Ljava/util/Collection;

    .prologue
    .line 87
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->toCoordinateArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-static {v0}, Lcom/vividsolutions/jts/triangulate/DelaunayTriangulationBuilder;->unique([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateList;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->siteCoords:Ljava/util/Collection;

    .line 88
    return-void
.end method

.method public setTolerance(D)V
    .locals 1
    .param p1, "tolerance"    # D

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/vividsolutions/jts/triangulate/VoronoiDiagramBuilder;->tolerance:D

    .line 111
    return-void
.end method
