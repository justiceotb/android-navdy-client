.class public Lcom/vividsolutions/jts/util/GeometricShapeFactory;
.super Ljava/lang/Object;
.source "GeometricShapeFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;
    }
.end annotation


# instance fields
.field protected dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

.field protected geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field protected nPts:I

.field protected precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

.field protected rotationAngle:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 2
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 60
    new-instance v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;-><init>(Lcom/vividsolutions/jts/util/GeometricShapeFactory;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    .line 61
    const/16 v0, 0x64

    iput v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotationAngle:D

    .line 85
    iput-object p1, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 86
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 87
    return-void
.end method


# virtual methods
.method protected coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 395
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 396
    .local v0, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 397
    return-object v0
.end method

.method protected coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 5
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "trans"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 402
    iget-wide v0, p5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v0, p1

    iget-wide v2, p5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v2, p3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public createArc(DD)Lcom/vividsolutions/jts/geom/LineString;
    .locals 35
    .param p1, "startAng"    # D
    .param p3, "angExtent"    # D

    .prologue
    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v16

    .line 329
    .local v16, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v30

    const-wide/high16 v32, 0x4000000000000000L    # 2.0

    div-double v24, v30, v32

    .line 330
    .local v24, "xRadius":D
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v30

    const-wide/high16 v32, 0x4000000000000000L    # 2.0

    div-double v28, v30, v32

    .line 332
    .local v28, "yRadius":D
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v30

    add-double v12, v30, v24

    .line 333
    .local v12, "centreX":D
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v30

    add-double v14, v30, v28

    .line 335
    .local v14, "centreY":D
    move-wide/from16 v10, p3

    .line 336
    .local v10, "angSize":D
    const-wide/16 v30, 0x0

    cmpg-double v30, v10, v30

    if-lez v30, :cond_0

    const-wide v30, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v30, v10, v30

    if-lez v30, :cond_1

    .line 337
    :cond_0
    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    .line 338
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v30, v0

    add-int/lit8 v30, v30, -0x1

    move/from16 v0, v30

    int-to-double v0, v0

    move-wide/from16 v30, v0

    div-double v8, v10, v30

    .line 340
    .local v8, "angInc":D
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v30, v0

    move/from16 v0, v30

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v21, v0

    .line 341
    .local v21, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/16 v18, 0x0

    .line 342
    .local v18, "iPt":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v30, v0

    move/from16 v0, v17

    move/from16 v1, v30

    if-ge v0, v1, :cond_2

    .line 343
    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v30, v0

    mul-double v30, v30, v8

    add-double v6, p1, v30

    .line 344
    .local v6, "ang":D
    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v30, v30, v24

    add-double v22, v30, v12

    .line 345
    .local v22, "x":D
    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v30, v30, v28

    add-double v26, v30, v14

    .line 346
    .local v26, "y":D
    add-int/lit8 v19, v18, 0x1

    .end local v18    # "iPt":I
    .local v19, "iPt":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    move-wide/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v30

    aput-object v30, v21, v18

    .line 342
    add-int/lit8 v17, v17, 0x1

    move/from16 v18, v19

    .end local v19    # "iPt":I
    .restart local v18    # "iPt":I
    goto :goto_0

    .line 348
    .end local v6    # "ang":D
    .end local v22    # "x":D
    .end local v26    # "y":D
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v20

    .line 349
    .local v20, "line":Lcom/vividsolutions/jts/geom/LineString;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v30

    check-cast v30, Lcom/vividsolutions/jts/geom/LineString;

    return-object v30
.end method

.method public createArcPolygon(DD)Lcom/vividsolutions/jts/geom/Polygon;
    .locals 37
    .param p1, "startAng"    # D
    .param p3, "angExtent"    # D

    .prologue
    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v16

    .line 363
    .local v16, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v32

    const-wide/high16 v34, 0x4000000000000000L    # 2.0

    div-double v26, v32, v34

    .line 364
    .local v26, "xRadius":D
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v32

    const-wide/high16 v34, 0x4000000000000000L    # 2.0

    div-double v30, v32, v34

    .line 366
    .local v30, "yRadius":D
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v32

    add-double v12, v32, v26

    .line 367
    .local v12, "centreX":D
    invoke-virtual/range {v16 .. v16}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v32

    add-double v14, v32, v30

    .line 369
    .local v14, "centreY":D
    move-wide/from16 v10, p3

    .line 370
    .local v10, "angSize":D
    const-wide/16 v32, 0x0

    cmpg-double v23, v10, v32

    if-lez v23, :cond_0

    const-wide v32, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v23, v10, v32

    if-lez v23, :cond_1

    .line 371
    :cond_0
    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    .line 372
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v32, v0

    div-double v8, v10, v32

    .line 376
    .local v8, "angInc":D
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v21, v0

    .line 378
    .local v21, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/16 v18, 0x0

    .line 379
    .local v18, "iPt":I
    add-int/lit8 v19, v18, 0x1

    .end local v18    # "iPt":I
    .local v19, "iPt":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14, v15}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v23

    aput-object v23, v21, v18

    .line 380
    const/16 v17, 0x0

    .local v17, "i":I
    move/from16 v18, v19

    .end local v19    # "iPt":I
    .restart local v18    # "iPt":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v23, v0

    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_2

    .line 381
    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v32, v0

    mul-double v32, v32, v8

    add-double v6, p1, v32

    .line 383
    .local v6, "ang":D
    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v32

    mul-double v32, v32, v26

    add-double v24, v32, v12

    .line 384
    .local v24, "x":D
    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v32

    mul-double v32, v32, v30

    add-double v28, v32, v14

    .line 385
    .local v28, "y":D
    add-int/lit8 v19, v18, 0x1

    .end local v18    # "iPt":I
    .restart local v19    # "iPt":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    move-wide/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v23

    aput-object v23, v21, v18

    .line 380
    add-int/lit8 v17, v17, 0x1

    move/from16 v18, v19

    .end local v19    # "iPt":I
    .restart local v18    # "iPt":I
    goto :goto_0

    .line 387
    .end local v6    # "ang":D
    .end local v24    # "x":D
    .end local v28    # "y":D
    :cond_2
    add-int/lit8 v19, v18, 0x1

    .end local v18    # "iPt":I
    .restart local v19    # "iPt":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14, v15}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v23

    aput-object v23, v21, v18

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v22

    .line 389
    .local v22, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v23, v0

    const/16 v32, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v20

    .line 390
    .local v20, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v23

    check-cast v23, Lcom/vividsolutions/jts/geom/Polygon;

    return-object v23
.end method

.method public createCircle()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->createEllipse()Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v0

    return-object v0
.end method

.method public createEllipse()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 34

    .prologue
    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v12

    .line 229
    .local v12, "env":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v12}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v28

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v22, v28, v30

    .line 230
    .local v22, "xRadius":D
    invoke-virtual {v12}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v28

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v26, v28, v30

    .line 232
    .local v26, "yRadius":D
    invoke-virtual {v12}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v28

    add-double v8, v28, v22

    .line 233
    .local v8, "centreX":D
    invoke-virtual {v12}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v28

    add-double v10, v28, v26

    .line 235
    .local v10, "centreY":D
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v17, v0

    .line 236
    .local v17, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v14, 0x0

    .line 237
    .local v14, "iPt":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v13, v0, :cond_0

    .line 238
    int-to-double v0, v13

    move-wide/from16 v28, v0

    const-wide v30, 0x401921fb54442d18L    # 6.283185307179586

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v32, v0

    div-double v30, v30, v32

    mul-double v6, v28, v30

    .line 239
    .local v6, "ang":D
    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v28

    mul-double v28, v28, v22

    add-double v20, v28, v8

    .line 240
    .local v20, "x":D
    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v28

    mul-double v28, v28, v26

    add-double v24, v28, v10

    .line 241
    .local v24, "y":D
    add-int/lit8 v15, v14, 0x1

    .end local v14    # "iPt":I
    .local v15, "iPt":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move-wide/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v19

    aput-object v19, v17, v14

    .line 237
    add-int/lit8 v13, v13, 0x1

    move v14, v15

    .end local v15    # "iPt":I
    .restart local v14    # "iPt":I
    goto :goto_0

    .line 243
    .end local v6    # "ang":D
    .end local v20    # "x":D
    .end local v24    # "y":D
    :cond_0
    new-instance v19, Lcom/vividsolutions/jts/geom/Coordinate;

    const/16 v28, 0x0

    aget-object v28, v17, v28

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v19, v17, v14

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v18

    .line 246
    .local v18, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v19, v0

    const/16 v28, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v16

    .line 247
    .local v16, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v19

    check-cast v19, Lcom/vividsolutions/jts/geom/Polygon;

    return-object v19
.end method

.method public createRectangle()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 26

    .prologue
    .line 168
    const/4 v12, 0x0

    .line 169
    .local v12, "ipt":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    move/from16 v22, v0

    div-int/lit8 v14, v22, 0x4

    .line 170
    .local v14, "nSide":I
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ge v14, v0, :cond_0

    const/4 v14, 0x1

    .line 171
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v22

    int-to-double v0, v14

    move-wide/from16 v24, v0

    div-double v6, v22, v24

    .line 172
    .local v6, "XsegLen":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v22

    int-to-double v0, v14

    move-wide/from16 v24, v0

    div-double v8, v22, v24

    .line 174
    .local v8, "YsegLen":D
    mul-int/lit8 v22, v14, 0x4

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v16, v0

    .line 175
    .local v16, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v10

    .line 180
    .local v10, "env":Lcom/vividsolutions/jts/geom/Envelope;
    const/4 v11, 0x0

    .local v11, "i":I
    move v13, v12

    .end local v12    # "ipt":I
    .local v13, "ipt":I
    :goto_0
    if-ge v11, v14, :cond_1

    .line 181
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v22

    int-to-double v0, v11

    move-wide/from16 v24, v0

    mul-double v24, v24, v6

    add-double v18, v22, v24

    .line 182
    .local v18, "x":D
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v20

    .line 183
    .local v20, "y":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "ipt":I
    .restart local v12    # "ipt":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v22

    aput-object v22, v16, v13

    .line 180
    add-int/lit8 v11, v11, 0x1

    move v13, v12

    .end local v12    # "ipt":I
    .restart local v13    # "ipt":I
    goto :goto_0

    .line 185
    .end local v18    # "x":D
    .end local v20    # "y":D
    :cond_1
    const/4 v11, 0x0

    :goto_1
    if-ge v11, v14, :cond_2

    .line 186
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v18

    .line 187
    .restart local v18    # "x":D
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v22

    int-to-double v0, v11

    move-wide/from16 v24, v0

    mul-double v24, v24, v8

    add-double v20, v22, v24

    .line 188
    .restart local v20    # "y":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "ipt":I
    .restart local v12    # "ipt":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v22

    aput-object v22, v16, v13

    .line 185
    add-int/lit8 v11, v11, 0x1

    move v13, v12

    .end local v12    # "ipt":I
    .restart local v13    # "ipt":I
    goto :goto_1

    .line 190
    .end local v18    # "x":D
    .end local v20    # "y":D
    :cond_2
    const/4 v11, 0x0

    :goto_2
    if-ge v11, v14, :cond_3

    .line 191
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v22

    int-to-double v0, v11

    move-wide/from16 v24, v0

    mul-double v24, v24, v6

    sub-double v18, v22, v24

    .line 192
    .restart local v18    # "x":D
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v20

    .line 193
    .restart local v20    # "y":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "ipt":I
    .restart local v12    # "ipt":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v22

    aput-object v22, v16, v13

    .line 190
    add-int/lit8 v11, v11, 0x1

    move v13, v12

    .end local v12    # "ipt":I
    .restart local v13    # "ipt":I
    goto :goto_2

    .line 195
    .end local v18    # "x":D
    .end local v20    # "y":D
    :cond_3
    const/4 v11, 0x0

    :goto_3
    if-ge v11, v14, :cond_4

    .line 196
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v18

    .line 197
    .restart local v18    # "x":D
    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v22

    int-to-double v0, v11

    move-wide/from16 v24, v0

    mul-double v24, v24, v8

    sub-double v20, v22, v24

    .line 198
    .restart local v20    # "y":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "ipt":I
    .restart local v12    # "ipt":I
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v22

    aput-object v22, v16, v13

    .line 195
    add-int/lit8 v11, v11, 0x1

    move v13, v12

    .end local v12    # "ipt":I
    .restart local v13    # "ipt":I
    goto :goto_3

    .line 200
    .end local v18    # "x":D
    .end local v20    # "y":D
    :cond_4
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "ipt":I
    .restart local v12    # "ipt":I
    new-instance v22, Lcom/vividsolutions/jts/geom/Coordinate;

    const/16 v23, 0x0

    aget-object v23, v16, v23

    invoke-direct/range {v22 .. v23}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v22, v16, v13

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v17

    .line 203
    .local v17, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v15

    .line 204
    .local v15, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v22

    check-cast v22, Lcom/vividsolutions/jts/geom/Polygon;

    return-object v22
.end method

.method public createSquircle()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 2

    .prologue
    .line 261
    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->createSupercircle(D)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v0

    return-object v0
.end method

.method public createSupercircle(D)Lcom/vividsolutions/jts/geom/Polygon;
    .locals 37
    .param p1, "power"    # D

    .prologue
    .line 272
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    div-double v24, v12, p1

    .line 274
    .local v24, "recipPow":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getMinSize()D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v22, v12, v14

    .line 275
    .local v22, "radius":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getCentre()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v10

    .line 277
    .local v10, "centre":Lcom/vividsolutions/jts/geom/Coordinate;
    move-wide/from16 v0, v22

    move-wide/from16 v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v20

    .line 278
    .local v20, "r4":D
    move-wide/from16 v34, v22

    .line 280
    .local v34, "y0":D
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v12, v20, v12

    move-wide/from16 v0, v24

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v32

    .line 282
    .local v32, "xyInt":D
    move-object/from16 v0, p0

    iget v5, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    div-int/lit8 v17, v5, 0x8

    .line 283
    .local v17, "nSegsInOct":I
    mul-int/lit8 v5, v17, 0x8

    add-int/lit8 v27, v5, 0x1

    .line 284
    .local v27, "totPts":I
    move/from16 v0, v27

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v19, v0

    .line 285
    .local v19, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    move/from16 v0, v17

    int-to-double v12, v0

    div-double v30, v32, v12

    .line 287
    .local v30, "xInc":D
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    move/from16 v0, v17

    if-gt v4, v0, :cond_1

    .line 288
    const-wide/16 v6, 0x0

    .line 289
    .local v6, "x":D
    move-wide/from16 v8, v34

    .line 290
    .local v8, "y":D
    if-eqz v4, :cond_0

    .line 291
    int-to-double v12, v4

    mul-double v6, v30, v12

    .line 292
    move-wide/from16 v0, p1

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v28

    .line 293
    .local v28, "x4":D
    sub-double v12, v20, v28

    move-wide/from16 v0, v24

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    .end local v28    # "x4":D
    :cond_0
    move-object/from16 v5, p0

    .line 295
    invoke-virtual/range {v5 .. v10}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    aput-object v5, v19, v4

    .line 296
    mul-int/lit8 v5, v17, 0x2

    sub-int/2addr v5, v4

    move-object/from16 v11, p0

    move-wide v12, v8

    move-wide v14, v6

    move-object/from16 v16, v10

    invoke-virtual/range {v11 .. v16}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    aput-object v11, v19, v5

    .line 298
    mul-int/lit8 v5, v17, 0x2

    add-int/2addr v5, v4

    neg-double v14, v6

    move-object/from16 v11, p0

    move-wide v12, v8

    move-object/from16 v16, v10

    invoke-virtual/range {v11 .. v16}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    aput-object v11, v19, v5

    .line 299
    mul-int/lit8 v5, v17, 0x4

    sub-int/2addr v5, v4

    neg-double v14, v8

    move-object/from16 v11, p0

    move-wide v12, v6

    move-object/from16 v16, v10

    invoke-virtual/range {v11 .. v16}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    aput-object v11, v19, v5

    .line 301
    mul-int/lit8 v5, v17, 0x4

    add-int/2addr v5, v4

    neg-double v12, v6

    neg-double v14, v8

    move-object/from16 v11, p0

    move-object/from16 v16, v10

    invoke-virtual/range {v11 .. v16}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    aput-object v11, v19, v5

    .line 302
    mul-int/lit8 v5, v17, 0x6

    sub-int/2addr v5, v4

    neg-double v12, v8

    neg-double v14, v6

    move-object/from16 v11, p0

    move-object/from16 v16, v10

    invoke-virtual/range {v11 .. v16}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    aput-object v11, v19, v5

    .line 304
    mul-int/lit8 v5, v17, 0x6

    add-int/2addr v5, v4

    neg-double v12, v8

    move-object/from16 v11, p0

    move-wide v14, v6

    move-object/from16 v16, v10

    invoke-virtual/range {v11 .. v16}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    aput-object v11, v19, v5

    .line 305
    mul-int/lit8 v5, v17, 0x8

    sub-int/2addr v5, v4

    neg-double v12, v6

    move-object/from16 v11, p0

    move-wide v14, v8

    move-object/from16 v16, v10

    invoke-virtual/range {v11 .. v16}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->coordTrans(DDLcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v11

    aput-object v11, v19, v5

    .line 287
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 307
    .end local v6    # "x":D
    .end local v8    # "y":D
    :cond_1
    move-object/from16 v0, v19

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    new-instance v11, Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v12, 0x0

    aget-object v12, v19, v12

    invoke-direct {v11, v12}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v11, v19, v5

    .line 309
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v26

    .line 310
    .local v26, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v11, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v5, v0, v11}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v18

    .line 311
    .local v18, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/geom/Polygon;

    return-object v5
.end method

.method protected rotate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 151
    iget-wide v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotationAngle:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 152
    iget-wide v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotationAngle:D

    iget-object v2, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getCentre()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v4, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->getCentre()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static/range {v0 .. v5}, Lcom/vividsolutions/jts/geom/util/AffineTransformation;->rotationInstance(DDD)Lcom/vividsolutions/jts/geom/util/AffineTransformation;

    move-result-object v6

    .line 154
    .local v6, "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    invoke-virtual {p1, v6}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 156
    .end local v6    # "trans":Lcom/vividsolutions/jts/geom/util/AffineTransformation;
    :cond_0
    return-object p1
.end method

.method public setBase(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "base"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->setBase(Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-void
.end method

.method public setCentre(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "centre"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->setCentre(Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-void
.end method

.method public setEnvelope(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 1
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->setEnvelope(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 92
    return-void
.end method

.method public setHeight(D)V
    .locals 1
    .param p1, "height"    # D

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->setHeight(D)V

    return-void
.end method

.method public setNumPoints(I)V
    .locals 0
    .param p1, "nPts"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->nPts:I

    return-void
.end method

.method public setRotation(D)V
    .locals 1
    .param p1, "radians"    # D

    .prologue
    .line 146
    iput-wide p1, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->rotationAngle:D

    .line 147
    return-void
.end method

.method public setSize(D)V
    .locals 1
    .param p1, "size"    # D

    .prologue
    .line 122
    iget-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->setSize(D)V

    return-void
.end method

.method public setWidth(D)V
    .locals 1
    .param p1, "width"    # D

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vividsolutions/jts/util/GeometricShapeFactory;->dim:Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/util/GeometricShapeFactory$Dimensions;->setWidth(D)V

    return-void
.end method
