.class public Lcom/vividsolutions/jts/util/Stopwatch;
.super Ljava/lang/Object;
.source "Stopwatch.java"


# instance fields
.field private isRunning:Z

.field private startTimestamp:J

.field private totalTime:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->totalTime:J

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->isRunning:Z

    .line 50
    invoke-virtual {p0}, Lcom/vividsolutions/jts/util/Stopwatch;->start()V

    .line 51
    return-void
.end method

.method private updateTotalTime()V
    .locals 6

    .prologue
    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 85
    .local v2, "endTimestamp":J
    iget-wide v4, p0, Lcom/vividsolutions/jts/util/Stopwatch;->startTimestamp:J

    sub-long v0, v2, v4

    .line 86
    .local v0, "elapsedTime":J
    iput-wide v2, p0, Lcom/vividsolutions/jts/util/Stopwatch;->startTimestamp:J

    .line 87
    iget-wide v4, p0, Lcom/vividsolutions/jts/util/Stopwatch;->totalTime:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/vividsolutions/jts/util/Stopwatch;->totalTime:J

    .line 88
    return-void
.end method


# virtual methods
.method public getTime()J
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/vividsolutions/jts/util/Stopwatch;->updateTotalTime()V

    .line 93
    iget-wide v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->totalTime:J

    return-wide v0
.end method

.method public getTimeString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/vividsolutions/jts/util/Stopwatch;->getTime()J

    move-result-wide v0

    .line 99
    .local v0, "totalTime":J
    const-wide/16 v4, 0x2710

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 100
    .local v2, "totalTimeStr":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 99
    .end local v2    # "totalTimeStr":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    long-to-double v4, v0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 71
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->totalTime:J

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->startTimestamp:J

    .line 73
    return-void
.end method

.method public split()J
    .locals 2

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->isRunning:Z

    if-eqz v0, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/vividsolutions/jts/util/Stopwatch;->updateTotalTime()V

    .line 79
    :cond_0
    iget-wide v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->totalTime:J

    return-wide v0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->isRunning:Z

    if-eqz v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->startTimestamp:J

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->isRunning:Z

    goto :goto_0
.end method

.method public stop()J
    .locals 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->isRunning:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/vividsolutions/jts/util/Stopwatch;->updateTotalTime()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->isRunning:Z

    .line 66
    :cond_0
    iget-wide v0, p0, Lcom/vividsolutions/jts/util/Stopwatch;->totalTime:J

    return-wide v0
.end method
