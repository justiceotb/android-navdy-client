.class Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;
.super Lcom/vividsolutions/jts/geom/util/GeometryTransformer;
.source "Densifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/densify/Densifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DensifyTransformer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/densify/Densifier;


# direct methods
.method constructor <init>(Lcom/vividsolutions/jts/densify/Densifier;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;->this$0:Lcom/vividsolutions/jts/densify/Densifier;

    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/GeometryTransformer;-><init>()V

    return-void
.end method

.method private createValidArea(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "roughAreaGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 175
    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected transformCoordinates(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 5
    .param p1, "coords"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "parent"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 138
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 139
    .local v0, "inputPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v2, p0, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;->this$0:Lcom/vividsolutions/jts/densify/Densifier;

    invoke-static {v2}, Lcom/vividsolutions/jts/densify/Densifier;->access$000(Lcom/vividsolutions/jts/densify/Densifier;)D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Lcom/vividsolutions/jts/densify/Densifier;->access$100([Lcom/vividsolutions/jts/geom/Coordinate;DLcom/vividsolutions/jts/geom/PrecisionModel;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 142
    .local v1, "newPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    instance-of v2, p2, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v2, :cond_0

    array-length v2, v1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 143
    const/4 v2, 0x0

    new-array v1, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getCoordinateSequenceFactory()Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;->create([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    return-object v2
.end method

.method protected transformMultiPolygon(Lcom/vividsolutions/jts/geom/MultiPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/MultiPolygon;
    .param p2, "parent"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 158
    invoke-super {p0, p1, p2}, Lcom/vividsolutions/jts/geom/util/GeometryTransformer;->transformMultiPolygon(Lcom/vividsolutions/jts/geom/MultiPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 159
    .local v0, "roughGeom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;->createValidArea(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method protected transformPolygon(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "parent"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 149
    invoke-super {p0, p1, p2}, Lcom/vividsolutions/jts/geom/util/GeometryTransformer;->transformPolygon(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 151
    .local v0, "roughGeom":Lcom/vividsolutions/jts/geom/Geometry;
    instance-of v1, p2, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v1, :cond_0

    .line 154
    .end local v0    # "roughGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return-object v0

    .restart local v0    # "roughGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;->createValidArea(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method
