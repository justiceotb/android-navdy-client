.class Lcom/vividsolutions/jts/noding/NodeVertexIterator;
.super Ljava/lang/Object;
.source "SegmentNodeList.java"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

.field private currSegIndex:I

.field private edge:Lcom/vividsolutions/jts/noding/NodedSegmentString;

.field private nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

.field private nodeIt:Ljava/util/Iterator;

.field private nodeList:Lcom/vividsolutions/jts/noding/SegmentNodeList;


# direct methods
.method constructor <init>(Lcom/vividsolutions/jts/noding/SegmentNodeList;)V
    .locals 1
    .param p1, "nodeList"    # Lcom/vividsolutions/jts/noding/SegmentNodeList;

    .prologue
    const/4 v0, 0x0

    .line 280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    .line 276
    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    .line 277
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currSegIndex:I

    .line 281
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nodeList:Lcom/vividsolutions/jts/noding/SegmentNodeList;

    .line 282
    invoke-virtual {p1}, Lcom/vividsolutions/jts/noding/SegmentNodeList;->getEdge()Lcom/vividsolutions/jts/noding/NodedSegmentString;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->edge:Lcom/vividsolutions/jts/noding/NodedSegmentString;

    .line 283
    invoke-virtual {p1}, Lcom/vividsolutions/jts/noding/SegmentNodeList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nodeIt:Ljava/util/Iterator;

    .line 284
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->readNextNode()V

    .line 285
    return-void
.end method

.method private readNextNode()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nodeIt:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nodeIt:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/noding/SegmentNode;

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    .line 322
    :goto_0
    return-void

    .line 321
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 289
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 294
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    if-nez v1, :cond_1

    .line 295
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    .line 296
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iget v0, v0, Lcom/vividsolutions/jts/noding/SegmentNode;->segmentIndex:I

    iput v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currSegIndex:I

    .line 297
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->readNextNode()V

    .line 298
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    .line 313
    :cond_0
    :goto_0
    return-object v0

    .line 301
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    if-eqz v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iget v1, v1, Lcom/vividsolutions/jts/noding/SegmentNode;->segmentIndex:I

    iget-object v2, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iget v2, v2, Lcom/vividsolutions/jts/noding/SegmentNode;->segmentIndex:I

    if-ne v1, v2, :cond_2

    .line 304
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    .line 305
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iget v0, v0, Lcom/vividsolutions/jts/noding/SegmentNode;->segmentIndex:I

    iput v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currSegIndex:I

    .line 306
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->readNextNode()V

    .line 307
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    goto :goto_0

    .line 310
    :cond_2
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->nextNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iget v1, v1, Lcom/vividsolutions/jts/noding/SegmentNode;->segmentIndex:I

    iget-object v2, p0, Lcom/vividsolutions/jts/noding/NodeVertexIterator;->currNode:Lcom/vividsolutions/jts/noding/SegmentNode;

    iget v2, v2, Lcom/vividsolutions/jts/noding/SegmentNode;->segmentIndex:I

    if-le v1, v2, :cond_0

    goto :goto_0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 329
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
