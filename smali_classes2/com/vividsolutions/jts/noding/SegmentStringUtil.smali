.class public Lcom/vividsolutions/jts/noding/SegmentStringUtil;
.super Ljava/lang/Object;
.source "SegmentStringUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractSegmentStrings(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 6
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 58
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v4, "segStr":Ljava/util/List;
    invoke-static {p0}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 60
    .local v2, "lines":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 61
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    .line 62
    .local v1, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 63
    .local v3, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v5, Lcom/vividsolutions/jts/noding/NodedSegmentString;

    invoke-direct {v5, v3, p0}, Lcom/vividsolutions/jts/noding/NodedSegmentString;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    .end local v1    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .end local v3    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-object v4
.end method
