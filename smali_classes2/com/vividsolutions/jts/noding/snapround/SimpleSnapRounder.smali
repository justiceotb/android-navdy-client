.class public Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;
.super Ljava/lang/Object;
.source "SimpleSnapRounder.java"

# interfaces
.implements Lcom/vividsolutions/jts/noding/Noder;


# instance fields
.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private nodedSegStrings:Ljava/util/Collection;

.field private final pm:Lcom/vividsolutions/jts/geom/PrecisionModel;

.field private final scaleFactor:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V
    .locals 2
    .param p1, "pm"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->pm:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 69
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 70
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->setPrecisionModel(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 71
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/PrecisionModel;->getScale()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->scaleFactor:D

    .line 72
    return-void
.end method

.method private checkCorrectness(Ljava/util/Collection;)V
    .locals 3
    .param p1, "inputSegmentStrings"    # Ljava/util/Collection;

    .prologue
    .line 97
    invoke-static {p1}, Lcom/vividsolutions/jts/noding/NodedSegmentString;->getNodedSubstrings(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 98
    .local v2, "resultSegStrings":Ljava/util/Collection;
    new-instance v1, Lcom/vividsolutions/jts/noding/NodingValidator;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/noding/NodingValidator;-><init>(Ljava/util/Collection;)V

    .line 100
    .local v1, "nv":Lcom/vividsolutions/jts/noding/NodingValidator;
    :try_start_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkValid()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private computeSnaps(Lcom/vividsolutions/jts/noding/NodedSegmentString;Ljava/util/Collection;)V
    .locals 7
    .param p1, "ss"    # Lcom/vividsolutions/jts/noding/NodedSegmentString;
    .param p2, "snapPts"    # Ljava/util/Collection;

    .prologue
    .line 144
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 145
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 146
    .local v3, "snapPt":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v0, Lcom/vividsolutions/jts/noding/snapround/HotPixel;

    iget-wide v4, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->scaleFactor:D

    iget-object v6, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/vividsolutions/jts/noding/snapround/HotPixel;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;DLcom/vividsolutions/jts/algorithm/LineIntersector;)V

    .line 147
    .local v0, "hotPixel":Lcom/vividsolutions/jts/noding/snapround/HotPixel;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/noding/NodedSegmentString;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_0

    .line 148
    invoke-virtual {v0, p1, v1}, Lcom/vividsolutions/jts/noding/snapround/HotPixel;->addSnappedNode(Lcom/vividsolutions/jts/noding/NodedSegmentString;I)Z

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    .end local v0    # "hotPixel":Lcom/vividsolutions/jts/noding/snapround/HotPixel;
    .end local v1    # "i":I
    .end local v3    # "snapPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    return-void
.end method

.method private computeSnaps(Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .param p1, "segStrings"    # Ljava/util/Collection;
    .param p2, "snapPts"    # Ljava/util/Collection;

    .prologue
    .line 136
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i0":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/noding/NodedSegmentString;

    .line 138
    .local v1, "ss":Lcom/vividsolutions/jts/noding/NodedSegmentString;
    invoke-direct {p0, v1, p2}, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->computeSnaps(Lcom/vividsolutions/jts/noding/NodedSegmentString;Ljava/util/Collection;)V

    goto :goto_0

    .line 140
    .end local v1    # "ss":Lcom/vividsolutions/jts/noding/NodedSegmentString;
    :cond_0
    return-void
.end method

.method private computeVertexSnaps(Lcom/vividsolutions/jts/noding/NodedSegmentString;Lcom/vividsolutions/jts/noding/NodedSegmentString;)V
    .locals 10
    .param p1, "e0"    # Lcom/vividsolutions/jts/noding/NodedSegmentString;
    .param p2, "e1"    # Lcom/vividsolutions/jts/noding/NodedSegmentString;

    .prologue
    .line 176
    invoke-virtual {p1}, Lcom/vividsolutions/jts/noding/NodedSegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 177
    .local v4, "pts0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/noding/NodedSegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 178
    .local v5, "pts1":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x0

    .local v1, "i0":I
    :goto_0
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 179
    new-instance v0, Lcom/vividsolutions/jts/noding/snapround/HotPixel;

    aget-object v6, v4, v1

    iget-wide v8, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->scaleFactor:D

    iget-object v7, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-direct {v0, v6, v8, v9, v7}, Lcom/vividsolutions/jts/noding/snapround/HotPixel;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;DLcom/vividsolutions/jts/algorithm/LineIntersector;)V

    .line 180
    .local v0, "hotPixel":Lcom/vividsolutions/jts/noding/snapround/HotPixel;
    const/4 v2, 0x0

    .local v2, "i1":I
    :goto_1
    array-length v6, v5

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_2

    .line 182
    if-ne p1, p2, :cond_1

    .line 183
    if-ne v1, v2, :cond_1

    .line 180
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 186
    :cond_1
    invoke-virtual {v0, p2, v2}, Lcom/vividsolutions/jts/noding/snapround/HotPixel;->addSnappedNode(Lcom/vividsolutions/jts/noding/NodedSegmentString;I)Z

    move-result v3

    .line 188
    .local v3, "isNodeAdded":Z
    if-eqz v3, :cond_0

    .line 189
    aget-object v6, v4, v1

    invoke-virtual {p1, v6, v1}, Lcom/vividsolutions/jts/noding/NodedSegmentString;->addIntersection(Lcom/vividsolutions/jts/geom/Coordinate;I)V

    goto :goto_2

    .line 178
    .end local v3    # "isNodeAdded":Z
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 193
    .end local v0    # "hotPixel":Lcom/vividsolutions/jts/noding/snapround/HotPixel;
    .end local v2    # "i1":I
    :cond_3
    return-void
.end method

.method private findInteriorIntersections(Ljava/util/Collection;Lcom/vividsolutions/jts/algorithm/LineIntersector;)Ljava/util/List;
    .locals 3
    .param p1, "segStrings"    # Ljava/util/Collection;
    .param p2, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .prologue
    .line 122
    new-instance v0, Lcom/vividsolutions/jts/noding/IntersectionFinderAdder;

    invoke-direct {v0, p2}, Lcom/vividsolutions/jts/noding/IntersectionFinderAdder;-><init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;)V

    .line 123
    .local v0, "intFinderAdder":Lcom/vividsolutions/jts/noding/IntersectionFinderAdder;
    new-instance v1, Lcom/vividsolutions/jts/noding/MCIndexNoder;

    invoke-direct {v1}, Lcom/vividsolutions/jts/noding/MCIndexNoder;-><init>()V

    .line 124
    .local v1, "noder":Lcom/vividsolutions/jts/noding/SinglePassNoder;
    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/noding/SinglePassNoder;->setSegmentIntersector(Lcom/vividsolutions/jts/noding/SegmentIntersector;)V

    .line 125
    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/noding/SinglePassNoder;->computeNodes(Ljava/util/Collection;)V

    .line 126
    invoke-virtual {v0}, Lcom/vividsolutions/jts/noding/IntersectionFinderAdder;->getInteriorIntersections()Ljava/util/List;

    move-result-object v2

    return-object v2
.end method

.method private snapRound(Ljava/util/Collection;Lcom/vividsolutions/jts/algorithm/LineIntersector;)V
    .locals 1
    .param p1, "segStrings"    # Ljava/util/Collection;
    .param p2, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->findInteriorIntersections(Ljava/util/Collection;Lcom/vividsolutions/jts/algorithm/LineIntersector;)Ljava/util/List;

    move-result-object v0

    .line 108
    .local v0, "intersections":Ljava/util/List;
    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->computeSnaps(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 109
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->computeVertexSnaps(Ljava/util/Collection;)V

    .line 110
    return-void
.end method


# virtual methods
.method public computeNodes(Ljava/util/Collection;)V
    .locals 1
    .param p1, "inputSegmentStrings"    # Ljava/util/Collection;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->nodedSegStrings:Ljava/util/Collection;

    .line 89
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->snapRound(Ljava/util/Collection;Lcom/vividsolutions/jts/algorithm/LineIntersector;)V

    .line 93
    return-void
.end method

.method public computeVertexSnaps(Ljava/util/Collection;)V
    .locals 5
    .param p1, "edges"    # Ljava/util/Collection;

    .prologue
    .line 161
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i0":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 162
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/noding/NodedSegmentString;

    .line 163
    .local v0, "edge0":Lcom/vividsolutions/jts/noding/NodedSegmentString;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i1":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 164
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/noding/NodedSegmentString;

    .line 165
    .local v1, "edge1":Lcom/vividsolutions/jts/noding/NodedSegmentString;
    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->computeVertexSnaps(Lcom/vividsolutions/jts/noding/NodedSegmentString;Lcom/vividsolutions/jts/noding/NodedSegmentString;)V

    goto :goto_0

    .line 168
    .end local v0    # "edge0":Lcom/vividsolutions/jts/noding/NodedSegmentString;
    .end local v1    # "edge1":Lcom/vividsolutions/jts/noding/NodedSegmentString;
    .end local v3    # "i1":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public getNodedSubstrings()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/snapround/SimpleSnapRounder;->nodedSegStrings:Ljava/util/Collection;

    invoke-static {v0}, Lcom/vividsolutions/jts/noding/NodedSegmentString;->getNodedSubstrings(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
