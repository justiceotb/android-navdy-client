.class public Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
.super Ljava/lang/Object;
.source "OctagonalEnvelope.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/geom/OctagonalEnvelope$1;,
        Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;
    }
.end annotation


# static fields
.field private static SQRT2:D


# instance fields
.field private maxA:D

.field private maxB:D

.field private maxX:D

.field private maxY:D

.field private minA:D

.field private minB:D

.field private minX:D

.field private minY:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    sput-wide v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->SQRT2:D

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 80
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 88
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 89
    invoke-virtual {p0, p2}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 2
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 97
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 114
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 115
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;)V
    .locals 2
    .param p1, "oct"    # Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 106
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 107
    return-void
.end method

.method private static computeA(DD)D
    .locals 2
    .param p0, "x"    # D
    .param p2, "y"    # D

    .prologue
    .line 48
    add-double v0, p0, p2

    return-wide v0
.end method

.method private static computeB(DD)D
    .locals 2
    .param p0, "x"    # D
    .param p2, "y"    # D

    .prologue
    .line 53
    sub-double v0, p0, p2

    return-wide v0
.end method

.method private isValid()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 246
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 247
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    cmpg-double v1, v2, v4

    if-lez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public contains(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;)Z
    .locals 6
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .prologue
    const/4 v0, 0x0

    .line 286
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 288
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public expandBy(D)V
    .locals 5
    .param p1, "distance"    # D

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    sget-wide v2, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->SQRT2:D

    mul-double v0, v2, p1

    .line 226
    .local v0, "diagonalDistance":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    sub-double/2addr v2, p1

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 227
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    add-double/2addr v2, p1

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    .line 228
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    sub-double/2addr v2, p1

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    .line 229
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    add-double/2addr v2, p1

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    .line 230
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    sub-double/2addr v2, v0

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    .line 231
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    add-double/2addr v2, v0

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    .line 232
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    sub-double/2addr v2, v0

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    .line 233
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    add-double/2addr v2, v0

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    .line 235
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isValid()Z

    move-result v2

    if-nez v2, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->setToNull()V

    goto :goto_0
.end method

.method public expandToInclude(DD)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
    .locals 7
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 194
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->computeA(DD)D

    move-result-wide v0

    .line 195
    .local v0, "A":D
    invoke-static {p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->computeB(DD)D

    move-result-wide v2

    .line 197
    .local v2, "B":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 198
    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 199
    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    .line 200
    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    .line 201
    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    .line 202
    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    .line 203
    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    .line 204
    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    .line 205
    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    .line 217
    :cond_0
    :goto_0
    return-object p0

    .line 208
    :cond_1
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    cmpg-double v4, p1, v4

    if-gez v4, :cond_2

    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 209
    :cond_2
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    cmpl-double v4, p1, v4

    if-lez v4, :cond_3

    iput-wide p1, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    .line 210
    :cond_3
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    cmpg-double v4, p3, v4

    if-gez v4, :cond_4

    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    .line 211
    :cond_4
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    cmpl-double v4, p3, v4

    if-lez v4, :cond_5

    iput-wide p3, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    .line 212
    :cond_5
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    cmpg-double v4, v0, v4

    if-gez v4, :cond_6

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    .line 213
    :cond_6
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    cmpl-double v4, v0, v4

    if-lez v4, :cond_7

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    .line 214
    :cond_7
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    cmpg-double v4, v2, v4

    if-gez v4, :cond_8

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    .line 215
    :cond_8
    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    cmpl-double v4, v2, v4

    if-lez v4, :cond_0

    iput-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    goto :goto_0
.end method

.method public expandToInclude(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
    .locals 4
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 179
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(DD)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 180
    return-object p0
.end method

.method public expandToInclude(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
    .locals 6
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 143
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 144
    invoke-interface {p1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getX(I)D

    move-result-wide v2

    .line 145
    .local v2, "x":D
    invoke-interface {p1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getY(I)D

    move-result-wide v4

    .line 146
    .local v4, "y":D
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(DD)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    .end local v2    # "x":D
    .end local v4    # "y":D
    :cond_0
    return-object p0
.end method

.method public expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
    .locals 4
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 185
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(DD)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 186
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(DD)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 187
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(DD)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 188
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->expandToInclude(DD)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .line 189
    return-object p0
.end method

.method public expandToInclude(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;)Lcom/vividsolutions/jts/geom/OctagonalEnvelope;
    .locals 4
    .param p1, "oct"    # Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .prologue
    .line 153
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-object p0

    .line 155
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 156
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 157
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    .line 158
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    .line 159
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    .line 160
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    .line 161
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    .line 162
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    .line 163
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    goto :goto_0

    .line 166
    :cond_2
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 167
    :cond_3
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_4

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    .line 168
    :cond_4
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_5

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    .line 169
    :cond_5
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_6

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    .line 170
    :cond_6
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_7

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    .line 171
    :cond_7
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_8

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    .line 172
    :cond_8
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_9

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    .line 173
    :cond_9
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    goto/16 :goto_0
.end method

.method public expandToInclude(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 138
    new-instance v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope$BoundingOctagonComponentFilter;-><init>(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;Lcom/vividsolutions/jts/geom/OctagonalEnvelope$1;)V

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 139
    return-void
.end method

.method public getMaxA()D
    .locals 2

    .prologue
    .line 123
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    return-wide v0
.end method

.method public getMaxB()D
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    return-wide v0
.end method

.method public getMaxX()D
    .locals 2

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    return-wide v0
.end method

.method public getMaxY()D
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    return-wide v0
.end method

.method public getMinA()D
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    return-wide v0
.end method

.method public getMinB()D
    .locals 2

    .prologue
    .line 124
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    return-wide v0
.end method

.method public getMinX()D
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    return-wide v0
.end method

.method public getMinY()D
    .locals 2

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    return-wide v0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 10
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v4, 0x0

    .line 270
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpl-double v5, v6, v8

    if-lez v5, :cond_1

    .line 281
    :cond_0
    :goto_0
    return v4

    .line 271
    :cond_1
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpg-double v5, v6, v8

    if-ltz v5, :cond_0

    .line 272
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v5, v6, v8

    if-gtz v5, :cond_0

    .line 273
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v5, v6, v8

    if-ltz v5, :cond_0

    .line 275
    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->computeA(DD)D

    move-result-wide v0

    .line 276
    .local v0, "A":D
    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->computeB(DD)D

    move-result-wide v2

    .line 277
    .local v2, "B":D
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    cmpl-double v5, v6, v0

    if-gtz v5, :cond_0

    .line 278
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    cmpg-double v5, v6, v0

    if-ltz v5, :cond_0

    .line 279
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    cmpl-double v5, v6, v2

    if-gtz v5, :cond_0

    .line 280
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    cmpg-double v5, v6, v2

    if-ltz v5, :cond_0

    .line 281
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/OctagonalEnvelope;)Z
    .locals 6
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/OctagonalEnvelope;

    .prologue
    const/4 v0, 0x0

    .line 255
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    return v0

    .line 257
    :cond_1
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 258
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 259
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 260
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 261
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 262
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 263
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 264
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 265
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isNull()Z
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    return v0
.end method

.method public setToNull()V
    .locals 2

    .prologue
    .line 133
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    iput-wide v0, p0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    .line 134
    return-void
.end method

.method public toGeometry(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 20
    .param p1, "geomFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 300
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->isNull()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 301
    const/4 v13, 0x0

    check-cast v13, Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v13

    .line 346
    :goto_0
    return-object v13

    .line 304
    :cond_0
    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-wide/from16 v0, v16

    invoke-direct {v5, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 305
    .local v5, "px00":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v6, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minX:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-wide/from16 v0, v16

    invoke-direct {v6, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 307
    .local v6, "px01":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v7, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-wide/from16 v0, v16

    invoke-direct {v7, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 308
    .local v7, "px10":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v8, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxX:D

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    move-wide/from16 v0, v16

    invoke-direct {v8, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 310
    .local v8, "px11":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v9, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minA:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v9, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 311
    .local v9, "py00":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v10, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxB:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minY:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v10, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 313
    .local v10, "py01":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v11, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->minB:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v11, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 314
    .local v11, "py10":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v12, Lcom/vividsolutions/jts/geom/Coordinate;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxA:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/OctagonalEnvelope;->maxY:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v12, v14, v15, v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 316
    .local v12, "py11":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v3

    .line 317
    .local v3, "pm":Lcom/vividsolutions/jts/geom/PrecisionModel;
    invoke-virtual {v3, v5}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 318
    invoke-virtual {v3, v6}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 319
    invoke-virtual {v3, v7}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 320
    invoke-virtual {v3, v8}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 321
    invoke-virtual {v3, v9}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 322
    invoke-virtual {v3, v10}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 323
    invoke-virtual {v3, v11}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 324
    invoke-virtual {v3, v12}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 326
    new-instance v2, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 327
    .local v2, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    const/4 v13, 0x0

    invoke-virtual {v2, v5, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 328
    const/4 v13, 0x0

    invoke-virtual {v2, v6, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 329
    const/4 v13, 0x0

    invoke-virtual {v2, v11, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 330
    const/4 v13, 0x0

    invoke-virtual {v2, v12, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 331
    const/4 v13, 0x0

    invoke-virtual {v2, v8, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 332
    const/4 v13, 0x0

    invoke-virtual {v2, v7, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 333
    const/4 v13, 0x0

    invoke-virtual {v2, v10, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 334
    const/4 v13, 0x0

    invoke-virtual {v2, v9, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 336
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    .line 337
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v13

    goto/16 :goto_0

    .line 339
    :cond_1
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_2

    .line 340
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 341
    .local v4, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v13

    goto/16 :goto_0

    .line 344
    .end local v4    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    const/4 v13, 0x0

    invoke-virtual {v2, v5, v13}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 345
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 346
    .restart local v4    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v13

    goto/16 :goto_0
.end method
