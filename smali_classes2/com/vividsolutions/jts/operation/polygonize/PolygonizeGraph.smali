.class Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;
.super Lcom/vividsolutions/jts/planargraph/PlanarGraph;
.source "PolygonizeGraph.java"


# instance fields
.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 0
    .param p1, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/PlanarGraph;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 104
    return-void
.end method

.method private computeDepthParity(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)V
    .locals 0
    .param p1, "de"    # Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .prologue
    .line 472
    return-void
.end method

.method private static computeNextCCWEdges(Lcom/vividsolutions/jts/planargraph/Node;J)V
    .locals 13
    .param p0, "node"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p1, "label"    # J

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v1

    .line 326
    .local v1, "deStar":Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;
    const/4 v3, 0x0

    .line 327
    .local v3, "firstOutDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    const/4 v7, 0x0

    .line 330
    .local v7, "prevInDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v2

    .line 332
    .local v2, "edges":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v4, v9, -0x1

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_6

    .line 333
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 334
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v8

    check-cast v8, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 336
    .local v8, "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    const/4 v6, 0x0

    .line 337
    .local v6, "outDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getLabel()J

    move-result-wide v10

    cmp-long v9, v10, p1

    if-nez v9, :cond_0

    move-object v6, v0

    .line 338
    :cond_0
    const/4 v5, 0x0

    .line 339
    .local v5, "inDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v8}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getLabel()J

    move-result-wide v10

    cmp-long v9, v10, p1

    if-nez v9, :cond_1

    move-object v5, v8

    .line 341
    :cond_1
    if-nez v6, :cond_3

    if-nez v5, :cond_3

    .line 332
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 343
    :cond_3
    if-eqz v5, :cond_4

    .line 344
    move-object v7, v5

    .line 347
    :cond_4
    if-eqz v6, :cond_2

    .line 348
    if-eqz v7, :cond_5

    .line 349
    invoke-virtual {v7, v6}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setNext(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)V

    .line 350
    const/4 v7, 0x0

    .line 352
    :cond_5
    if-nez v3, :cond_2

    .line 353
    move-object v3, v6

    goto :goto_1

    .line 356
    .end local v0    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v5    # "inDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v6    # "outDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v8    # "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_6
    if-eqz v7, :cond_7

    .line 357
    if-eqz v3, :cond_8

    const/4 v9, 0x1

    :goto_2
    invoke-static {v9}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 358
    invoke-virtual {v7, v3}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setNext(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)V

    .line 360
    :cond_7
    return-void

    .line 357
    :cond_8
    const/4 v9, 0x0

    goto :goto_2
.end method

.method private computeNextCWEdges()V
    .locals 3

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->nodeIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "iNode":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/Node;

    .line 146
    .local v1, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-static {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->computeNextCWEdges(Lcom/vividsolutions/jts/planargraph/Node;)V

    goto :goto_0

    .line 148
    .end local v1    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_0
    return-void
.end method

.method private static computeNextCWEdges(Lcom/vividsolutions/jts/planargraph/Node;)V
    .locals 7
    .param p0, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v0

    .line 296
    .local v0, "deStar":Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;
    const/4 v4, 0x0

    .line 297
    .local v4, "startDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    const/4 v3, 0x0

    .line 300
    .local v3, "prevDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 301
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 302
    .local v2, "outDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isMarked()Z

    move-result v6

    if-nez v6, :cond_0

    .line 304
    if-nez v4, :cond_1

    .line 305
    move-object v4, v2

    .line 306
    :cond_1
    if-eqz v3, :cond_2

    .line 307
    invoke-virtual {v3}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 308
    .local v5, "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v5, v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setNext(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)V

    .line 310
    .end local v5    # "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_2
    move-object v3, v2

    .line 311
    goto :goto_0

    .line 312
    .end local v2    # "outDE":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_3
    if-eqz v3, :cond_4

    .line 313
    invoke-virtual {v3}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 314
    .restart local v5    # "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v5, v4}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setNext(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)V

    .line 316
    .end local v5    # "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_4
    return-void
.end method

.method private convertMaximalToMinimalEdgeRings(Ljava/util/List;)V
    .locals 8
    .param p1, "ringEdges"    # Ljava/util/List;

    .prologue
    .line 158
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 159
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 160
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getLabel()J

    move-result-wide v4

    .line 161
    .local v4, "label":J
    invoke-static {v0, v4, v5}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->findIntersectionNodes(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;J)Ljava/util/List;

    move-result-object v3

    .line 163
    .local v3, "intNodes":Ljava/util/List;
    if-eqz v3, :cond_0

    .line 165
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "iNode":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 166
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/planargraph/Node;

    .line 167
    .local v6, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-static {v6, v4, v5}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->computeNextCCWEdges(Lcom/vividsolutions/jts/planargraph/Node;J)V

    goto :goto_0

    .line 170
    .end local v0    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v2    # "iNode":Ljava/util/Iterator;
    .end local v3    # "intNodes":Ljava/util/List;
    .end local v4    # "label":J
    .end local v6    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_1
    return-void
.end method

.method public static deleteAllEdges(Lcom/vividsolutions/jts/planargraph/Node;)V
    .locals 6
    .param p0, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    const/4 v5, 0x1

    .line 84
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v1

    .line 85
    .local v1, "edges":Ljava/util/List;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 86
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 87
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setMarked(Z)V

    .line 88
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 89
    .local v3, "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    if-eqz v3, :cond_0

    .line 90
    invoke-virtual {v3, v5}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setMarked(Z)V

    goto :goto_0

    .line 92
    .end local v0    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v3    # "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_1
    return-void
.end method

.method private static findDirEdgesInRing(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)Ljava/util/List;
    .locals 6
    .param p0, "startDE"    # Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 372
    move-object v0, p0

    .line 373
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 375
    .local v1, "edges":Ljava/util/List;
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getNext()Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    move-result-object v0

    .line 377
    if-eqz v0, :cond_2

    move v2, v3

    :goto_0
    const-string v5, "found null DE in ring"

    invoke-static {v2, v5}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 378
    if-eq v0, p0, :cond_1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isInRing()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    move v2, v3

    :goto_1
    const-string v5, "found DE already in ring"

    invoke-static {v2, v5}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 379
    if-ne v0, p0, :cond_0

    .line 381
    return-object v1

    :cond_2
    move v2, v4

    .line 377
    goto :goto_0

    :cond_3
    move v2, v4

    .line 378
    goto :goto_1
.end method

.method private findEdgeRing(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    .locals 6
    .param p1, "startDE"    # Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 386
    move-object v0, p1

    .line 387
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    new-instance v1, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 389
    .local v1, "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    :cond_0
    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;->add(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 390
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setRing(Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;)V

    .line 391
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getNext()Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_2

    move v2, v3

    :goto_0
    const-string v5, "found null DE in ring"

    invoke-static {v2, v5}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 393
    if-eq v0, p1, :cond_1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isInRing()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    move v2, v3

    :goto_1
    const-string v5, "found DE already in ring"

    invoke-static {v2, v5}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 394
    if-ne v0, p1, :cond_0

    .line 396
    return-object v1

    :cond_2
    move v2, v4

    .line 392
    goto :goto_0

    :cond_3
    move v2, v4

    .line 393
    goto :goto_1
.end method

.method private static findIntersectionNodes(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;J)Ljava/util/List;
    .locals 7
    .param p0, "startDE"    # Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .param p1, "label"    # J

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 181
    move-object v0, p0

    .line 182
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    const/4 v1, 0x0

    .line 184
    .local v1, "intNodes":Ljava/util/List;
    :cond_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getFromNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v2

    .line 185
    .local v2, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-static {v2, p1, p2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->getDegree(Lcom/vividsolutions/jts/planargraph/Node;J)I

    move-result v3

    if-le v3, v4, :cond_2

    .line 186
    if-nez v1, :cond_1

    .line 187
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "intNodes":Ljava/util/List;
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .restart local v1    # "intNodes":Ljava/util/List;
    :cond_1
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getNext()Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_4

    move v3, v4

    :goto_0
    const-string v6, "found null DE in ring"

    invoke-static {v3, v6}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 193
    if-eq v0, p0, :cond_3

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isInRing()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_3
    move v3, v4

    :goto_1
    const-string v6, "found DE already in ring"

    invoke-static {v3, v6}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 194
    if-ne v0, p0, :cond_0

    .line 196
    return-object v1

    :cond_4
    move v3, v5

    .line 192
    goto :goto_0

    :cond_5
    move v3, v5

    .line 193
    goto :goto_1
.end method

.method private static findLabeledEdgeRings(Ljava/util/Collection;)Ljava/util/List;
    .locals 10
    .param p0, "dirEdges"    # Ljava/util/Collection;

    .prologue
    .line 236
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 238
    .local v3, "edgeRingStarts":Ljava/util/List;
    const-wide/16 v0, 0x1

    .line 239
    .local v0, "currLabel":J
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 240
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 241
    .local v2, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isMarked()Z

    move-result v6

    if-nez v6, :cond_0

    .line 242
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getLabel()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    .line 244
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-static {v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->findDirEdgesInRing(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)Ljava/util/List;

    move-result-object v4

    .line 247
    .local v4, "edges":Ljava/util/List;
    invoke-static {v4, v0, v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->label(Ljava/util/Collection;J)V

    .line 248
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    .line 249
    goto :goto_0

    .line 250
    .end local v2    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v4    # "edges":Ljava/util/List;
    :cond_1
    return-object v3
.end method

.method private static getDegree(Lcom/vividsolutions/jts/planargraph/Node;J)I
    .locals 7
    .param p0, "node"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p1, "label"    # J

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v2

    .line 71
    .local v2, "edges":Ljava/util/List;
    const/4 v1, 0x0

    .line 72
    .local v1, "degree":I
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 73
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 74
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getLabel()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    .end local v0    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_1
    return v1
.end method

.method private static getDegreeNonDeleted(Lcom/vividsolutions/jts/planargraph/Node;)I
    .locals 5
    .param p0, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v2

    .line 60
    .local v2, "edges":Ljava/util/List;
    const/4 v1, 0x0

    .line 61
    .local v1, "degree":I
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 62
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 63
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isMarked()Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    .end local v0    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_1
    return v1
.end method

.method private getNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->findNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v0

    .line 133
    .local v0, "node":Lcom/vividsolutions/jts/planargraph/Node;
    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/vividsolutions/jts/planargraph/Node;

    .end local v0    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/planargraph/Node;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 136
    .restart local v0    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->add(Lcom/vividsolutions/jts/planargraph/Node;)V

    .line 138
    :cond_0
    return-object v0
.end method

.method private static label(Ljava/util/Collection;J)V
    .locals 3
    .param p0, "dirEdges"    # Ljava/util/Collection;
    .param p1, "label"    # J

    .prologue
    .line 288
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 290
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setLabel(J)V

    goto :goto_0

    .line 292
    .end local v0    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_0
    return-void
.end method


# virtual methods
.method public addEdge(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 12
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 112
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    invoke-static {v8}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->removeRepeatedPoints([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 115
    .local v4, "linePts":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v8, v4

    const/4 v9, 0x2

    if-lt v8, v9, :cond_0

    .line 117
    aget-object v7, v4, v10

    .line 118
    .local v7, "startPt":Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    aget-object v3, v4, v8

    .line 120
    .local v3, "endPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v7}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->getNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v6

    .line 121
    .local v6, "nStart":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->getNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v5

    .line 123
    .local v5, "nEnd":Lcom/vividsolutions/jts/planargraph/Node;
    new-instance v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    aget-object v8, v4, v11

    invoke-direct {v0, v6, v5, v8, v11}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;-><init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 124
    .local v0, "de0":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    new-instance v1, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    array-length v8, v4

    add-int/lit8 v8, v8, -0x2

    aget-object v8, v4, v8

    invoke-direct {v1, v5, v6, v8, v10}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;-><init>(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 125
    .local v1, "de1":Lcom/vividsolutions/jts/planargraph/DirectedEdge;
    new-instance v2, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;

    invoke-direct {v2, p1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;-><init>(Lcom/vividsolutions/jts/geom/LineString;)V

    .line 126
    .local v2, "edge":Lcom/vividsolutions/jts/planargraph/Edge;
    invoke-virtual {v2, v0, v1}, Lcom/vividsolutions/jts/planargraph/Edge;->setDirectedEdges(Lcom/vividsolutions/jts/planargraph/DirectedEdge;Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 127
    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->add(Lcom/vividsolutions/jts/planargraph/Edge;)V

    goto :goto_0
.end method

.method public computeDepthParity()V
    .locals 1

    .prologue
    .line 456
    :goto_0
    const/4 v0, 0x0

    .line 457
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    if-nez v0, :cond_0

    .line 458
    return-void

    .line 459
    :cond_0
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->computeDepthParity(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)V

    goto :goto_0
.end method

.method public deleteCutEdges()Ljava/util/List;
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 259
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->computeNextCWEdges()V

    .line 261
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->dirEdges:Ljava/util/Set;

    invoke-static {v5}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->findLabeledEdgeRings(Ljava/util/Collection;)Ljava/util/List;

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .local v0, "cutLines":Ljava/util/List;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->dirEdges:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 269
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 270
    .local v1, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isMarked()Z

    move-result v5

    if-nez v5, :cond_0

    .line 272
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 274
    .local v4, "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getLabel()J

    move-result-wide v6

    invoke-virtual {v4}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getLabel()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-nez v5, :cond_0

    .line 275
    invoke-virtual {v1, v10}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setMarked(Z)V

    .line 276
    invoke-virtual {v4, v10}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setMarked(Z)V

    .line 279
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;

    .line 280
    .local v2, "e":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 283
    .end local v1    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v2    # "e":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;
    .end local v4    # "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    :cond_1
    return-object v0
.end method

.method public deleteDangles()Ljava/util/Collection;
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 411
    invoke-virtual {p0, v11}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->findNodesOfDegree(I)Ljava/util/List;

    move-result-object v7

    .line 412
    .local v7, "nodesToRemove":Ljava/util/List;
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 414
    .local v0, "dangleLines":Ljava/util/Set;
    new-instance v6, Ljava/util/Stack;

    invoke-direct {v6}, Ljava/util/Stack;-><init>()V

    .line 415
    .local v6, "nodeStack":Ljava/util/Stack;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 416
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 419
    :cond_0
    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_3

    .line 420
    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/planargraph/Node;

    .line 422
    .local v4, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-static {v4}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->deleteAllEdges(Lcom/vividsolutions/jts/planargraph/Node;)V

    .line 423
    invoke-virtual {v4}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v10

    invoke-virtual {v10}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v5

    .line 424
    .local v5, "nodeOutEdges":Ljava/util/List;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 425
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 427
    .local v1, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v1, v11}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setMarked(Z)V

    .line 428
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getSym()Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    move-result-object v8

    check-cast v8, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 429
    .local v8, "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    if-eqz v8, :cond_2

    .line 430
    invoke-virtual {v8, v11}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->setMarked(Z)V

    .line 433
    :cond_2
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;

    .line 434
    .local v2, "e":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v10

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 436
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->getToNode()Lcom/vividsolutions/jts/planargraph/Node;

    move-result-object v9

    .line 438
    .local v9, "toNode":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-static {v9}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->getDegreeNonDeleted(Lcom/vividsolutions/jts/planargraph/Node;)I

    move-result v10

    if-ne v10, v11, :cond_1

    .line 439
    invoke-virtual {v6, v9}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 442
    .end local v1    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v2    # "e":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeEdge;
    .end local v4    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    .end local v5    # "nodeOutEdges":Ljava/util/List;
    .end local v8    # "sym":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v9    # "toNode":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_3
    return-object v0
.end method

.method public getEdgeRings()Ljava/util/List;
    .locals 8

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->computeNextCWEdges()V

    .line 209
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->dirEdges:Ljava/util/Set;

    const-wide/16 v6, -0x1

    invoke-static {v5, v6, v7}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->label(Ljava/util/Collection;J)V

    .line 210
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->dirEdges:Ljava/util/Set;

    invoke-static {v5}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->findLabeledEdgeRings(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v4

    .line 211
    .local v4, "maximalRings":Ljava/util/List;
    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->convertMaximalToMinimalEdgeRings(Ljava/util/List;)V

    .line 214
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v1, "edgeRingList":Ljava/util/List;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->dirEdges:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 216
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;

    .line 217
    .local v0, "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isMarked()Z

    move-result v5

    if-nez v5, :cond_0

    .line 218
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;->isInRing()Z

    move-result v5

    if-nez v5, :cond_0

    .line 220
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/polygonize/PolygonizeGraph;->findEdgeRing(Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;)Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;

    move-result-object v2

    .line 221
    .local v2, "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 223
    .end local v0    # "de":Lcom/vividsolutions/jts/operation/polygonize/PolygonizeDirectedEdge;
    .end local v2    # "er":Lcom/vividsolutions/jts/operation/polygonize/EdgeRing;
    :cond_1
    return-object v1
.end method
