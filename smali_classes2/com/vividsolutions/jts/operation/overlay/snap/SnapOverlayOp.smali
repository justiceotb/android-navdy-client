.class public Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;
.super Ljava/lang/Object;
.source "SnapOverlayOp.java"


# instance fields
.field private cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

.field private geom:[Lcom/vividsolutions/jts/geom/Geometry;

.field private snapTolerance:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g2"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 87
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 88
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->computeSnapTolerance()V

    .line 89
    return-void
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 155
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Snapped geometry is invalid"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method

.method private computeSnapTolerance()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->computeOverlaySnapTolerance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->snapTolerance:D

    .line 95
    return-void
.end method

.method public static difference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 72
    const/4 v0, 0x3

    invoke-static {p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public static intersection(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public static overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "opCode"    # I

    .prologue
    .line 56
    new-instance v0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 57
    .local v0, "op":Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;
    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->getResultGeometry(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private prepareResult(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->addCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 137
    return-object p1
.end method

.method private removeCommonBits([Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5
    .param p1, "geom"    # [Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 144
    new-instance v1, Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    invoke-direct {v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;-><init>()V

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    .line 145
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    aget-object v2, p1, v3

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 146
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    aget-object v2, p1, v4

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 147
    const/4 v1, 0x2

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Geometry;

    .line 148
    .local v0, "remGeom":[Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    aget-object v1, p1, v3

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    aput-object v1, v0, v3

    .line 149
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->cbr:Lcom/vividsolutions/jts/precision/CommonBitsRemover;

    aget-object v1, p1, v4

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/precision/CommonBitsRemover;->removeCommonBits(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    aput-object v1, v0, v4

    .line 150
    return-object v0
.end method

.method private selfSnap(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 107
    new-instance v1, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;

    invoke-direct {v1, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 108
    .local v1, "snapper0":Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;
    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->snapTolerance:D

    invoke-virtual {v1, p1, v2, v3}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->snapTo(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 111
    .local v0, "snapGeom":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v0
.end method

.method private snap([Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "geom"    # [Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->removeCommonBits([Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 121
    .local v0, "remGeom":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->snapTolerance:D

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/operation/overlay/snap/GeometrySnapper;->snap(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 131
    .local v1, "snapGeom":[Lcom/vividsolutions/jts/geom/Geometry;
    return-object v1
.end method

.method public static symDifference(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 77
    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public static union(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 67
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getResultGeometry(I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "opCode"    # I

    .prologue
    .line 100
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->geom:[Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->snap([Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 101
    .local v0, "prepGeom":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-static {v2, v3, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 102
    .local v1, "result":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapOverlayOp;->prepareResult(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    return-object v2
.end method
