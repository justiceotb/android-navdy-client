.class Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;
.super Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;
.source "RectangleIntersects.java"


# instance fields
.field private containsPoint:Z

.field private rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

.field private rectSeq:Lcom/vividsolutions/jts/geom/CoordinateSequence;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 1
    .param p1, "rectangle"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;-><init>()V

    .line 214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->containsPoint:Z

    .line 218
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->rectSeq:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .line 219
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 220
    return-void
.end method


# virtual methods
.method public containsPoint()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->containsPoint:Z

    return v0
.end method

.method protected isDone()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 263
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->containsPoint:Z

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected visit(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 237
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-nez v3, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 242
    .local v0, "elementEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v3, v0}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 246
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 247
    .local v2, "rectPt":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v3, 0x4

    if-ge v1, v3, :cond_0

    .line 248
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->rectSeq:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v3, v1, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 249
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 247
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object v3, p1

    .line 253
    check-cast v3, Lcom/vividsolutions/jts/geom/Polygon;

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/algorithm/locate/SimplePointInAreaLocator;->containsPointInPolygon(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Polygon;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 255
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/vividsolutions/jts/operation/predicate/GeometryContainsPointVisitor;->containsPoint:Z

    goto :goto_0
.end method
