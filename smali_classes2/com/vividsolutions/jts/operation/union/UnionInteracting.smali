.class public Lcom/vividsolutions/jts/operation/union/UnionInteracting;
.super Ljava/lang/Object;
.source "UnionInteracting.java"


# instance fields
.field private g0:Lcom/vividsolutions/jts/geom/Geometry;

.field private g1:Lcom/vividsolutions/jts/geom/Geometry;

.field private geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private interacts0:[Z

.field private interacts1:[Z


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    .line 71
    iput-object p2, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    .line 72
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 73
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts0:[Z

    .line 74
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts1:[Z

    .line 75
    return-void
.end method

.method private bufferUnion(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 112
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    .line 113
    .local v0, "factory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v1

    .line 114
    .local v1, "gColl":Lcom/vividsolutions/jts/geom/Geometry;
    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/vividsolutions/jts/geom/Geometry;->buffer(D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 115
    .local v2, "unionAll":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v2
.end method

.method private computeInteracting()V
    .locals 4

    .prologue
    .line 120
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 121
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 122
    .local v0, "elem":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts0:[Z

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->computeInteracting(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v3

    aput-boolean v3, v2, v1

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    .end local v0    # "elem":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method private computeInteracting(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 6
    .param p1, "elem0"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 128
    const/4 v3, 0x0

    .line 129
    .local v3, "interactsWithAny":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 130
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 131
    .local v0, "elem1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v2

    .line 132
    .local v2, "interacts":Z
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts1:[Z

    const/4 v5, 0x1

    aput-boolean v5, v4, v1

    .line 133
    :cond_0
    if-eqz v2, :cond_1

    .line 134
    const/4 v3, 0x1

    .line 129
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "elem1":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v2    # "interacts":Z
    :cond_2
    return v3
.end method

.method private extractElements(Lcom/vividsolutions/jts/geom/Geometry;[ZZ)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "interacts"    # [Z
    .param p3, "isInteracting"    # Z

    .prologue
    .line 142
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v1, "extractedGeoms":Ljava/util/List;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 144
    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 145
    .local v0, "elem":Lcom/vividsolutions/jts/geom/Geometry;
    aget-boolean v3, p2, v2

    if-ne v3, p3, :cond_0

    .line 146
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 148
    .end local v0    # "elem":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v3, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    return-object v3
.end method

.method public static union(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 55
    new-instance v0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;

    invoke-direct {v0, p0, p1}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 56
    .local v0, "uue":Lcom/vividsolutions/jts/operation/union/UnionInteracting;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->union()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public union()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 79
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->computeInteracting()V

    .line 83
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts0:[Z

    invoke-direct {p0, v6, v7, v9}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->extractElements(Lcom/vividsolutions/jts/geom/Geometry;[ZZ)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    .line 84
    .local v2, "int0":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts1:[Z

    invoke-direct {p0, v6, v7, v9}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->extractElements(Lcom/vividsolutions/jts/geom/Geometry;[ZZ)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    .line 89
    .local v3, "int1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 90
    :cond_0
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "found empty!"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 98
    :cond_1
    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Geometry;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 101
    .local v5, "union":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g0:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts0:[Z

    invoke-direct {p0, v6, v7, v8}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->extractElements(Lcom/vividsolutions/jts/geom/Geometry;[ZZ)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 102
    .local v0, "disjoint0":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->g1:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->interacts1:[Z

    invoke-direct {p0, v6, v7, v8}, Lcom/vividsolutions/jts/operation/union/UnionInteracting;->extractElements(Lcom/vividsolutions/jts/geom/Geometry;[ZZ)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 104
    .local v1, "disjoint1":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {v5, v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->combine(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    .line 106
    .local v4, "overallUnion":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v4
.end method
