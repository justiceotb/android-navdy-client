.class public abstract Lcom/vividsolutions/jts/planargraph/GraphComponent;
.super Ljava/lang/Object;
.source "GraphComponent.java"


# instance fields
.field private data:Ljava/lang/Object;

.field protected isMarked:Z

.field protected isVisited:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->isMarked:Z

    .line 106
    iput-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->isVisited:Z

    .line 110
    return-void
.end method

.method public static getComponentWithVisitedState(Ljava/util/Iterator;Z)Lcom/vividsolutions/jts/planargraph/GraphComponent;
    .locals 2
    .param p0, "i"    # Ljava/util/Iterator;
    .param p1, "visitedState"    # Z

    .prologue
    .line 97
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/GraphComponent;

    .line 99
    .local v0, "comp":Lcom/vividsolutions/jts/planargraph/GraphComponent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/GraphComponent;->isVisited()Z

    move-result v1

    if-ne v1, p1, :cond_0

    .line 102
    .end local v0    # "comp":Lcom/vividsolutions/jts/planargraph/GraphComponent;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setMarked(Ljava/util/Iterator;Z)V
    .locals 2
    .param p0, "i"    # Ljava/util/Iterator;
    .param p1, "marked"    # Z

    .prologue
    .line 81
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/GraphComponent;

    .line 83
    .local v0, "comp":Lcom/vividsolutions/jts/planargraph/GraphComponent;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/GraphComponent;->setMarked(Z)V

    goto :goto_0

    .line 85
    .end local v0    # "comp":Lcom/vividsolutions/jts/planargraph/GraphComponent;
    :cond_0
    return-void
.end method

.method public static setVisited(Ljava/util/Iterator;Z)V
    .locals 2
    .param p0, "i"    # Ljava/util/Iterator;
    .param p1, "visited"    # Z

    .prologue
    .line 67
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/planargraph/GraphComponent;

    .line 69
    .local v0, "comp":Lcom/vividsolutions/jts/planargraph/GraphComponent;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/GraphComponent;->setVisited(Z)V

    goto :goto_0

    .line 71
    .end local v0    # "comp":Lcom/vividsolutions/jts/planargraph/GraphComponent;
    :cond_0
    return-void
.end method


# virtual methods
.method public getContext()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public isMarked()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->isMarked:Z

    return v0
.end method

.method public abstract isRemoved()Z
.end method

.method public isVisited()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->isVisited:Z

    return v0
.end method

.method public setContext(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->data:Ljava/lang/Object;

    return-void
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->data:Ljava/lang/Object;

    return-void
.end method

.method public setMarked(Z)V
    .locals 0
    .param p1, "isMarked"    # Z

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->isMarked:Z

    return-void
.end method

.method public setVisited(Z)V
    .locals 0
    .param p1, "isVisited"    # Z

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/vividsolutions/jts/planargraph/GraphComponent;->isVisited:Z

    return-void
.end method
