.class public Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;
.super Ljava/lang/Object;
.source "InteriorPointPoint.java"


# instance fields
.field private centroid:Lcom/vividsolutions/jts/geom/Coordinate;

.field private interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

.field private minDistance:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->minDistance:D

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 54
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCentroid()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->centroid:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 55
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 56
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "point"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 77
    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->centroid:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 78
    .local v0, "dist":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->minDistance:D

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 79
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, p1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v2, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 80
    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->minDistance:D

    .line 82
    :cond_0
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 65
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v2, :cond_1

    .line 66
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 74
    :cond_0
    return-void

    .line 68
    :cond_1
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 69
    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 70
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 71
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getInteriorPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method
