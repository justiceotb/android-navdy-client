.class public Lcom/vividsolutions/jts/index/bintree/Node;
.super Lcom/vividsolutions/jts/index/bintree/NodeBase;
.source "Node.java"


# instance fields
.field private centre:D

.field private interval:Lcom/vividsolutions/jts/index/bintree/Interval;

.field private level:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/index/bintree/Interval;I)V
    .locals 4
    .param p1, "interval"    # Lcom/vividsolutions/jts/index/bintree/Interval;
    .param p2, "level"    # I

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/bintree/NodeBase;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    .line 72
    iput p2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->level:I

    .line 73
    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/bintree/Interval;->getMin()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/bintree/Interval;->getMax()D

    move-result-wide v2

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/bintree/Node;->centre:D

    .line 74
    return-void
.end method

.method public static createExpanded(Lcom/vividsolutions/jts/index/bintree/Node;Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/Node;
    .locals 3
    .param p0, "node"    # Lcom/vividsolutions/jts/index/bintree/Node;
    .param p1, "addInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 57
    new-instance v0, Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/index/bintree/Interval;-><init>(Lcom/vividsolutions/jts/index/bintree/Interval;)V

    .line 58
    .local v0, "expandInt":Lcom/vividsolutions/jts/index/bintree/Interval;
    if-eqz p0, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/index/bintree/Interval;->expandToInclude(Lcom/vividsolutions/jts/index/bintree/Interval;)V

    .line 60
    :cond_0
    invoke-static {v0}, Lcom/vividsolutions/jts/index/bintree/Node;->createNode(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/Node;

    move-result-object v1

    .line 61
    .local v1, "largerNode":Lcom/vividsolutions/jts/index/bintree/Node;
    if-eqz p0, :cond_1

    invoke-virtual {v1, p0}, Lcom/vividsolutions/jts/index/bintree/Node;->insert(Lcom/vividsolutions/jts/index/bintree/Node;)V

    .line 62
    :cond_1
    return-object v1
.end method

.method public static createNode(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/Node;
    .locals 4
    .param p0, "itemInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 48
    new-instance v0, Lcom/vividsolutions/jts/index/bintree/Key;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/index/bintree/Key;-><init>(Lcom/vividsolutions/jts/index/bintree/Interval;)V

    .line 51
    .local v0, "key":Lcom/vividsolutions/jts/index/bintree/Key;
    new-instance v1, Lcom/vividsolutions/jts/index/bintree/Node;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/bintree/Key;->getInterval()Lcom/vividsolutions/jts/index/bintree/Interval;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/bintree/Key;->getLevel()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/index/bintree/Node;-><init>(Lcom/vividsolutions/jts/index/bintree/Interval;I)V

    .line 52
    .local v1, "node":Lcom/vividsolutions/jts/index/bintree/Node;
    return-object v1
.end method

.method private createSubnode(I)Lcom/vividsolutions/jts/index/bintree/Node;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 155
    const-wide/16 v2, 0x0

    .line 156
    .local v2, "min":D
    const-wide/16 v0, 0x0

    .line 158
    .local v0, "max":D
    packed-switch p1, :pswitch_data_0

    .line 168
    :goto_0
    new-instance v5, Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-direct {v5, v2, v3, v0, v1}, Lcom/vividsolutions/jts/index/bintree/Interval;-><init>(DD)V

    .line 169
    .local v5, "subInt":Lcom/vividsolutions/jts/index/bintree/Interval;
    new-instance v4, Lcom/vividsolutions/jts/index/bintree/Node;

    iget v6, p0, Lcom/vividsolutions/jts/index/bintree/Node;->level:I

    add-int/lit8 v6, v6, -0x1

    invoke-direct {v4, v5, v6}, Lcom/vividsolutions/jts/index/bintree/Node;-><init>(Lcom/vividsolutions/jts/index/bintree/Interval;I)V

    .line 170
    .local v4, "node":Lcom/vividsolutions/jts/index/bintree/Node;
    return-object v4

    .line 160
    .end local v4    # "node":Lcom/vividsolutions/jts/index/bintree/Node;
    .end local v5    # "subInt":Lcom/vividsolutions/jts/index/bintree/Interval;
    :pswitch_0
    iget-object v6, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/index/bintree/Interval;->getMin()D

    move-result-wide v2

    .line 161
    iget-wide v0, p0, Lcom/vividsolutions/jts/index/bintree/Node;->centre:D

    .line 162
    goto :goto_0

    .line 164
    :pswitch_1
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->centre:D

    .line 165
    iget-object v6, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/index/bintree/Interval;->getMax()D

    move-result-wide v0

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSubnode(I)Lcom/vividsolutions/jts/index/bintree/Node;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 145
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Node;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Node;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/index/bintree/Node;->createSubnode(I)Lcom/vividsolutions/jts/index/bintree/Node;

    move-result-object v1

    aput-object v1, v0, p1

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Node;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v0, v0, p1

    return-object v0
.end method


# virtual methods
.method public find(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/NodeBase;
    .locals 4
    .param p1, "searchInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 111
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->centre:D

    invoke-static {p1, v2, v3}, Lcom/vividsolutions/jts/index/bintree/Node;->getSubnodeIndex(Lcom/vividsolutions/jts/index/bintree/Interval;D)I

    move-result v1

    .line 112
    .local v1, "subnodeIndex":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 120
    .end local p0    # "this":Lcom/vividsolutions/jts/index/bintree/Node;
    :cond_0
    :goto_0
    return-object p0

    .line 114
    .restart local p0    # "this":Lcom/vividsolutions/jts/index/bintree/Node;
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    .line 116
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v0, v2, v1

    .line 117
    .local v0, "node":Lcom/vividsolutions/jts/index/bintree/Node;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/index/bintree/Node;->find(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/NodeBase;

    move-result-object p0

    goto :goto_0
.end method

.method public getInterval()Lcom/vividsolutions/jts/index/bintree/Interval;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    return-object v0
.end method

.method public getNode(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/Node;
    .locals 4
    .param p1, "searchInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 92
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->centre:D

    invoke-static {p1, v2, v3}, Lcom/vividsolutions/jts/index/bintree/Node;->getSubnodeIndex(Lcom/vividsolutions/jts/index/bintree/Interval;D)I

    move-result v1

    .line 94
    .local v1, "subnodeIndex":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 96
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/index/bintree/Node;->getSubnode(I)Lcom/vividsolutions/jts/index/bintree/Node;

    move-result-object v0

    .line 98
    .local v0, "node":Lcom/vividsolutions/jts/index/bintree/Node;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/index/bintree/Node;->getNode(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/Node;

    move-result-object p0

    .line 101
    .end local v0    # "node":Lcom/vividsolutions/jts/index/bintree/Node;
    .end local p0    # "this":Lcom/vividsolutions/jts/index/bintree/Node;
    :cond_0
    return-object p0
.end method

.method insert(Lcom/vividsolutions/jts/index/bintree/Node;)V
    .locals 6
    .param p1, "node"    # Lcom/vividsolutions/jts/index/bintree/Node;

    .prologue
    .line 125
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    iget-object v3, p1, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/index/bintree/Interval;->contains(Lcom/vividsolutions/jts/index/bintree/Interval;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 126
    iget-object v2, p1, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    iget-wide v4, p0, Lcom/vividsolutions/jts/index/bintree/Node;->centre:D

    invoke-static {v2, v4, v5}, Lcom/vividsolutions/jts/index/bintree/Node;->getSubnodeIndex(Lcom/vividsolutions/jts/index/bintree/Interval;D)I

    move-result v1

    .line 127
    .local v1, "index":I
    iget v2, p1, Lcom/vividsolutions/jts/index/bintree/Node;->level:I

    iget v3, p0, Lcom/vividsolutions/jts/index/bintree/Node;->level:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_2

    .line 128
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aput-object p1, v2, v1

    .line 137
    :goto_1
    return-void

    .line 125
    .end local v1    # "index":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 133
    .restart local v1    # "index":I
    :cond_2
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/index/bintree/Node;->createSubnode(I)Lcom/vividsolutions/jts/index/bintree/Node;

    move-result-object v0

    .line 134
    .local v0, "childNode":Lcom/vividsolutions/jts/index/bintree/Node;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/index/bintree/Node;->insert(Lcom/vividsolutions/jts/index/bintree/Node;)V

    .line 135
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Node;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aput-object v0, v2, v1

    goto :goto_1
.end method

.method protected isSearchMatch(Lcom/vividsolutions/jts/index/bintree/Interval;)Z
    .locals 1
    .param p1, "itemInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Node;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/index/bintree/Interval;->overlaps(Lcom/vividsolutions/jts/index/bintree/Interval;)Z

    move-result v0

    return v0
.end method
