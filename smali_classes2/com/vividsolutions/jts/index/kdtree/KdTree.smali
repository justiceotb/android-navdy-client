.class public Lcom/vividsolutions/jts/index/kdtree/KdTree;
.super Ljava/lang/Object;
.source "KdTree.java"


# instance fields
.field private last:Lcom/vividsolutions/jts/index/kdtree/KdNode;

.field private numberOfNodes:J

.field private root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

.field private tolerance:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 67
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/index/kdtree/KdTree;-><init>(D)V

    .line 68
    return-void
.end method

.method public constructor <init>(D)V
    .locals 1
    .param p1, "tolerance"    # D

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 57
    iput-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->last:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 79
    iput-wide p1, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->tolerance:D

    .line 80
    return-void
.end method

.method private queryNode(Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/geom/Envelope;ZLjava/util/List;)V
    .locals 16
    .param p1, "currentNode"    # Lcom/vividsolutions/jts/index/kdtree/KdNode;
    .param p2, "bottomNode"    # Lcom/vividsolutions/jts/index/kdtree/KdNode;
    .param p3, "queryEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p4, "odd"    # Z
    .param p5, "result"    # Ljava/util/List;

    .prologue
    .line 174
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    if-ne v0, v1, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    if-eqz p4, :cond_4

    .line 181
    invoke-virtual/range {p3 .. p3}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v12

    .line 182
    .local v12, "min":D
    invoke-virtual/range {p3 .. p3}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v10

    .line 183
    .local v10, "max":D
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getX()D

    move-result-wide v8

    .line 189
    .local v8, "discriminant":D
    :goto_1
    cmpg-double v2, v12, v8

    if-gez v2, :cond_5

    const/4 v14, 0x1

    .line 190
    .local v14, "searchLeft":Z
    :goto_2
    cmpg-double v2, v8, v10

    if-gtz v2, :cond_6

    const/4 v15, 0x1

    .line 192
    .local v15, "searchRight":Z
    :goto_3
    if-eqz v14, :cond_2

    .line 193
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getLeft()Lcom/vividsolutions/jts/index/kdtree/KdNode;

    move-result-object v3

    if-nez p4, :cond_7

    const/4 v6, 0x1

    :goto_4
    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/vividsolutions/jts/index/kdtree/KdTree;->queryNode(Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/geom/Envelope;ZLjava/util/List;)V

    .line 195
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 196
    move-object/from16 v0, p5

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_3
    if-eqz v15, :cond_0

    .line 199
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getRight()Lcom/vividsolutions/jts/index/kdtree/KdNode;

    move-result-object v3

    if-nez p4, :cond_8

    const/4 v6, 0x1

    :goto_5
    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/vividsolutions/jts/index/kdtree/KdTree;->queryNode(Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/geom/Envelope;ZLjava/util/List;)V

    goto :goto_0

    .line 185
    .end local v8    # "discriminant":D
    .end local v10    # "max":D
    .end local v12    # "min":D
    .end local v14    # "searchLeft":Z
    .end local v15    # "searchRight":Z
    :cond_4
    invoke-virtual/range {p3 .. p3}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v12

    .line 186
    .restart local v12    # "min":D
    invoke-virtual/range {p3 .. p3}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v10

    .line 187
    .restart local v10    # "max":D
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getY()D

    move-result-wide v8

    .restart local v8    # "discriminant":D
    goto :goto_1

    .line 189
    :cond_5
    const/4 v14, 0x0

    goto :goto_2

    .line 190
    .restart local v14    # "searchLeft":Z
    :cond_6
    const/4 v15, 0x0

    goto :goto_3

    .line 193
    .restart local v15    # "searchRight":Z
    :cond_7
    const/4 v6, 0x0

    goto :goto_4

    .line 199
    :cond_8
    const/4 v6, 0x0

    goto :goto_5
.end method


# virtual methods
.method public insert(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/index/kdtree/KdNode;
    .locals 1
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/index/kdtree/KdTree;->insert(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)Lcom/vividsolutions/jts/index/kdtree/KdNode;

    move-result-object v0

    return-object v0
.end method

.method public insert(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)Lcom/vividsolutions/jts/index/kdtree/KdNode;
    .locals 12
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 116
    iget-object v8, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    if-nez v8, :cond_0

    .line 117
    new-instance v6, Lcom/vividsolutions/jts/index/kdtree/KdNode;

    invoke-direct {v6, p1, p2}, Lcom/vividsolutions/jts/index/kdtree/KdNode;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    iput-object v6, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 118
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 169
    :goto_0
    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 122
    .local v0, "currentNode":Lcom/vividsolutions/jts/index/kdtree/KdNode;
    iget-object v4, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    .line 123
    .local v4, "leafNode":Lcom/vividsolutions/jts/index/kdtree/KdNode;
    const/4 v3, 0x1

    .line 124
    .local v3, "isOddLevel":Z
    const/4 v2, 0x1

    .line 131
    .local v2, "isLessThan":Z
    :goto_1
    iget-object v8, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->last:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    if-eq v0, v8, :cond_8

    .line 133
    if-eqz v0, :cond_2

    .line 134
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    iget-wide v10, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->tolerance:D

    cmpg-double v8, v8, v10

    if-gtz v8, :cond_1

    move v1, v6

    .line 138
    .local v1, "isInTolerance":Z
    :goto_2
    if-eqz v1, :cond_2

    .line 139
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->increment()V

    goto :goto_0

    .end local v1    # "isInTolerance":Z
    :cond_1
    move v1, v7

    .line 134
    goto :goto_2

    .line 144
    :cond_2
    if-eqz v3, :cond_4

    .line 145
    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getX()D

    move-result-wide v10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_3

    move v2, v6

    .line 149
    :goto_3
    move-object v4, v0

    .line 150
    if-eqz v2, :cond_6

    .line 151
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getLeft()Lcom/vividsolutions/jts/index/kdtree/KdNode;

    move-result-object v0

    .line 156
    :goto_4
    if-nez v3, :cond_7

    move v3, v6

    :goto_5
    goto :goto_1

    :cond_3
    move v2, v7

    .line 145
    goto :goto_3

    .line 147
    :cond_4
    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getY()D

    move-result-wide v10

    cmpg-double v8, v8, v10

    if-gez v8, :cond_5

    move v2, v6

    :goto_6
    goto :goto_3

    :cond_5
    move v2, v7

    goto :goto_6

    .line 153
    :cond_6
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->getRight()Lcom/vividsolutions/jts/index/kdtree/KdNode;

    move-result-object v0

    goto :goto_4

    :cond_7
    move v3, v7

    .line 156
    goto :goto_5

    .line 160
    :cond_8
    iget-wide v6, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->numberOfNodes:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->numberOfNodes:J

    .line 161
    new-instance v5, Lcom/vividsolutions/jts/index/kdtree/KdNode;

    invoke-direct {v5, p1, p2}, Lcom/vividsolutions/jts/index/kdtree/KdNode;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    .line 162
    .local v5, "node":Lcom/vividsolutions/jts/index/kdtree/KdNode;
    iget-object v6, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->last:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->setLeft(Lcom/vividsolutions/jts/index/kdtree/KdNode;)V

    .line 163
    iget-object v6, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->last:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->setRight(Lcom/vividsolutions/jts/index/kdtree/KdNode;)V

    .line 164
    if-eqz v2, :cond_9

    .line 165
    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->setLeft(Lcom/vividsolutions/jts/index/kdtree/KdNode;)V

    :goto_7
    move-object v0, v5

    .line 169
    goto :goto_0

    .line 167
    :cond_9
    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/index/kdtree/KdNode;->setRight(Lcom/vividsolutions/jts/index/kdtree/KdNode;)V

    goto :goto_7
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public query(Lcom/vividsolutions/jts/geom/Envelope;)Ljava/util/List;
    .locals 6
    .param p1, "queryEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 212
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 213
    .local v5, "result":Ljava/util/List;
    iget-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    iget-object v2, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->last:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    const/4 v4, 0x1

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/index/kdtree/KdTree;->queryNode(Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/geom/Envelope;ZLjava/util/List;)V

    .line 214
    return-object v5
.end method

.method public query(Lcom/vividsolutions/jts/geom/Envelope;Ljava/util/List;)V
    .locals 6
    .param p1, "queryEnv"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "result"    # Ljava/util/List;

    .prologue
    .line 226
    iget-object v1, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->root:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    iget-object v2, p0, Lcom/vividsolutions/jts/index/kdtree/KdTree;->last:Lcom/vividsolutions/jts/index/kdtree/KdNode;

    const/4 v4, 0x1

    move-object v0, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/index/kdtree/KdTree;->queryNode(Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/index/kdtree/KdNode;Lcom/vividsolutions/jts/geom/Envelope;ZLjava/util/List;)V

    .line 227
    return-void
.end method
