.class public Lcom/zendesk/belvedere/BelvedereConfig$Builder;
.super Ljava/lang/Object;
.source "BelvedereConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/belvedere/BelvedereConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAllowMultiple:Z

.field private mBelvedereLogger:Lcom/zendesk/belvedere/BelvedereLogger;

.field private mCameraRequestCodeEnd:I

.field private mCameraRequestCodeStart:I

.field private mContentType:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDebugEnabled:Z

.field private mDirectory:Ljava/lang/String;

.field private mGalleryRequestCode:I

.field private mSources:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/zendesk/belvedere/BelvedereSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-string v0, "belvedere-data"

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mDirectory:Ljava/lang/String;

    .line 79
    const/16 v0, 0x642

    iput v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mGalleryRequestCode:I

    .line 80
    const/16 v0, 0x643

    iput v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mCameraRequestCodeStart:I

    .line 81
    const/16 v0, 0x675

    iput v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mCameraRequestCodeEnd:I

    .line 83
    iput-boolean v4, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mAllowMultiple:Z

    .line 85
    const-string v0, "*/*"

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mContentType:Ljava/lang/String;

    .line 86
    new-instance v0, Lcom/zendesk/belvedere/DefaultLogger;

    invoke-direct {v0}, Lcom/zendesk/belvedere/DefaultLogger;-><init>()V

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mBelvedereLogger:Lcom/zendesk/belvedere/BelvedereLogger;

    .line 87
    iput-boolean v3, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mDebugEnabled:Z

    .line 88
    new-instance v0, Ljava/util/TreeSet;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/zendesk/belvedere/BelvedereSource;

    sget-object v2, Lcom/zendesk/belvedere/BelvedereSource;->Camera:Lcom/zendesk/belvedere/BelvedereSource;

    aput-object v2, v1, v3

    sget-object v2, Lcom/zendesk/belvedere/BelvedereSource;->Gallery:Lcom/zendesk/belvedere/BelvedereSource;

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mSources:Ljava/util/TreeSet;

    .line 91
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mContext:Landroid/content/Context;

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mDirectory:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mGalleryRequestCode:I

    return v0
.end method

.method static synthetic access$200(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mCameraRequestCodeStart:I

    return v0
.end method

.method static synthetic access$300(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mCameraRequestCodeEnd:I

    return v0
.end method

.method static synthetic access$400(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mAllowMultiple:Z

    return v0
.end method

.method static synthetic access$500(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)Lcom/zendesk/belvedere/BelvedereLogger;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mBelvedereLogger:Lcom/zendesk/belvedere/BelvedereLogger;

    return-object v0
.end method

.method static synthetic access$700(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)Ljava/util/TreeSet;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mSources:Ljava/util/TreeSet;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/zendesk/belvedere/Belvedere;
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mBelvedereLogger:Lcom/zendesk/belvedere/BelvedereLogger;

    iget-boolean v1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mDebugEnabled:Z

    invoke-interface {v0, v1}, Lcom/zendesk/belvedere/BelvedereLogger;->setLoggable(Z)V

    .line 237
    new-instance v0, Lcom/zendesk/belvedere/Belvedere;

    iget-object v1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/zendesk/belvedere/BelvedereConfig;

    invoke-direct {v2, p0}, Lcom/zendesk/belvedere/BelvedereConfig;-><init>(Lcom/zendesk/belvedere/BelvedereConfig$Builder;)V

    invoke-direct {v0, v1, v2}, Lcom/zendesk/belvedere/Belvedere;-><init>(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereConfig;)V

    return-object v0
.end method

.method public withAllowMultiple(Z)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 0
    .param p1, "allowMultiple"    # Z

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mAllowMultiple:Z

    .line 153
    return-object p0
.end method

.method public withCameraRequestCode(II)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 2
    .param p1, "startRequestCode"    # I
    .param p2, "endRequestCode"    # I

    .prologue
    .line 125
    sub-int v0, p2, p1

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The following formula must be apply for the given arguments: (endRequestCode - startRequestCode) >= 5"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :cond_0
    iput p1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mCameraRequestCodeStart:I

    .line 133
    iput p2, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mCameraRequestCodeEnd:I

    .line 134
    return-object p0
.end method

.method public withContentType(Ljava/lang/String;)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 0
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mContentType:Ljava/lang/String;

    .line 170
    return-object p0
.end method

.method public withCustomLogger(Lcom/zendesk/belvedere/BelvedereLogger;)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 2
    .param p1, "belvedereLogger"    # Lcom/zendesk/belvedere/BelvedereLogger;

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 185
    iput-object p1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mBelvedereLogger:Lcom/zendesk/belvedere/BelvedereLogger;

    .line 186
    return-object p0

    .line 189
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid logger provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public withDebug(Z)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 225
    iput-boolean p1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mDebugEnabled:Z

    .line 226
    return-object p0
.end method

.method public withGalleryRequestCode(I)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 0
    .param p1, "requestCode"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mGalleryRequestCode:I

    .line 105
    return-object p0
.end method

.method public varargs withSource([Lcom/zendesk/belvedere/BelvedereSource;)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 2
    .param p1, "sources"    # [Lcom/zendesk/belvedere/BelvedereSource;

    .prologue
    .line 207
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Please provide at least one source"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_1
    new-instance v0, Ljava/util/TreeSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;->mSources:Ljava/util/TreeSet;

    .line 212
    return-object p0
.end method
