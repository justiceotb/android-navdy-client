.class public Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;
.super Lcom/zendesk/sdk/rating/impl/BaseRateMyAppButton;
.source "RateMyAppSendFeedbackButton.java"


# static fields
.field public static final FEEDBACK_DIALOG_TAG:Ljava/lang/String; = "feedback"


# instance fields
.field private final transient mConnector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

.field private transient mFeedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

.field private final transient mFragmentActivity:Landroid/support/v4/app/FragmentActivity;

.field private final mLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/zendesk/sdk/feedback/FeedbackConnector;)V
    .locals 1
    .param p1, "fragmentActivity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "connector"    # Lcom/zendesk/sdk/feedback/FeedbackConnector;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/zendesk/sdk/rating/impl/BaseRateMyAppButton;-><init>()V

    .line 45
    sget v0, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_negative_action_label:I

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mLabel:Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mFragmentActivity:Landroid/support/v4/app/FragmentActivity;

    .line 47
    iput-object p2, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mConnector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)Landroid/support/v4/app/FragmentActivity;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mFragmentActivity:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)Lcom/zendesk/sdk/feedback/FeedbackConnector;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mConnector:Lcom/zendesk/sdk/feedback/FeedbackConnector;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)Lcom/zendesk/sdk/network/SubmissionListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mFeedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 116
    sget v0, Lcom/zendesk/sdk/R$id;->rma_feedback_button:I

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton$1;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton$1;-><init>(Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;)V

    return-object v0
.end method

.method public setFeedbackListener(Lcom/zendesk/sdk/network/SubmissionListener;)V
    .locals 0
    .param p1, "feedbackListener"    # Lcom/zendesk/sdk/network/SubmissionListener;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppSendFeedbackButton;->mFeedbackListener:Lcom/zendesk/sdk/network/SubmissionListener;

    .line 112
    return-void
.end method

.method public shouldDismissDialog()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method
