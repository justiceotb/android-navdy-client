.class Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton$1;
.super Ljava/lang/Object;
.source "RateMyAppDontAskAgainButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;->getOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton$1;->this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppDontAskAgainButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 32
    new-instance v0, Lcom/zendesk/sdk/storage/RateMyAppStorage;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/storage/RateMyAppStorage;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->setDontShowAgain()V

    .line 33
    return-void
.end method
