.class public Lcom/zendesk/sdk/rating/impl/RateMyAppRules;
.super Ljava/lang/Object;
.source "RateMyAppRules.java"


# instance fields
.field private mRules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/rating/RateMyAppRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lcom/zendesk/sdk/rating/RateMyAppRule;)V
    .locals 1
    .param p1, "rules"    # [Lcom/zendesk/sdk/rating/RateMyAppRule;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->isEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;->mRules:Ljava/util/List;

    .line 30
    :cond_0
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;->mRules:Ljava/util/List;

    .line 31
    return-void
.end method


# virtual methods
.method public checkRules()Z
    .locals 3

    .prologue
    .line 40
    iget-object v1, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppRules;->mRules:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/rating/RateMyAppRule;

    .line 41
    .local v0, "rule":Lcom/zendesk/sdk/rating/RateMyAppRule;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/zendesk/sdk/rating/RateMyAppRule;->permitsShowOfDialog()Z

    move-result v2

    if-nez v2, :cond_0

    .line 42
    const/4 v1, 0x0

    .line 46
    .end local v0    # "rule":Lcom/zendesk/sdk/rating/RateMyAppRule;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
