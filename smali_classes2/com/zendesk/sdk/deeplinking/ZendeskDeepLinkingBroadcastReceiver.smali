.class public Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ZendeskDeepLinkingBroadcastReceiver.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingBroadcastReceiver;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 31
    sget-object v2, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingBroadcastReceiver$1;->$SwitchMap$com$zendesk$sdk$deeplinking$targets$DeepLinkType:[I

    invoke-static {p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->getDeepLinkType(Landroid/content/Intent;)Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 46
    :goto_0
    return-void

    .line 33
    :pswitch_0
    new-instance v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;

    invoke-direct {v1}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetRequest;-><init>()V

    .line 34
    .local v1, "deepLinkingTargetRequest":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;
    invoke-virtual {v1, p1, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->execute(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 38
    .end local v1    # "deepLinkingTargetRequest":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;
    :pswitch_1
    new-instance v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;

    invoke-direct {v0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;-><init>()V

    .line 39
    .local v0, "deepLinkingTargetArticle":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;
    invoke-virtual {v0, p1, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;->execute(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 43
    .end local v0    # "deepLinkingTargetArticle":Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;
    :pswitch_2
    sget-object v2, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingBroadcastReceiver;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Unknown intent"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
