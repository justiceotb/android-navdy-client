.class public Lcom/zendesk/sdk/support/SupportUiConfig;
.super Ljava/lang/Object;
.source "SupportUiConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/zendesk/sdk/support/SupportUiConfig;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOG_TAG:Ljava/lang/String; = "SupportUiConfig"


# instance fields
.field private addListPaddingBottom:Z

.field private categoryIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private collapseCategories:Z

.field private labelNames:[Ljava/lang/String;

.field private sectionIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private showContactUsButton:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/zendesk/sdk/support/SupportUiConfig$1;

    invoke-direct {v0}, Lcom/zendesk/sdk/support/SupportUiConfig$1;-><init>()V

    sput-object v0, Lcom/zendesk/sdk/support/SupportUiConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->categoryIds:Ljava/util/List;

    .line 109
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->categoryIds:Ljava/util/List;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->sectionIds:Ljava/util/List;

    .line 111
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->sectionIds:Ljava/util/List;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->labelNames:[Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v0

    .line 115
    .local v0, "parcelledBools":[Z
    array-length v1, v0

    packed-switch v1, :pswitch_data_0

    .line 123
    :goto_0
    return-void

    .line 117
    :pswitch_0
    const/4 v1, 0x2

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->collapseCategories:Z

    .line 119
    :pswitch_1
    const/4 v1, 0x1

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->addListPaddingBottom:Z

    .line 121
    :pswitch_2
    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->showContactUsButton:Z

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private constructor <init>(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->access$000(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->categoryIds:Ljava/util/List;

    .line 55
    invoke-static {p1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->access$100(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->sectionIds:Ljava/util/List;

    .line 56
    invoke-static {p1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->access$200(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->labelNames:[Ljava/lang/String;

    .line 57
    invoke-static {p1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->access$300(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->showContactUsButton:Z

    .line 58
    invoke-static {p1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->access$400(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->addListPaddingBottom:Z

    .line 59
    invoke-static {p1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->access$500(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->collapseCategories:Z

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;Lcom/zendesk/sdk/support/SupportUiConfig$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .param p2, "x1"    # Lcom/zendesk/sdk/support/SupportUiConfig$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/support/SupportUiConfig;-><init>(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public getCategoryIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->categoryIds:Ljava/util/List;

    return-object v0
.end method

.method public getLabelNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->labelNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getSectionIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->sectionIds:Ljava/util/List;

    return-object v0
.end method

.method public isAddListPaddingBottom()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->addListPaddingBottom:Z

    return v0
.end method

.method public isCollapseCategories()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->collapseCategories:Z

    return v0
.end method

.method isShowContactUsButton()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->showContactUsButton:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "flags"    # I

    .prologue
    .line 132
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->categoryIds:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 133
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->sectionIds:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 134
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->labelNames:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x3

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->showContactUsButton:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->addListPaddingBottom:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/zendesk/sdk/support/SupportUiConfig;->collapseCategories:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 136
    return-void
.end method
