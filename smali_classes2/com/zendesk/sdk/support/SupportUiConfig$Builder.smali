.class Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
.super Ljava/lang/Object;
.source "SupportUiConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/SupportUiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field private addListPaddingBottom:Z

.field private categoryIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private collapseCategories:Z

.field private labelNames:[Ljava/lang/String;

.field private sectionIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private showContactUsButton:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->categoryIds:Ljava/util/List;

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->sectionIds:Ljava/util/List;

    .line 65
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->labelNames:[Ljava/lang/String;

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->showContactUsButton:Z

    .line 67
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->showContactUsButton:Z

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->addListPaddingBottom:Z

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->categoryIds:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->sectionIds:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->labelNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->showContactUsButton:Z

    return v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->addListPaddingBottom:Z

    return v0
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->collapseCategories:Z

    return v0
.end method


# virtual methods
.method build()Lcom/zendesk/sdk/support/SupportUiConfig;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Lcom/zendesk/sdk/support/SupportUiConfig;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/zendesk/sdk/support/SupportUiConfig;-><init>(Lcom/zendesk/sdk/support/SupportUiConfig$Builder;Lcom/zendesk/sdk/support/SupportUiConfig$1;)V

    return-object v0
.end method

.method withAddListPaddingBottom(Z)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .locals 0
    .param p1, "addListPaddingBottom"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->addListPaddingBottom:Z

    .line 94
    return-object p0
.end method

.method withCategoryIds(Ljava/util/List;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/zendesk/sdk/support/SupportUiConfig$Builder;"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->categoryIds:Ljava/util/List;

    .line 72
    return-object p0
.end method

.method withCollapseCategories(Z)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .locals 0
    .param p1, "collapseCategories"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->collapseCategories:Z

    .line 99
    return-object p0
.end method

.method withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .locals 1
    .param p1, "labelNames"    # [Ljava/lang/String;

    .prologue
    .line 81
    if-eqz p1, :cond_0

    .line 82
    invoke-virtual {p1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->labelNames:[Ljava/lang/String;

    .line 84
    :cond_0
    return-object p0
.end method

.method withSectionIds(Ljava/util/List;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/zendesk/sdk/support/SupportUiConfig$Builder;"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "sectionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->sectionIds:Ljava/util/List;

    .line 77
    return-object p0
.end method

.method withShowContactUsButton(Z)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .locals 0
    .param p1, "showContactUsButton"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->showContactUsButton:Z

    .line 89
    return-object p0
.end method
