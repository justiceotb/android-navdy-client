.class Lcom/zendesk/sdk/support/SupportPresenter$3;
.super Ljava/lang/Object;
.source "SupportPresenter.java"

# interfaces
.implements Lcom/zendesk/sdk/network/RetryAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/SupportPresenter;->onErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/support/SupportPresenter;

.field final synthetic val$action:Lcom/zendesk/sdk/network/RetryAction;

.field final synthetic val$errorType:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/SupportPresenter;Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/SupportPresenter;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    iput-object p2, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->val$errorType:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    iput-object p3, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->val$action:Lcom/zendesk/sdk/network/RetryAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRetry()V
    .locals 3

    .prologue
    .line 255
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->isShowingHelp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 257
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->this$0:Lcom/zendesk/sdk/support/SupportPresenter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/SupportPresenter;->access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->val$errorType:Lcom/zendesk/sdk/support/SupportMvp$ErrorType;

    iget-object v2, p0, Lcom/zendesk/sdk/support/SupportPresenter$3;->val$action:Lcom/zendesk/sdk/network/RetryAction;

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/support/SupportMvp$View;->showErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V

    .line 259
    :cond_0
    return-void
.end method
