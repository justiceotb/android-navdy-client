.class Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "NetworkAwareActionbarActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->registerForNetworkCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;->this$0:Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

    iput-object p2, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 2
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3$1;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3$1;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 172
    return-void
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 2
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3$2;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3$2;-><init>(Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity$3;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 182
    return-void
.end method
