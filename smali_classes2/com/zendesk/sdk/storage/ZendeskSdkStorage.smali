.class Lcom/zendesk/sdk/storage/ZendeskSdkStorage;
.super Ljava/lang/Object;
.source "ZendeskSdkStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/SdkStorage;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final mUserStorage:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    .line 23
    return-void
.end method


# virtual methods
.method public clearUserData()V
    .locals 4

    .prologue
    .line 76
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 77
    sget-object v1, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Additional user storage is null, returning..."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    monitor-enter v2

    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;

    .line 83
    .local v0, "userStorage":Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;
    if-eqz v0, :cond_1

    .line 84
    invoke-interface {v0}, Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;->clearUserData()V

    goto :goto_1

    .line 87
    .end local v0    # "userStorage":Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method public getUserStorage()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public registerUserStorage(Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;)V
    .locals 13
    .param p1, "userStorage"    # Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 27
    iget-object v7, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    if-nez v7, :cond_0

    .line 28
    sget-object v5, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Additional storage set is null, returning..."

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v7, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    :goto_0
    return-void

    .line 32
    :cond_0
    if-nez p1, :cond_1

    .line 33
    sget-object v5, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Supplied UserStorage is null, returning..."

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v7, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37
    :cond_1
    iget-object v7, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    monitor-enter v7

    .line 39
    const/4 v2, 0x0

    .line 40
    .local v2, "instanceAlreadyRegistered":Z
    :try_start_0
    iget-object v8, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 42
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;>;"
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    if-nez v2, :cond_5

    .line 43
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;

    .line 45
    .local v0, "currentStorage":Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 46
    sget-object v8, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->LOG_TAG:Ljava/lang/String;

    const-string v9, "There is already an instance of %s registered, will not add a duplicate"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 49
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    .line 46
    invoke-static {v8, v9, v10}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    const/4 v2, 0x1

    .line 54
    invoke-interface {v0}, Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;->getCacheKey()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1}, Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;->getCacheKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    move v1, v5

    .line 56
    .local v1, "currentStorageShouldBeCleared":Z
    :goto_2
    if-eqz v1, :cond_3

    .line 57
    invoke-interface {v0}, Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;->clearUserData()V

    .line 60
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 63
    .end local v0    # "currentStorage":Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;
    .end local v1    # "currentStorageShouldBeCleared":Z
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;>;"
    :catchall_0
    move-exception v5

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v0    # "currentStorage":Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;
    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;>;"
    :cond_4
    move v1, v6

    .line 54
    goto :goto_2

    .line 63
    .end local v0    # "currentStorage":Lcom/zendesk/sdk/storage/SdkStorage$UserStorage;
    :cond_5
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    iget-object v7, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    invoke-interface {v7, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    .line 67
    .local v4, "modified":Z
    sget-object v7, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Additional user storage size %s, modified %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/zendesk/sdk/storage/ZendeskSdkStorage;->mUserStorage:Ljava/util/Set;

    .line 70
    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v9, v5

    .line 67
    invoke-static {v7, v8, v9}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
