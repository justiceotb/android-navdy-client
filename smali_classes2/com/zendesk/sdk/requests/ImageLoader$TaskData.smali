.class Lcom/zendesk/sdk/requests/ImageLoader$TaskData;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TaskData"
.end annotation


# instance fields
.field private attachment:Lcom/zendesk/sdk/model/request/Attachment;

.field private bitmap:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/zendesk/sdk/requests/ImageLoader;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/sdk/model/request/Attachment;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ImageLoader;
    .param p2, "attachment"    # Lcom/zendesk/sdk/model/request/Attachment;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p2, p0, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;->attachment:Lcom/zendesk/sdk/model/request/Attachment;

    .line 191
    iput-object p3, p0, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;->bitmap:Landroid/graphics/Bitmap;

    .line 192
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/requests/ImageLoader$TaskData;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ImageLoader$TaskData;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/requests/ImageLoader$TaskData;)Lcom/zendesk/sdk/model/request/Attachment;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/ImageLoader$TaskData;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;->attachment:Lcom/zendesk/sdk/model/request/Attachment;

    return-object v0
.end method
