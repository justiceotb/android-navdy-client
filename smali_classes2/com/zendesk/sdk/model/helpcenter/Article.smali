.class public Lcom/zendesk/sdk/model/helpcenter/Article;
.super Ljava/lang/Object;
.source "Article.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "Article"

.field public static final UNKNOWN_VOTE_COUNT:I = -0x1


# instance fields
.field private author:Lcom/zendesk/sdk/model/helpcenter/User;

.field private authorId:Ljava/lang/Long;

.field private body:Ljava/lang/String;

.field private commentsDisabled:Z

.field private createdAt:Ljava/util/Date;

.field private draft:Z

.field private htmlUrl:Ljava/lang/String;

.field private id:Ljava/lang/Long;

.field private labelNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private locale:Ljava/lang/String;

.field private outdated:Z

.field private sectionId:Ljava/lang/Long;

.field private sourceLocale:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private updatedAt:Ljava/util/Date;

.field private url:Ljava/lang/String;

.field private voteCount:Ljava/lang/Integer;

.field private voteSum:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthor()Lcom/zendesk/sdk/model/helpcenter/User;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->author:Lcom/zendesk/sdk/model/helpcenter/User;

    return-object v0
.end method

.method public getAuthorId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->authorId:Ljava/lang/Long;

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->body:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->createdAt:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 176
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->createdAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public getDownvoteCount()I
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteSum:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteCount:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 311
    :cond_0
    const-string v0, "Article"

    const-string v1, "Cannot determine vote count because vote_sum and / or vote_count are null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    const/4 v0, -0x1

    .line 318
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteSum:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method public getHtmlUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->htmlUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getLabelNames()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->labelNames:Ljava/util/List;

    invoke-static {v0}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->locale:Ljava/lang/String;

    return-object v0
.end method

.method public getSectionId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->sectionId:Ljava/lang/Long;

    return-object v0
.end method

.method public getSourceLocale()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->sourceLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdatedAt()Ljava/util/Date;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->updatedAt:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 189
    const/4 v0, 0x0

    .line 192
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->updatedAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public getUpvoteCount()I
    .locals 3

    .prologue
    .line 286
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteSum:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteCount:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 287
    :cond_0
    const-string v0, "Article"

    const-string v1, "Cannot determine vote count because vote_sum and / or vote_count are null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    const/4 v0, -0x1

    .line 294
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteSum:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteCount:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->url:Ljava/lang/String;

    return-object v0
.end method

.method public getVoteCount()I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteCount:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getVoteSum()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteSum:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->voteSum:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public isCommentsDisabled()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->commentsDisabled:Z

    return v0
.end method

.method public isDraft()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->draft:Z

    return v0
.end method

.method public isOutdated()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->outdated:Z

    return v0
.end method

.method public setAuthor(Lcom/zendesk/sdk/model/helpcenter/User;)V
    .locals 0
    .param p1, "author"    # Lcom/zendesk/sdk/model/helpcenter/User;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/Article;->author:Lcom/zendesk/sdk/model/helpcenter/User;

    .line 271
    return-void
.end method
