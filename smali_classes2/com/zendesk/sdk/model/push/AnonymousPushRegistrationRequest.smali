.class public Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;
.super Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
.source "AnonymousPushRegistrationRequest.java"


# instance fields
.field private sdkGuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;-><init>()V

    return-void
.end method


# virtual methods
.method public getSdkGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;->sdkGuid:Ljava/lang/String;

    return-object v0
.end method

.method public setSdkGuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "sdkGuid"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;->sdkGuid:Ljava/lang/String;

    .line 21
    return-void
.end method
