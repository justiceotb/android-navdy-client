.class Lcom/navdy/proxy/ProxyOutputThread$1;
.super Landroid/os/Handler;
.source "ProxyOutputThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/proxy/ProxyOutputThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/proxy/ProxyOutputThread;


# direct methods
.method constructor <init>(Lcom/navdy/proxy/ProxyOutputThread;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/proxy/ProxyOutputThread;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/navdy/proxy/ProxyOutputThread$1;->this$0:Lcom/navdy/proxy/ProxyOutputThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 49
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    move-object v1, v2

    check-cast v1, [B

    .line 53
    .local v1, "outputBuffer":[B
    :try_start_0
    iget-object v2, p0, Lcom/navdy/proxy/ProxyOutputThread$1;->this$0:Lcom/navdy/proxy/ProxyOutputThread;

    iget-object v2, v2, Lcom/navdy/proxy/ProxyOutputThread;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v2, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/io/IOException;
    invoke-static {}, Lcom/navdy/proxy/ProxyOutputThread;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "error writing to output"

    invoke-virtual {v2, v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 58
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->quit()V

    goto :goto_0
.end method
