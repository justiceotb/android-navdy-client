.class public Lcom/navdy/client/app/ui/customviews/TripCardView;
.super Landroid/widget/RelativeLayout;
.source "TripCardView.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;


# static fields
.field private static final COLOR_RED:I

.field private static final VERBOSE:Z

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bottomSheet:Landroid/view/View;

.field private bottomSheetCalculating:Landroid/view/View;

.field private bottomSheetCalculatingText:Landroid/widget/TextView;

.field protected icon:Landroid/widget/ImageView;

.field protected final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field protected tripAddressFirstLine:Landroid/widget/TextView;

.field protected tripAddressSecondLine:Landroid/widget/TextView;

.field protected tripName:Landroid/widget/TextView;

.field private tripOverviewArrivalTime:Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;

.field private tripOverviewDistance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

.field private tripOverviewETA:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/customviews/TripCardView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/TripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    .line 33
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f00af

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    sput v0, Lcom/navdy/client/app/ui/customviews/TripCardView;->COLOR_RED:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/TripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 63
    return-void
.end method

.method private hideMessages()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheetCalculating:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheet:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 225
    return-void
.end method

.method private showCalculating()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheetCalculatingText:Landroid/widget/TextView;

    const v1, 0x7f0800e1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 197
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheet:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheetCalculating:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 199
    return-void
.end method

.method private showError()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheetCalculatingText:Landroid/widget/TextView;

    const v1, 0x7f080163

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 204
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheet:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheetCalculating:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    return-void
.end method

.method private updateFields(Lcom/navdy/client/app/framework/models/Destination;IIZ)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "distance"    # I
    .param p3, "timeToDestination"    # I
    .param p4, "isTrafficHeavy"    # Z

    .prologue
    .line 209
    if-ltz p2, :cond_0

    if-gez p3, :cond_1

    .line 210
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->showCalculating()V

    .line 219
    :goto_0
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/customviews/TripCardView;->setNameAddressAndIcon(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 220
    return-void

    .line 212
    :cond_1
    if-eqz p4, :cond_2

    .line 213
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripOverviewArrivalTime:Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;

    sget v1, Lcom/navdy/client/app/ui/customviews/TripCardView;->COLOR_RED:I

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->setTextColor(I)V

    .line 216
    :cond_2
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->hideMessages()V

    .line 217
    invoke-direct {p0, p2, p3}, Lcom/navdy/client/app/ui/customviews/TripCardView;->updateTextFields(II)V

    goto :goto_0
.end method

.method private updateTextFields(II)V
    .locals 4
    .param p1, "distance"    # I
    .param p2, "durationWithTraffic"    # I

    .prologue
    .line 228
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripOverviewETA:Landroid/widget/TextView;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/TimeStampUtils;->getDurationStringInHoursAndMinutes(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripOverviewDistance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    int-to-double v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setDistance(D)V

    .line 230
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripOverviewArrivalTime:Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;

    invoke-virtual {v0, p2}, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;->setTimeToArrival(I)V

    .line 231
    return-void
.end method


# virtual methods
.method protected appendPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 241
    return-object p1
.end method

.method public isInEditMode()Z
    .locals 1

    .prologue
    .line 282
    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 96
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 97
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 106
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 107
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 73
    const v0, 0x7f1000b1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->icon:Landroid/widget/ImageView;

    .line 75
    const v0, 0x7f1000b3

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripName:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f1000b4

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripAddressFirstLine:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f10033b

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripAddressSecondLine:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f1000bf

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripOverviewDistance:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    .line 81
    const v0, 0x7f1000bc

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripOverviewETA:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f1000c2

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripOverviewArrivalTime:Lcom/navdy/client/app/ui/customviews/ArrivalTimeTextView;

    .line 84
    const v0, 0x7f1000b5

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheet:Landroid/view/View;

    .line 85
    const v0, 0x7f1000b6

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheetCalculating:Landroid/view/View;

    .line 86
    const v0, 0x7f1000b7

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->bottomSheetCalculatingText:Landroid/widget/TextView;

    .line 87
    return-void
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 4
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 117
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    if-eq p1, v0, :cond_0

    .line 118
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->setNameAddressAndIcon(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 119
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->showError()V

    .line 129
    :goto_0
    return-void

    .line 124
    :cond_0
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 125
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDistanceToDestination()I

    move-result v1

    .line 126
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getTimeToDestination()I

    move-result v2

    iget-boolean v3, p2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    .line 123
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/customviews/TripCardView;->updateFields(Lcom/navdy/client/app/framework/models/Destination;IIZ)V

    goto :goto_0
.end method

.method public onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/customviews/TripCardView;->setNameAddressAndIcon(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 112
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->showCalculating()V

    .line 113
    return-void
.end method

.method public onReroute()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->showCalculating()V

    .line 186
    return-void
.end method

.method public onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 189
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 4
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    .line 145
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    if-eq p1, v0, :cond_0

    .line 147
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    const/4 v1, 0x0

    .line 146
    invoke-direct {p0, v0, v2, v2, v1}, Lcom/navdy/client/app/ui/customviews/TripCardView;->updateFields(Lcom/navdy/client/app/framework/models/Destination;IIZ)V

    .line 152
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->showError()V

    .line 161
    :goto_0
    return-void

    .line 155
    :cond_0
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 156
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDistanceToDestination()I

    move-result v1

    .line 157
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getTimeToDestination()I

    move-result v2

    iget-boolean v3, p2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    .line 154
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/customviews/TripCardView;->updateFields(Lcom/navdy/client/app/framework/models/Destination;IIZ)V

    goto :goto_0
.end method

.method public onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->updateFields(Lcom/navdy/client/app/framework/models/Destination;IIZ)V

    .line 140
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/TripCardView;->showCalculating()V

    .line 141
    return-void
.end method

.method public onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 4
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 165
    .line 166
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 167
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDistanceToDestination()I

    move-result v1

    .line 168
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getTimeToDestination()I

    move-result v2

    iget-boolean v3, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    .line 165
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/customviews/TripCardView;->updateFields(Lcom/navdy/client/app/framework/models/Destination;IIZ)V

    .line 171
    return-void
.end method

.method public onStopRoute()V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 4
    .param p1, "progress"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 175
    .line 176
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 177
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getDistanceToDestination()I

    move-result v1

    .line 178
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getTimeToDestination()I

    move-result v2

    iget-boolean v3, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    .line 175
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/client/app/ui/customviews/TripCardView;->updateFields(Lcom/navdy/client/app/framework/models/Destination;IIZ)V

    .line 181
    return-void
.end method

.method public setNameAddressAndIcon(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 5
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 245
    if-nez p1, :cond_0

    .line 246
    sget-object v2, Lcom/navdy/client/app/ui/customviews/TripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Destination object is null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 278
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->icon:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 253
    instance-of v2, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    if-eqz v2, :cond_2

    .line 254
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForPendingTrip()I

    move-result v0

    .line 258
    .local v0, "asset":I
    :goto_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->icon:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 261
    .end local v0    # "asset":I
    :cond_1
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getTitleAndSubtitle()Landroid/util/Pair;

    move-result-object v1

    .line 262
    .local v1, "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 263
    sget-object v2, Lcom/navdy/client/app/ui/customviews/TripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "titleAndSubtitle is empty: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/util/Pair;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    .end local v1    # "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForActiveTrip()I

    move-result v0

    .restart local v0    # "asset":I
    goto :goto_1

    .line 267
    .end local v0    # "asset":I
    .restart local v1    # "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripName:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 268
    iget-object v3, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripName:Landroid/widget/TextView;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/customviews/TripCardView;->appendPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    :goto_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripAddressFirstLine:Landroid/widget/TextView;

    if-eqz v2, :cond_5

    .line 274
    iget-object v3, p0, Lcom/navdy/client/app/ui/customviews/TripCardView;->tripAddressFirstLine:Landroid/widget/TextView;

    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 270
    :cond_4
    sget-object v2, Lcom/navdy/client/app/ui/customviews/TripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Current route has no name!"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_2

    .line 276
    :cond_5
    sget-object v2, Lcom/navdy/client/app/ui/customviews/TripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Current route has no address!"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
