.class Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;
.super Ljava/lang/Object;
.source "SearchRecyclerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setCardData(Lcom/navdy/client/app/ui/search/SearchViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .prologue
    .line 440
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iput p2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 443
    iget v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;->val$position:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 444
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->access$000(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 445
    invoke-static {}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "click on item at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;->val$position:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 446
    return-void
.end method
