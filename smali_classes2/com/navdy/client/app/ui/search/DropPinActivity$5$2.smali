.class Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;
.super Ljava/lang/Object;
.source "DropPinActivity.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/DropPinActivity$5;->onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/DropPinActivity$5;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 6
    .param p1, "cameraPosition"    # Lcom/google/android/gms/maps/model/CameraPosition;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 306
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$500(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 307
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$600(Lcom/navdy/client/app/ui/search/DropPinActivity;)I

    move-result v2

    if-gez v2, :cond_0

    .line 309
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$608(Lcom/navdy/client/app/ui/search/DropPinActivity;)I

    .line 389
    :goto_0
    return-void

    .line 312
    :cond_0
    const/16 v1, 0x1f4

    .line 313
    .local v1, "delay":I
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$600(Lcom/navdy/client/app/ui/search/DropPinActivity;)I

    move-result v2

    if-nez v2, :cond_3

    .line 314
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$608(Lcom/navdy/client/app/ui/search/DropPinActivity;)I

    .line 315
    const/4 v1, 0x0

    .line 317
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$700(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 318
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v2, -0x3db80000    # -50.0f

    invoke-direct {v0, v4, v4, v2, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 320
    .local v0, "animation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 321
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 322
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$700(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 323
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$700(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 325
    .end local v0    # "animation":Landroid/view/animation/TranslateAnimation;
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$800(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 326
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$800(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;

    move-result-object v2

    const v3, 0x7f020133

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 328
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$900(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 329
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$900(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 333
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$1000(Lcom/navdy/client/app/ui/search/DropPinActivity;)V

    .line 335
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$1600(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;

    invoke-direct {v3, p0, p1}, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;-><init>(Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;Lcom/google/android/gms/maps/model/CameraPosition;)V

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
