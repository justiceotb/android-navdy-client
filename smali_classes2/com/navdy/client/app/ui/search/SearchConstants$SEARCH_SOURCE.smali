.class public final enum Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;
.super Ljava/lang/Enum;
.source "SearchConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SEARCH_SOURCE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

.field public static final enum SEARCH_AUTOCOMPLETE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

.field public static final enum SEARCH_CONTACT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

.field public static final enum SEARCH_MAP:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

.field public static final enum SEARCH_RESULT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    const-string v1, "SEARCH_AUTOCOMPLETE"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_AUTOCOMPLETE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    .line 58
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    const-string v1, "SEARCH_CONTACT"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_CONTACT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    .line 59
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    const-string v1, "SEARCH_RESULT"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_RESULT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    .line 60
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    const-string v1, "SEARCH_MAP"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_MAP:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    .line 56
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_AUTOCOMPLETE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_CONTACT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_RESULT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->SEARCH_MAP:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_SOURCE;->code:I

    return v0
.end method
