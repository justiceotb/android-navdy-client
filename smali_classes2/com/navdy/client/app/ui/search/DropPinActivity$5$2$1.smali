.class Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;
.super Ljava/lang/Object;
.source "DropPinActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;

.field final synthetic val$cameraPosition:Lcom/google/android/gms/maps/model/CameraPosition;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;->this$2:Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;->val$cameraPosition:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 338
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;->val$cameraPosition:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, v2, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    .line 340
    .local v0, "center":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v1, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1$1;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1$1;-><init>(Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;Lcom/google/android/gms/maps/model/LatLng;)V

    .line 381
    .local v1, "reverseGeoCode":Ljava/lang/Runnable;
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;->this$2:Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    new-instance v3, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v3}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$002(Lcom/navdy/client/app/ui/search/DropPinActivity;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;

    .line 383
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;->this$2:Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/framework/models/Destination;->setCoordsToSame(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 384
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2$1;->this$2:Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5$2;->this$1:Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    iget-object v2, v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;->this$0:Lcom/navdy/client/app/ui/search/DropPinActivity;

    .line 385
    invoke-static {v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 384
    invoke-static {v0, v2, v1}, Lcom/navdy/client/app/framework/map/MapUtils;->doReverseGeocodingFor(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V

    .line 387
    return-void
.end method
