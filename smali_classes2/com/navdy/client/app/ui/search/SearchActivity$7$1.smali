.class Lcom/navdy/client/app/ui/search/SearchActivity$7$1;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity$7;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

.field final synthetic val$contactList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity$7;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/search/SearchActivity$7;

    .prologue
    .line 552
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->val$contactList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 4
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 559
    .local p1, "autocompleteResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v0, v0, Lcom/navdy/client/app/ui/search/SearchActivity$7;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1200(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "runGoogleQueryAutoComplete"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 561
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;-><init>(Lcom/navdy/client/app/ui/search/SearchActivity$7$1;Ljava/util/List;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$7$1;->this$1:Lcom/navdy/client/app/ui/search/SearchActivity$7;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity$7;->val$constraint:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 601
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchActivity$7$1$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 602
    return-void
.end method
