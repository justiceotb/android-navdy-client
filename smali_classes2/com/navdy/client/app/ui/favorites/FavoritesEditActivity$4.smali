.class Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$4;
.super Ljava/lang/Object;
.source "FavoritesEditActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->saveChanges()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$4;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$4;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$000(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iget v0, v0, Lcom/navdy/client/app/framework/models/Destination;->id:I

    if-gtz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$4;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->access$400(Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Missing favorite id. Currently unable to save."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 260
    const v0, 0x7f0804bc

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 265
    :goto_0
    return-void

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity$4;->this$0:Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->saveChanges()V

    goto :goto_0
.end method
