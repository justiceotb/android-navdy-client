.class Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;
.super Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;
.source "FavoritesFragmentRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ItemTouchHelperCallback"
.end annotation


# instance fields
.field private final mAdapter:Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;

    .prologue
    .line 221
    invoke-direct {p0}, Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;-><init>()V

    .line 222
    iput-object p1, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;->mAdapter:Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;

    .line 223
    return-void
.end method


# virtual methods
.method public getMovementFlags(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;)I
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 237
    const/4 v0, 0x3

    .line 238
    .local v0, "dragFlags":I
    const/16 v1, 0x30

    .line 239
    .local v1, "swipeFlags":I
    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;->makeMovementFlags(II)I

    move-result v2

    return v2
.end method

.method public isItemViewSwipeEnabled()Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x0

    return v0
.end method

.method public isLongPressDragEnabled()Z
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public onMove(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p3, "target"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;->mAdapter:Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;->onItemMove(II)V

    .line 255
    const/4 v0, 0x1

    return v0
.end method

.method public onMoved(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;ILandroid/support/v7/widget/RecyclerView$ViewHolder;III)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p3, "fromPos"    # I
    .param p4, "target"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p5, "toPos"    # I
    .param p6, "x"    # I
    .param p7, "y"    # I

    .prologue
    .line 266
    invoke-super/range {p0 .. p7}, Landroid/support/v7/widget/helper/ItemTouchHelper$Callback;->onMoved(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$ViewHolder;ILandroid/support/v7/widget/RecyclerView$ViewHolder;III)V

    .line 267
    iget-object v0, p0, Lcom/navdy/client/app/ui/favorites/FavoritesFragmentRecyclerAdapter$ItemTouchHelperCallback;->mAdapter:Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/favorites/ItemTouchHelperAdapter;->setupDynamicShortcuts()V

    .line 268
    return-void
.end method

.method public onSwiped(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "direction"    # I

    .prologue
    .line 274
    return-void
.end method
