.class Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isEnglish(Ljava/util/Locale;)Z
    .locals 2
    .param p1, "currentLocale"    # Ljava/util/Locale;

    .prologue
    .line 146
    if-eqz p1, :cond_0

    .line 148
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en"

    .line 147
    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 7
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 80
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->hideProgressDialog()V

    .line 83
    new-instance v3, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;)V

    invoke-virtual {p1, v3}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 96
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getInstance()Lcom/navdy/client/app/framework/i18n/I18nManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    .line 97
    .local v0, "currentLocale":Ljava/util/Locale;
    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->isEnglish(Ljava/util/Locale;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 98
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    .line 99
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v4

    const/4 v5, 0x3

    .line 101
    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v6

    .line 98
    invoke-static {v3, v4, v5, v6}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->access$000(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;[Landroid/media/MediaPlayer$TrackInfo;ILjava/lang/String;)I

    move-result v2

    .line 102
    .local v2, "textTrackIndex":I
    if-ltz v2, :cond_1

    .line 103
    invoke-virtual {p1, v2}, Landroid/media/MediaPlayer;->selectTrack(I)V

    .line 114
    .end local v2    # "textTrackIndex":I
    :cond_0
    :goto_0
    new-instance v3, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;)V

    invoke-virtual {p1, v3}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 143
    .end local v0    # "currentLocale":Ljava/util/Locale;
    :goto_1
    return-void

    .line 105
    .restart local v0    # "currentLocale":Ljava/util/Locale;
    .restart local v2    # "textTrackIndex":I
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;->this$0:Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->access$100(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot find text track for language: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 106
    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 105
    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 140
    .end local v0    # "currentLocale":Ljava/util/Locale;
    .end local v2    # "textTrackIndex":I
    :catch_0
    move-exception v1

    .line 141
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
