.class public Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "InstallIntroActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v3, 0x7f03006f

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->setContentView(I)V

    .line 27
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 28
    .local v1, "callingIntent":Landroid/content/Intent;
    const-string v3, "extra_was_skipped"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 30
    .local v2, "previousScreenWasSkipped":Z
    if-eqz v2, :cond_0

    .line 31
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->userHasFinishedInstall()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->userHasFinishedAppSetup()Z

    move-result v3

    if-nez v3, :cond_0

    .line 33
    invoke-static {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->goToAppSetup(Landroid/app/Activity;)V

    .line 36
    :cond_0
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v4, 0x7f080269

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 39
    const v3, 0x7f1001c6

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 40
    .local v0, "alreadyInstalled":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 41
    const v3, 0x7f0800ba

    .line 42
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v3

    .line 41
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    new-instance v3, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    :cond_1
    return-void
.end method

.method public onNextClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 59
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 60
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 61
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "has_skipped_install"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 63
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->startActivity(Landroid/content/Intent;)V

    .line 64
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 69
    const-string v0, "Installation_Ready_Check"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 70
    return-void
.end method
