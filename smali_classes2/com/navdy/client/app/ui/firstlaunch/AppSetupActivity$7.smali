.class Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$7;
.super Ljava/lang/Object;
.source "AppSetupActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->onButtonClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 432
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 435
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$7;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$1000(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V

    .line 436
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 437
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_0

    .line 438
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getMusicCapabilities()Lcom/navdy/service/library/events/audio/MusicCapabilitiesResponse;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 440
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    .line 441
    .local v0, "musicServiceHandler":Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->shouldPerformFullPlaylistIndex()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 442
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->indexPlaylists()V

    .line 444
    :cond_1
    return-void
.end method
