.class Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;
.super Landroid/os/AsyncTask;
.source "MountPickerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

.field final synthetic val$desc:Landroid/widget/TextView;

.field final synthetic val$mediumMountPill:Landroid/widget/TextView;

.field final synthetic val$modelString:Ljava/lang/String;

.field final synthetic val$shortMountPill:Landroid/widget/TextView;

.field final synthetic val$title:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$modelString:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$shortMountPill:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$mediumMountPill:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$title:Landroid/widget/TextView;

    iput-object p6, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$desc:Landroid/widget/TextView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getMountInfo()Lcom/navdy/client/app/framework/models/MountInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 72
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 7
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->box:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$modelString:Ljava/lang/String;

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$shortMountPill:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$mediumMountPill:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$title:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->val$desc:Landroid/widget/TextView;

    invoke-static/range {v0 .. v6}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->access$000(Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 87
    return-void
.end method
