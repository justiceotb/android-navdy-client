.class public Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "BoxViewerActivity.java"


# static fields
.field public static final EXTRA_BOX:Ljava/lang/String; = "extra_box"


# instance fields
.field private box:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method

.method private getCurrentConfig()Ljava/lang/String;
    .locals 9

    .prologue
    .line 84
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 85
    .local v4, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v7, "vehicle-year"

    const-string v8, ""

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "carYear":Ljava/lang/String;
    const-string v7, "vehicle-make"

    const-string v8, ""

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "carMake":Ljava/lang/String;
    const-string v7, "vehicle-model"

    const-string v8, ""

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "carModel":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    .line 91
    .local v6, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-static {v6}, Lcom/navdy/client/app/ui/firstlaunch/InstallPagerAdapter;->getCurrentMountType(Landroid/content/SharedPreferences;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v3

    .line 92
    .local v3, "currentMountType":Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 94
    .local v5, "mount":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->box:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->finish()V

    .line 145
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v1, 0x7f030069

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->setContentView(I)V

    .line 38
    const v1, 0x7f100183

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 40
    .local v0, "next":Landroid/view/View;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_box"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->box:Ljava/lang/String;

    .line 42
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->box:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 45
    :cond_0
    return-void
.end method

.method public onNextClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 48
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 49
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    if-eqz v4, :cond_1

    .line 52
    const-string v5, "last_config"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "lastConfig":Ljava/lang/String;
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->getCurrentConfig()Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "currentConfig":Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 60
    const-string v5, "nb_config"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    add-int/lit8 v3, v5, 0x1

    .line 65
    .local v3, "nbConfig":I
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "nb_config"

    .line 66
    invoke-interface {v5, v6, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "last_config"

    .line 67
    invoke-interface {v5, v6, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 68
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 73
    .end local v3    # "nbConfig":I
    :cond_0
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "has_skipped_install"

    .line 74
    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 75
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 77
    .end local v0    # "currentConfig":Ljava/lang/String;
    .end local v2    # "lastConfig":Ljava/lang/String;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "extra_next_step"

    const-string v6, "installation_flow"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->startActivity(Landroid/content/Intent;)V

    .line 80
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->finish()V

    .line 81
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const v5, 0x7f100182

    .line 99
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->hideSystemUI()V

    .line 101
    const v2, 0x7f10007f

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 102
    .local v1, "title":Landroid/widget/TextView;
    const v2, 0x7f100181

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    .local v0, "subtitle":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->box:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 135
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid box type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->box:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->finish()V

    .line 140
    :goto_1
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 141
    return-void

    .line 103
    :sswitch_0
    const-string v4, "New_Box"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :sswitch_1
    const-string v4, "New_Box_Plus_Mounts"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :sswitch_2
    const-string v4, "Old_Box"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x2

    goto :goto_0

    .line 105
    :pswitch_0
    const v2, 0x7f0201f9

    invoke-virtual {p0, v5, v2}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->loadImage(II)V

    .line 106
    if-eqz v1, :cond_1

    .line 107
    const v2, 0x7f0802f6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 109
    :cond_1
    if-eqz v0, :cond_2

    .line 110
    const v2, 0x7f0802f2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 112
    :cond_2
    const-string v2, "Installation_Box_Viewer_New_Box"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_1

    .line 115
    :pswitch_1
    const v2, 0x7f020207

    invoke-virtual {p0, v5, v2}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->loadImage(II)V

    .line 116
    if-eqz v1, :cond_3

    .line 117
    const v2, 0x7f0802f5

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 119
    :cond_3
    if-eqz v0, :cond_4

    .line 120
    const v2, 0x7f0802f4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 122
    :cond_4
    const-string v2, "Installation_Box_Viewer_New_Box_Plus_Mounts"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_1

    .line 125
    :pswitch_2
    const v2, 0x7f020202

    invoke-virtual {p0, v5, v2}, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;->loadImage(II)V

    .line 126
    if-eqz v1, :cond_5

    .line 127
    const v2, 0x7f080323

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 129
    :cond_5
    if-eqz v0, :cond_6

    .line 130
    const v2, 0x7f080322

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 132
    :cond_6
    const-string v2, "Installation_Box_Viewer_Old_Box"

    invoke-static {v2}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    goto :goto_1

    .line 103
    :sswitch_data_0
    .sparse-switch
        -0x2ec75994 -> :sswitch_0
        0x11050f73 -> :sswitch_2
        0x5cac6c6c -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
