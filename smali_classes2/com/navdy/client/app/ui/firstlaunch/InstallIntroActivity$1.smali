.class Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity$1;
.super Ljava/lang/Object;
.source "InstallIntroActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 46
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 47
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 48
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "has_skipped_install"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 50
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/InstallCompleteActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallIntroActivity;->startActivity(Landroid/content/Intent;)V

    .line 52
    return-void
.end method
