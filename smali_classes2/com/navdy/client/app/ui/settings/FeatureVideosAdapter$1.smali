.class Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;
.super Ljava/lang/Object;
.source "FeatureVideosAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

.field final synthetic val$featureVideo:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;->val$featureVideo:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 113
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;->val$featureVideo:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    iget-object v2, v2, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->sharedPrefKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$000(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;Ljava/lang/String;)V

    .line 115
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$100(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "video_url"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;->val$featureVideo:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    iget-object v2, v2, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->link:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;->this$0:Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->access$100(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 118
    return-void
.end method
