.class Lcom/navdy/client/app/ui/details/DetailsActivity$1;
.super Ljava/lang/Object;
.source "DetailsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/details/DetailsActivity;->initFavoriteButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/details/DetailsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$002(Lcom/navdy/client/app/ui/details/DetailsActivity;Z)Z

    .line 132
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$100(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$100(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/favorites/FavoritesEditActivity;->startFavoriteEditActivity(Landroid/app/Activity;Landroid/os/Parcelable;)V

    .line 157
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->showProgressDialog()V

    .line 137
    iget-object v0, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$1;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/details/DetailsActivity;->access$100(Lcom/navdy/client/app/ui/details/DetailsActivity;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    const/4 v1, -0x1

    new-instance v2, Lcom/navdy/client/app/ui/details/DetailsActivity$1$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/details/DetailsActivity$1$1;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Destination;->saveDestinationAsFavoritesAsync(ILcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V

    goto :goto_0
.end method
