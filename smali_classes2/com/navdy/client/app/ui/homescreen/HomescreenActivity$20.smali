.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->routeToAddressOrCoords(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 1511
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 1520
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$2100(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "routeToAddressOrCoords, couldn\'t get coords either from here or google, trying with just address"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 1521
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideProgressDialog()V

    .line 1522
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1800(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 1523
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideProgressDialog()V

    .line 1515
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$20;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1800(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 1516
    return-void
.end method
