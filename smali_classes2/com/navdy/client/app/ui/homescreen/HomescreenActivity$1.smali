.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 192
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    const v3, 0x7f1000ea

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 193
    .local v1, "successBanner":Landroid/widget/RelativeLayout;
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    const v3, 0x7f1000f0

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 194
    .local v0, "failureBanner":Landroid/widget/RelativeLayout;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 195
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$000(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 196
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 197
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v2, v4}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideConnectionBanner(Landroid/view/View;)V

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$100(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    goto :goto_0

    .line 202
    :cond_2
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 203
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v2, v4}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->hideConnectionBanner(Landroid/view/View;)V

    goto :goto_0

    .line 205
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$1;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$100(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    goto :goto_0
.end method
