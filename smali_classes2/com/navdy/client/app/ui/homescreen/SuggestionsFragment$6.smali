.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$6;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

.field final synthetic val$progress:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 592
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$6;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$6;->val$progress:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 600
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 2
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 595
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$6;->val$progress:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getProgress()Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v0

    .line 596
    .local v0, "progressPolyline":Lcom/here/android/mpa/common/GeoPolyline;
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$6;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v1, p1, v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$300(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/here/android/mpa/common/GeoPolyline;)V

    .line 597
    return-void
.end method
