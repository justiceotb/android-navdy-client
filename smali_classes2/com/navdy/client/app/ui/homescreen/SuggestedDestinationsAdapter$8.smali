.class Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;
.super Ljava/lang/Object;
.source "SuggestedDestinationsAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindSuggestionClickListeners(Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

.field final synthetic val$background:Landroid/view/View;

.field final synthetic val$shouldSendToHud:Z

.field final synthetic val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

.field final synthetic val$viewHolder:Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;ZLandroid/view/View;Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    .prologue
    .line 767
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iput-boolean p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$shouldSendToHud:Z

    iput-object p3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$background:Landroid/view/View;

    iput-object p4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$viewHolder:Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    iput-object p5, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 770
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iget-boolean v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$shouldSendToHud:Z

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$background:Landroid/view/View;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$viewHolder:Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->getAdapterPosition()I

    move-result v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$8;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->access$200(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;ZLandroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z

    move-result v0

    return v0
.end method
