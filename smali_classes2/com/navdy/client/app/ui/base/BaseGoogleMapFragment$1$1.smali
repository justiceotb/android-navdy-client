.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1$1;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;->onPhoneLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 3
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 114
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$000(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;

    iget-object v1, v1, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$1;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerOnLastKnownLocation(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
