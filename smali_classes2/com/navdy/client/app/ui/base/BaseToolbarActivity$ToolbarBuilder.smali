.class public Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;
.super Ljava/lang/Object;
.source "BaseToolbarActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ToolbarBuilder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseToolbarActivity;

.field title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V
    .locals 1
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseToolbarActivity;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->this$0:Lcom/navdy/client/app/ui/base/BaseToolbarActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public build()Landroid/support/v7/widget/Toolbar;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->this$0:Lcom/navdy/client/app/ui/base/BaseToolbarActivity;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->this$0:Lcom/navdy/client/app/ui/base/BaseToolbarActivity;

    const v1, 0x7f1000a9

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->access$000(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;I)Landroid/support/v7/widget/Toolbar;

    move-result-object v0

    return-object v0
.end method

.method public title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;
    .locals 2
    .param p1, "title"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 33
    const-string v0, ""

    .line 35
    .local v0, "titleString":Ljava/lang/String;
    if-lez p1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->this$0:Lcom/navdy/client/app/ui/base/BaseToolbarActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 38
    :cond_0
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(Ljava/lang/String;)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v1

    return-object v1
.end method

.method public title(Ljava/lang/String;)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title:Ljava/lang/String;

    .line 28
    :cond_0
    return-object p0
.end method
