.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->whenAttached(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field final synthetic val$attachListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 535
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;->val$attachListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 538
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;->val$attachListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;->onAttached()V

    .line 546
    :goto_0
    return-void

    .line 540
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "whenAttached, isRemoving, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 543
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "queueing up a whenAttachListener"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 544
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$2000(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Ljava/util/Queue;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$15;->val$attachListener:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
