.class Lcom/navdy/client/app/ui/base/BaseActivity$6;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseActivity;->showObdDialogIfCarHasBeenAddedToBlacklist()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseActivity;

    .prologue
    .line 372
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseActivity$6;->this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 375
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 377
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->obdScanIsOn()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 378
    const-string v2, "user_overrided_blacklist"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 379
    .local v0, "overrodeBlacklist":Z
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->carIsBlacklistedForObdData()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 380
    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity$6;->this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

    new-instance v3, Lcom/navdy/client/app/ui/base/BaseActivity$6$1;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/base/BaseActivity$6$1;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity$6;)V

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/base/BaseActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 388
    .end local v0    # "overrodeBlacklist":Z
    :cond_0
    return-void
.end method
