.class public Lcom/navdy/client/app/framework/map/HereMapsConfigurator;
.super Ljava/lang/Object;
.source "HereMapsConfigurator.java"


# static fields
.field private static final sInstance:Lcom/navdy/client/app/framework/map/HereMapsConfigurator;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private volatile isAlreadyConfigured:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 28
    new-instance v0, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sInstance:Lcom/navdy/client/app/framework/map/HereMapsConfigurator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/map/HereMapsConfigurator;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sInstance:Lcom/navdy/client/app/framework/map/HereMapsConfigurator;

    return-object v0
.end method

.method private removeOldHereMapsFolders(Ljava/util/List;Ljava/lang/String;)V
    .locals 4
    .param p2, "latestHereMapConfigFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "hereMapsConfigFolders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 74
    .local v0, "hereMapsConfigFolder":Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 75
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    goto :goto_0

    .line 78
    .end local v0    # "hereMapsConfigFolder":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized updateMapsConfig()V
    .locals 14
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 38
    monitor-enter p0

    :try_start_0
    iget-boolean v9, p0, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->isAlreadyConfigured:Z

    if-eqz v9, :cond_0

    .line 39
    sget-object v9, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "MWConfig is already configured"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :goto_0
    monitor-exit p0

    return-void

    .line 43
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/client/app/framework/PathManager;->hasDiskCacheDir()Z

    move-result v9

    if-nez v9, :cond_1

    .line 44
    sget-object v9, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "diskcache does not exist"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 38
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 48
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 50
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/client/app/framework/PathManager;->getHereMapsConfigDirs()Ljava/util/List;

    move-result-object v1

    .line 53
    .local v1, "hereMapsConfigFolders":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/navdy/client/app/framework/PathManager;->getLatestHereMapsConfigPath()Ljava/lang/String;

    move-result-object v6

    .line 54
    .local v6, "latestHereMapConfigFolder":Ljava/lang/String;
    new-instance v7, Lcom/navdy/client/app/framework/util/PackagedResource;

    const-string v9, "mwconfig_client"

    invoke-direct {v7, v9, v6}, Lcom/navdy/client/app/framework/util/PackagedResource;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 57
    .local v7, "mwConfigLatest":Lcom/navdy/client/app/framework/util/PackagedResource;
    :try_start_3
    sget-object v9, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "MWConfig: starting"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 58
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 60
    .local v2, "l1":J
    const v9, 0x7f070009

    const v10, 0x7f07000a

    invoke-virtual {v7, v0, v9, v10}, Lcom/navdy/client/app/framework/util/PackagedResource;->updateFromResources(Landroid/content/Context;II)V

    .line 61
    invoke-direct {p0, v1, v6}, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->removeOldHereMapsFolders(Ljava/util/List;Ljava/lang/String;)V

    .line 63
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 64
    .local v4, "l2":J
    sget-object v9, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "MWConfig: time took "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v12, v4, v2

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 66
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->isAlreadyConfigured:Z
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 67
    .end local v2    # "l1":J
    .end local v4    # "l2":J
    :catch_0
    move-exception v8

    .line 68
    .local v8, "throwable":Ljava/lang/Throwable;
    :try_start_4
    sget-object v9, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "MWConfig error"

    invoke-virtual {v9, v10, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method
