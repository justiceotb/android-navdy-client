.class public interface abstract Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;
.super Ljava/lang/Object;
.source "NavdyLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/location/NavdyLocationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnNavdyLocationChangedListener"
.end annotation


# virtual methods
.method public abstract onCarLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
    .param p1    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onPhoneLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
    .param p1    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
