.class public final enum Lcom/navdy/client/app/framework/models/MountInfo$MountType;
.super Ljava/lang/Enum;
.source "MountInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/MountInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MountType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/models/MountInfo$MountType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/models/MountInfo$MountType;

.field public static final enum MEDIUM:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

.field public static final enum SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

.field public static final enum TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;


# instance fields
.field type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    const-string v1, "SHORT"

    const-string v2, "Short_Mount"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 21
    new-instance v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    const-string v1, "MEDIUM"

    const-string v2, "Medium_Mount"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->MEDIUM:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 22
    new-instance v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    const-string v1, "TALL"

    const-string v2, "Tall_Mount"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    sget-object v1, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->MEDIUM:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->$VALUES:[Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput-object p3, p0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->type:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public static getMountTypeForName(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 50
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 54
    :goto_1
    return-object v0

    .line 47
    :sswitch_0
    const-string v1, "SMALL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const-string v1, "MEDIUM"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_2
    const-string v1, "TALL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 52
    :pswitch_0
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->MEDIUM:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    goto :goto_1

    .line 54
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    goto :goto_1

    .line 47
    :sswitch_data_0
    .sparse-switch
        -0x78ae7c8b -> :sswitch_1
        0x272cad -> :sswitch_2
        0x4b59ce7 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getMountTypeForValue(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 35
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 38
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 42
    :goto_1
    return-object v0

    .line 35
    :sswitch_0
    const-string v1, "Short_Mount"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const-string v1, "Medium_Mount"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_2
    const-string v1, "Tall_Mount"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 40
    :pswitch_0
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->MEDIUM:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    goto :goto_1

    .line 42
    :pswitch_1
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    goto :goto_1

    .line 35
    :sswitch_data_0
    .sparse-switch
        -0x513171aa -> :sswitch_0
        -0xe169931 -> :sswitch_1
        0x20b4ca07 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/models/MountInfo$MountType;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->$VALUES:[Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/models/MountInfo$MountType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    return-object v0
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->type:Ljava/lang/String;

    return-object v0
.end method
