.class public final enum Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
.super Ljava/lang/Enum;
.source "Suggestion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/Suggestion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SuggestionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum ADD_FAVORITE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum ADD_HOME:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum ADD_WORK:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum DEMO_VIDEO:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum ENABLE_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum ENABLE_GLANCES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum ENABLE_MICROPHONE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum GOOGLE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum GOOGLE_NOW:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum HERE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum HERE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum HUD_LOCAL_MUSIC_BROWSER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum LOADING:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum OTA:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum RECENT:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum SECTION_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum TRY_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

.field public static final enum VOICE_SEARCH:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 39
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->LOADING:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 41
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "HERE_HEADER"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 42
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "GOOGLE_HEADER"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 43
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->SECTION_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 45
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "ACTIVE_TRIP"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 46
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "PENDING_TRIP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 48
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "DEMO_VIDEO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->DEMO_VIDEO:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 49
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "ENABLE_GLANCES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GLANCES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 50
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "ENABLE_MICROPHONE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_MICROPHONE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 51
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "GOOGLE_NOW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_NOW:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 52
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "HUD_LOCAL_MUSIC_BROWSER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HUD_LOCAL_MUSIC_BROWSER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 53
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "VOICE_SEARCH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->VOICE_SEARCH:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 54
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "ENABLE_GESTURES"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 55
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "TRY_GESTURES"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->TRY_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 56
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "ADD_HOME"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_HOME:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 57
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "ADD_WORK"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_WORK:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 58
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "OTA"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->OTA:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 59
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "ADD_FAVORITE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_FAVORITE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 61
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "CALENDAR"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 62
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "RECOMMENDATION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 64
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "RECENT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECENT:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 65
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "GOOGLE_FOOTER"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 66
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    const-string v1, "HERE_FOOTER"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    .line 38
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->LOADING:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->SECTION_HEADER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ACTIVE_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->PENDING_TRIP:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->DEMO_VIDEO:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GLANCES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_MICROPHONE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_NOW:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HUD_LOCAL_MUSIC_BROWSER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->VOICE_SEARCH:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ENABLE_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->TRY_GESTURES:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_HOME:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_WORK:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->OTA:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->ADD_FAVORITE:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECOMMENDATION:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->RECENT:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->GOOGLE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->HERE_FOOTER:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->$VALUES:[Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    const-class v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->$VALUES:[Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    return-object v0
.end method
