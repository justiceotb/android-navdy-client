.class Lcom/navdy/client/app/framework/DeviceConnection$3;
.super Ljava/lang/Object;
.source "DeviceConnection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/DeviceConnection;->onDeviceConnectFailure(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/DeviceConnection;

.field final synthetic val$cause:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

.field final synthetic val$device:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/DeviceConnection;Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/DeviceConnection;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/navdy/client/app/framework/DeviceConnection$3;->this$0:Lcom/navdy/client/app/framework/DeviceConnection;

    iput-object p2, p0, Lcom/navdy/client/app/framework/DeviceConnection$3;->val$device:Lcom/navdy/service/library/device/RemoteDevice;

    iput-object p3, p0, Lcom/navdy/client/app/framework/DeviceConnection$3;->val$cause:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 205
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectionFailedEvent;

    iget-object v2, p0, Lcom/navdy/client/app/framework/DeviceConnection$3;->val$device:Lcom/navdy/service/library/device/RemoteDevice;

    iget-object v3, p0, Lcom/navdy/client/app/framework/DeviceConnection$3;->val$cause:Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    invoke-direct {v1, v2, v3}, Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectionFailedEvent;-><init>(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 206
    return-void
.end method
