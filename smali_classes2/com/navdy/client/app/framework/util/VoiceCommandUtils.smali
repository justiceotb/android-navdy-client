.class public Lcom/navdy/client/app/framework/util/VoiceCommandUtils;
.super Ljava/lang/Object;
.source "VoiceCommandUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addHomeAndWorkIfMatch(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "destinationsFromDatabase":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    const/4 v3, 0x0

    .line 107
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->isHome(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getHome()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 109
    .local v0, "homeDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 118
    .end local v0    # "homeDestination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->isWork(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getWork()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    .line 114
    .local v1, "workDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p0, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public static getMatchingVoiceCommand(ILjava/lang/String;)Lcom/navdy/client/app/framework/util/VoiceCommand;
    .locals 12
    .param p0, "rawRes"    # I
        .annotation build Landroid/support/annotation/RawRes;
        .end annotation
    .end param
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x1

    .line 31
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 67
    :goto_0
    return-object v8

    .line 35
    :cond_0
    const/4 v1, 0x0

    .line 36
    .local v1, "inputStream":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 38
    .local v6, "sc":Ljava/util/Scanner;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 39
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 42
    .local v3, "lowercaseQuery":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, p0}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 43
    new-instance v7, Ljava/util/Scanner;

    const-string v9, "UTF-8"

    invoke-direct {v7, v1, v9}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    .end local v6    # "sc":Ljava/util/Scanner;
    .local v7, "sc":Ljava/util/Scanner;
    :cond_1
    :try_start_1
    invoke-virtual {v7}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 46
    invoke-virtual {v7}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "line":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 51
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 52
    const-string v9, "\\{.*?\\}"

    const-string v10, "(.*?)"

    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 54
    .local v5, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v5, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 55
    .local v4, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v9

    if-ne v9, v11, :cond_1

    .line 56
    new-instance v8, Lcom/navdy/client/app/framework/util/VoiceCommand;

    invoke-direct {v8}, Lcom/navdy/client/app/framework/util/VoiceCommand;-><init>()V

    .line 57
    .local v8, "vc":Lcom/navdy/client/app/framework/util/VoiceCommand;
    iput-object v2, v8, Lcom/navdy/client/app/framework/util/VoiceCommand;->prefix:Ljava/lang/String;

    .line 58
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/navdy/client/app/framework/util/VoiceCommand;->query:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 63
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 64
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 63
    .end local v2    # "line":Ljava/lang/String;
    .end local v4    # "matcher":Ljava/util/regex/Matcher;
    .end local v5    # "pattern":Ljava/util/regex/Pattern;
    .end local v8    # "vc":Lcom/navdy/client/app/framework/util/VoiceCommand;
    :cond_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 64
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 63
    .end local v7    # "sc":Ljava/util/Scanner;
    .restart local v6    # "sc":Ljava/util/Scanner;
    :catchall_0
    move-exception v9

    :goto_1
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 64
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v9

    .line 63
    .end local v6    # "sc":Ljava/util/Scanner;
    .restart local v7    # "sc":Ljava/util/Scanner;
    :catchall_1
    move-exception v9

    move-object v6, v7

    .end local v7    # "sc":Ljava/util/Scanner;
    .restart local v6    # "sc":Ljava/util/Scanner;
    goto :goto_1
.end method

.method public static isHome(Ljava/lang/String;)Z
    .locals 1
    .param p0, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 121
    const v0, 0x7f070005

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->isOneOfTheseVoiceCommands(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isOneOfTheseVoiceCommands(ILjava/lang/String;)Z
    .locals 8
    .param p0, "CommandListRawRes"    # I
        .annotation build Landroid/support/annotation/RawRes;
        .end annotation
    .end param
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 72
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 102
    :goto_0
    return v6

    .line 76
    :cond_0
    const/4 v1, 0x0

    .line 77
    .local v1, "inputStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 79
    .local v4, "sc":Ljava/util/Scanner;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 80
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "lowercaseQuery":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 84
    new-instance v5, Ljava/util/Scanner;

    const-string v7, "UTF-8"

    invoke-direct {v5, v1, v7}, Ljava/util/Scanner;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    .end local v4    # "sc":Ljava/util/Scanner;
    .local v5, "sc":Ljava/util/Scanner;
    :cond_1
    :try_start_1
    invoke-virtual {v5}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 87
    invoke-virtual {v5}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "line":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 92
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v7

    if-eqz v7, :cond_1

    .line 94
    const/4 v6, 0x1

    .line 98
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 99
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 98
    .end local v2    # "line":Ljava/lang/String;
    :cond_2
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 99
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_0

    .line 98
    .end local v5    # "sc":Ljava/util/Scanner;
    .restart local v4    # "sc":Ljava/util/Scanner;
    :catchall_0
    move-exception v6

    :goto_1
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 99
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v6

    .line 98
    .end local v4    # "sc":Ljava/util/Scanner;
    .restart local v5    # "sc":Ljava/util/Scanner;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "sc":Ljava/util/Scanner;
    .restart local v4    # "sc":Ljava/util/Scanner;
    goto :goto_1
.end method

.method public static isWork(Ljava/lang/String;)Z
    .locals 1
    .param p0, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 125
    const v0, 0x7f070011

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->isOneOfTheseVoiceCommands(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method
