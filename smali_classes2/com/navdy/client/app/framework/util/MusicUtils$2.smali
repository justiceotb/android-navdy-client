.class final Lcom/navdy/client/app/framework/util/MusicUtils$2;
.super Ljava/util/HashMap;
.source "MusicUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/MusicUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicEvent$Action;",
        "Landroid/view/KeyEvent;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x5a

    const/16 v4, 0x59

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 96
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    new-instance v1, Landroid/view/KeyEvent;

    invoke-direct {v1, v2, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_START:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    new-instance v1, Landroid/view/KeyEvent;

    invoke-direct {v1, v2, v5}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_REWIND_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    new-instance v1, Landroid/view/KeyEvent;

    invoke-direct {v1, v3, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_FAST_FORWARD_STOP:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    new-instance v1, Landroid/view/KeyEvent;

    invoke-direct {v1, v3, v5}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/framework/util/MusicUtils$2;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void
.end method
