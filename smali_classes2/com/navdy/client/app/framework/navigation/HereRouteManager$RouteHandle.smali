.class public Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
.super Ljava/lang/Object;
.source "HereRouteManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/navigation/HereRouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RouteHandle"
.end annotation


# instance fields
.field private final router:Lcom/here/android/mpa/routing/CoreRouter;


# direct methods
.method private constructor <init>(Lcom/here/android/mpa/routing/CoreRouter;)V
    .locals 0
    .param p1, "router"    # Lcom/here/android/mpa/routing/CoreRouter;

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;->router:Lcom/here/android/mpa/routing/CoreRouter;

    .line 169
    return-void
.end method

.method synthetic constructor <init>(Lcom/here/android/mpa/routing/CoreRouter;Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/here/android/mpa/routing/CoreRouter;
    .param p2, "x1"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$1;

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;-><init>(Lcom/here/android/mpa/routing/CoreRouter;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;->router:Lcom/here/android/mpa/routing/CoreRouter;

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/CoreRouter;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;->router:Lcom/here/android/mpa/routing/CoreRouter;

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/CoreRouter;->cancel()V

    .line 175
    :cond_0
    return-void
.end method
