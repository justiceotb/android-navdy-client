.class public interface abstract Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;
.super Ljava/lang/Object;
.source "HereRouteManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/navigation/HereRouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onPreCalculation(Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)V
    .param p1    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V
    .param p1    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/here/android/mpa/routing/Route;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
.end method
