.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 364
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$800(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Landroid/location/Location;

    move-result-object v4

    if-nez v4, :cond_0

    .line 365
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    const v6, 0x7f080301

    .line 366
    invoke-virtual {v5, v6}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 365
    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 367
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 394
    :goto_0
    return-void

    .line 375
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$900(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->getItem(I)Lcom/navdy/client/debug/PlaceDebugAutocomplete;

    move-result-object v1

    .line 376
    .local v1, "item":Lcom/navdy/client/debug/PlaceDebugAutocomplete;
    iget-object v4, v1, Lcom/navdy/client/debug/PlaceDebugAutocomplete;->placeId:Ljava/lang/CharSequence;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 377
    .local v2, "placeId":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Autocomplete item selected: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/navdy/client/debug/PlaceDebugAutocomplete;->description:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 383
    sget-object v4, Lcom/google/android/gms/location/places/Places;->GeoDataApi:Lcom/google/android/gms/location/places/GeoDataApi;

    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v5, v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    new-array v6, v8, [Ljava/lang/String;

    aput-object v2, v6, v7

    invoke-interface {v4, v5, v6}, Lcom/google/android/gms/location/places/GeoDataApi;->getPlaceById(Lcom/google/android/gms/common/api/GoogleApiClient;[Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v3

    .line 385
    .local v3, "placeResult":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/location/places/PlaceBuffer;>;"
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1000(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/google/android/gms/common/api/ResultCallback;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 387
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "input_method"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 388
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v0, v4, v7}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 390
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mAutocompleteView:Landroid/widget/AutoCompleteTextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->listView:Landroid/widget/ListView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 392
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Called getPlaceById to get Place details for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/navdy/client/debug/PlaceDebugAutocomplete;->placeId:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 393
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$4;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    const v6, 0x7f0803e8

    invoke-virtual {v5, v6}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1100(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
