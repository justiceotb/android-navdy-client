.class public Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
.super Ljava/lang/Object;
.source "NavCoordTestSuite.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
    }
.end annotation


# static fields
.field private static final ON_SCREEN_LOG_MAX_LENGTH:I = 0x1388

.field private static instance:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;


# instance fields
.field private final GOOGLE_WEB_SERVICE_KEY:Ljava/lang/String;

.field private final OUTPUT_FILE:Ljava/lang/String;

.field private coords:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;",
            "Landroid/util/Pair",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;>;"
        }
    .end annotation
.end field

.field private currentDestination:Lcom/navdy/client/app/framework/models/Destination;

.field private currentQueryIndex:I

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private mainThreadHandler:Landroid/os/Handler;

.field private output:Ljava/io/OutputStreamWriter;

.field private final queries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private singleTest:Z

.field private text:Ljava/lang/CharSequence;

.field private textView:Landroid/widget/TextView;

.field private toast:Landroid/widget/Toast;


# direct methods
.method private constructor <init>()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v6, Lcom/navdy/service/library/log/Logger;

    const-class v7, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-direct {v6, v7}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logger:Lcom/navdy/service/library/log/Logger;

    .line 58
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    .line 61
    iput-boolean v9, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->singleTest:Z

    .line 65
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    .line 66
    .local v5, "sdcard":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    const-string v6, "navCoordTestSuite.txt"

    invoke-direct {v3, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 69
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v3}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 71
    .local v0, "br":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .local v4, "line":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 72
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "line":Ljava/lang/String;
    .end local v5    # "sdcard":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 92
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 98
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    iput-object v8, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->mainThreadHandler:Landroid/os/Handler;

    .line 99
    iput-object v8, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->textView:Landroid/widget/TextView;

    .line 100
    iput-object v8, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->output:Ljava/io/OutputStreamWriter;

    .line 101
    const-string v6, ""

    iput-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->text:Ljava/lang/CharSequence;

    .line 103
    const-string v6, "NavCoordTest.json"

    iput-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->OUTPUT_FILE:Ljava/lang/String;

    .line 105
    iput v9, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentQueryIndex:I

    .line 316
    new-instance v6, Ljava/util/HashMap;

    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->values()[Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v7

    array-length v7, v7

    invoke-direct {v6, v7}, Ljava/util/HashMap;-><init>(I)V

    iput-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->coords:Ljava/util/HashMap;

    .line 109
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 110
    .local v1, "context":Landroid/content/Context;
    const v6, 0x7f080557

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->GOOGLE_WEB_SERVICE_KEY:Ljava/lang/String;

    .line 111
    return-void

    .line 74
    .end local v1    # "context":Landroid/content/Context;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "sdcard":Ljava/io/File;
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    goto :goto_1

    .line 77
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "SFO"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "SFO Airport Terimanal 2 - Lower Level"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "Palm Springs International Airport"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "Fort Funston National Park"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "575 7th St, SF, CA"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "Kanishka Gasto pub, Walnut creek"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "7030 Devon way, Berkeley"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "265 Ygnacico valled rd, Walnut creek"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "Apple Store, Walnut Creek"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "wrangell st elias national park"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "Menlo Park Caltrain station"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "Yosemite"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v6, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    const-string v7, "Elektrotehnicka skola rade serbia"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->googleSearchResultsToJsonArray(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->write(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logGoogleStaticMapForThisStep(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logGoogleStaticMapForAllSteps()V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->textView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->text:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/util/List;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->destinationsToJsonArray(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "x2"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->doApiCalls(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->runNextTest()V

    return-void
.end method

.method static synthetic access$700(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;DD)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # D
    .param p4, "x3"    # D

    .prologue
    .line 53
    invoke-direct/range {p0 .. p5}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logCoordinate(Ljava/lang/String;DD)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->coords:Ljava/util/HashMap;

    return-object v0
.end method

.method private addCoordMarker(Ljava/lang/String;Ljava/lang/String;DDDD)Ljava/lang/String;
    .locals 13
    .param p1, "staticMapUrl"    # Ljava/lang/String;
    .param p2, "color"    # Ljava/lang/String;
    .param p3, "dispLatitude"    # D
    .param p5, "dispLongitude"    # D
    .param p7, "navLatitude"    # D
    .param p9, "navLongitude"    # D
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 532
    cmpl-double v0, p3, p7

    if-nez v0, :cond_0

    cmpl-double v0, p5, p9

    if-nez v0, :cond_0

    .line 533
    const-string v3, "B"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->addMarker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object p1

    .line 546
    :goto_0
    return-object p1

    .line 535
    :cond_0
    const-string v3, "D"

    .line 536
    .local v3, "displayLabel":Ljava/lang/String;
    const-string v12, "N"

    .line 537
    .local v12, "navLabel":Ljava/lang/String;
    invoke-static/range {p7 .. p10}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-nez v0, :cond_1

    .line 538
    const-string v3, "C"

    .line 540
    :cond_1
    invoke-static/range {p3 .. p6}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-nez v0, :cond_2

    .line 541
    const-string v12, "M"

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    .line 543
    invoke-direct/range {v0 .. v7}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->addMarker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object p1

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, v12

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    .line 544
    invoke-direct/range {v4 .. v11}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->addMarker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private addMarker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;DD)Ljava/lang/String;
    .locals 2
    .param p1, "staticMapUrl"    # Ljava/lang/String;
    .param p2, "color"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;
    .param p4, "latitude"    # D
    .param p6, "longitude"    # D
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 551
    invoke-static {p4, p5, p6, p7}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-nez v0, :cond_0

    .line 554
    .end local p1    # "staticMapUrl":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "staticMapUrl":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&markers=color:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%7Clabel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%7C"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6, p7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private createOutputFileStreamWriter()Ljava/io/OutputStreamWriter;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 176
    .local v1, "filesDir":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "NavCoordTest.json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    .local v2, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/FileOutputStream;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V

    .line 178
    .local v0, "fileOutputStream":Ljava/io/FileOutputStream;
    new-instance v3, Ljava/io/OutputStreamWriter;

    invoke-direct {v3, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    return-object v3
.end method

.method private destinationsToJsonArray(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 619
    .local p1, "googleSearchDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Destination;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[ "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 621
    .local v2, "jsonString":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 622
    :cond_0
    const-string v3, "[]"

    .line 634
    :goto_0
    return-object v3

    .line 625
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 626
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Destination;

    .line 627
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->toJsonString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 628
    add-int/lit8 v3, v1, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 629
    const-string v3, ",\n    "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 633
    .end local v0    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_3
    const-string v3, " ]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private doApiCalls(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
    .locals 5
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "step"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .prologue
    .line 320
    new-instance v2, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v2, p1}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 322
    .local v2, "theDestination":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v3, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_NATIVE_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    if-eq p2, v3, :cond_0

    sget-object v3, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_WEB_PLACES_SEARCH_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    if-eq p2, v3, :cond_0

    sget-object v3, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->HERE_GEOCODER_WITH_NAME:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    if-ne p2, v3, :cond_1

    .line 325
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 329
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " {\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \"currentDestination\":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 331
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->toJsonString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 329
    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->write(Ljava/lang/String;)V

    .line 334
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n===========\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n==========="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 336
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;-><init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V

    .line 418
    .local v0, "callback":Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;
    new-instance v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$3;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$3;-><init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;)V

    .line 445
    .local v1, "hereCallback":Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;
    sget-object v3, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$6;->$SwitchMap$com$navdy$client$debug$navdebug$NavCoordTestSuite$Step:[I

    invoke-virtual {p2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 467
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PlaceId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Address: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 468
    new-instance v3, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$4;

    invoke-direct {v3, p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$4;-><init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;)V

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    .line 486
    :goto_0
    return-void

    .line 448
    :pswitch_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Address: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 449
    iget-object v3, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder;->makeRequest(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    goto :goto_0

    .line 453
    :pswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Address: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 454
    iget-object v3, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callWebHerePlacesSearchApi(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    goto :goto_0

    .line 458
    :pswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Address: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 459
    iget-object v3, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callWebHereGeocoderApi(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    goto :goto_0

    .line 462
    :pswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PlaceId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Address: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 463
    invoke-static {v2, v0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->getCoordinatesFromGoogleDirectionsWebApi(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;)V

    goto/16 :goto_0

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getInstance()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->instance:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-direct {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;-><init>()V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->instance:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .line 117
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->instance:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    return-object v0
.end method

.method private googleSearchResultsToJsonArray(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 601
    .local p1, "googleResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 603
    .local v1, "jsonString":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 604
    :cond_0
    const-string v2, "[]"

    .line 615
    :goto_0
    return-object v2

    .line 607
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 608
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    iget-object v2, v2, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->jsonString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 609
    add-int/lit8 v2, v0, 0x1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 610
    const-string v2, ",\n    "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 614
    :cond_3
    const-string v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 649
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, p1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 650
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->mainThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;-><init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 662
    return-void
.end method

.method private logCoordinate(Ljava/lang/String;DD)V
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "lat"    # D
    .param p4, "lon"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 560
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "[: ]"

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 564
    .local v1, "encodedName":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://www.google.com/maps?sourceid=chrome&q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 567
    .local v2, "googleMapsUrl":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    cmpl-double v3, p2, v6

    if-nez v3, :cond_0

    cmpl-double v3, p4, v6

    if-eqz v3, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\tmap: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 571
    return-void

    .line 561
    .end local v1    # "encodedName":Ljava/lang/String;
    .end local v2    # "googleMapsUrl":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const-string v1, ""

    .restart local v1    # "encodedName":Ljava/lang/String;
    goto :goto_0

    .line 567
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v2    # "googleMapsUrl":Ljava/lang/String;
    :cond_1
    const-string v3, ""

    goto :goto_1
.end method

.method private logEndOfTest()V
    .locals 1

    .prologue
    .line 594
    const-string v0, " "

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 595
    const-string v0, " @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 596
    const-string v0, " @@@@@@     Test suite finished.     @@@@@@"

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 597
    const-string v0, " @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 598
    return-void
.end method

.method private logGoogleStaticMapForAllSteps()V
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->coords:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logGoogleStaticMapForAllSteps(Ljava/util/Set;)V

    .line 496
    return-void
.end method

.method private logGoogleStaticMapForAllSteps(Ljava/util/Set;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 500
    .local p1, "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;>;"
    const-string v4, "https://maps.googleapis.com/maps/api/staticmap?size=1024x768&maptype=roadmap"

    .line 502
    .local v4, "staticMapUrl":Ljava/lang/String;
    const-string v3, "Map Legend:"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 503
    const-string v3, "red: \t GOOGLE_PLACE_DETAILS"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 504
    const-string v5, "red"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v6, v3, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v8, v3, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v10, v3, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v12, v3, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v13}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->addCoordMarker(Ljava/lang/String;Ljava/lang/String;DDDD)Ljava/lang/String;

    move-result-object v4

    .line 511
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .line 512
    .local v15, "key":Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v15, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->markerColor:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ": \t "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v15}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 513
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->coords:Ljava/util/HashMap;

    invoke-virtual {v3, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 515
    .local v2, "coordinatePair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;>;"
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Lcom/navdy/service/library/events/location/Coordinate;

    move-object v14, v3

    .line 516
    .local v14, "disp":Lcom/navdy/service/library/events/location/Coordinate;
    :goto_1
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v3, :cond_1

    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v16, v3

    .line 517
    .local v16, "nav":Lcom/navdy/service/library/events/location/Coordinate;
    :goto_2
    iget-object v5, v15, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->markerColor:Ljava/lang/String;

    iget-object v3, v14, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    .line 519
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    iget-object v3, v14, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    .line 520
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    .line 521
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    .line 522
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    move-object/from16 v3, p0

    .line 517
    invoke-direct/range {v3 .. v13}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->addCoordMarker(Ljava/lang/String;Ljava/lang/String;DDDD)Ljava/lang/String;

    move-result-object v4

    .line 523
    goto :goto_0

    .line 515
    .end local v14    # "disp":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v16    # "nav":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_0
    new-instance v5, Lcom/navdy/service/library/events/location/Coordinate;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    const-string v13, ""

    invoke-direct/range {v5 .. v13}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    move-object v14, v5

    goto :goto_1

    .line 516
    .restart local v14    # "disp":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_1
    new-instance v5, Lcom/navdy/service/library/events/location/Coordinate;

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    const-string v13, ""

    invoke-direct/range {v5 .. v13}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    move-object/from16 v16, v5

    goto/16 :goto_2

    .line 525
    .end local v2    # "coordinatePair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;>;"
    .end local v14    # "disp":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v15    # "key":Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "&key="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->GOOGLE_WEB_SERVICE_KEY:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 527
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Static map: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 528
    return-void
.end method

.method private logGoogleStaticMapForThisStep(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
    .locals 1
    .param p1, "step"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    .prologue
    .line 489
    new-instance v0, Landroid/support/v4/util/ArraySet;

    invoke-direct {v0}, Landroid/support/v4/util/ArraySet;-><init>()V

    .line 490
    .local v0, "keys":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;>;"
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 491
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logGoogleStaticMapForAllSteps(Ljava/util/Set;)V

    .line 492
    return-void
.end method

.method private runNextTest()V
    .locals 3

    .prologue
    .line 574
    const-string v1, "}"

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->write(Ljava/lang/String;)V

    .line 575
    iget v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentQueryIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentQueryIndex:I

    .line 576
    iget-boolean v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->singleTest:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentQueryIndex:I

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 577
    const-string v1, ",\n"

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->write(Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    iget v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentQueryIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->runTest(Ljava/lang/String;)V

    .line 591
    :goto_0
    return-void

    .line 580
    :cond_0
    const-string v1, "]"

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->write(Ljava/lang/String;)V

    .line 582
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->output:Ljava/io/OutputStreamWriter;

    invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V

    .line 583
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->output:Ljava/io/OutputStreamWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :goto_1
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logEndOfTest()V

    .line 589
    const-string v1, "...Test suite complete"

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->showToast(Ljava/lang/String;)V

    goto :goto_0

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Unable to close the output file (NavCoordTest.json) for nav coord test suite."

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private runTest(Ljava/lang/String;)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{\n  \"search_terms\":\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\",\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->write(Ljava/lang/String;)V

    .line 186
    const-string v0, " "

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 187
    const-string v0, " @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " @@ Testing: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 189
    const-string v0, " "

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 190
    const-string v0, "\n===========\nGoogle Text Search\n==========="

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  search query:      "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 194
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    new-instance v1, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$1;-><init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)V

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V

    .line 277
    invoke-virtual {v0, p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runTextSearchWebApi(Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method private write(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 640
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->output:Ljava/io/OutputStreamWriter;

    if-eqz v1, :cond_0

    .line 641
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->output:Ljava/io/OutputStreamWriter;

    invoke-virtual {v1, p1}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :cond_0
    :goto_0
    return-void

    .line 643
    :catch_0
    move-exception v0

    .line 644
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onPause()V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->textView:Landroid/widget/TextView;

    .line 128
    return-void
.end method

.method public onResume(Landroid/os/Handler;Landroid/widget/TextView;)V
    .locals 1
    .param p1, "mainThreadHandler"    # Landroid/os/Handler;
    .param p2, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->mainThreadHandler:Landroid/os/Handler;

    .line 122
    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->textView:Landroid/widget/TextView;

    .line 123
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->run(Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method public run(Ljava/lang/String;)V
    .locals 5
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 144
    iput v4, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentQueryIndex:I

    .line 146
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 147
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v2

    if-nez v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to test APIs when offline."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 149
    const-string v2, "Unable to test APIs when offline."

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->showToast(Ljava/lang/String;)V

    .line 171
    :goto_0
    return-void

    .line 153
    :cond_0
    const-string v2, "Starting test suite..."

    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->showToast(Ljava/lang/String;)V

    .line 154
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->output:Ljava/io/OutputStreamWriter;

    if-nez v2, :cond_1

    .line 156
    :try_start_0
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->createOutputFileStreamWriter()Ljava/io/OutputStreamWriter;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->output:Ljava/io/OutputStreamWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :cond_1
    :goto_1
    const-string v2, "["

    invoke-direct {p0, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->write(Ljava/lang/String;)V

    .line 164
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 165
    iput-boolean v4, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->singleTest:Z

    .line 166
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->queries:Ljava/util/ArrayList;

    iget v3, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->currentQueryIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->runTest(Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to create or open the output file (NavCoordTest.json) for nav coord test suite."

    invoke-virtual {v2, v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 168
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->singleTest:Z

    .line 169
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->runTest(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->toast:Landroid/widget/Toast;

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->toast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 134
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 135
    .local v0, "context":Landroid/content/Context;
    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->toast:Landroid/widget/Toast;

    .line 136
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->toast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 137
    return-void
.end method
