.class public Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "NavAddressPickerFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f10026c

    const-string v2, "field \'mEditTextDestinationQuery\' and method \'onTextChanged\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .local v0, "view":Landroid/view/View;
    move-object v1, v0

    .line 11
    check-cast v1, Lorg/droidparts/widget/ClearableEditText;

    iput-object v1, p1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    .line 12
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 36
    const v1, 0x7f100269

    const-string v2, "field \'mSearchTypeRadioGroup\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 37
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/RadioGroup;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mSearchTypeRadioGroup:Landroid/widget/RadioGroup;

    .line 38
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    .prologue
    const/4 v0, 0x0

    .line 41
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    .line 42
    iput-object v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mSearchTypeRadioGroup:Landroid/widget/RadioGroup;

    .line 43
    return-void
.end method
