.class Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;
.super Ljava/lang/Object;
.source "NavAddressPickerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;->onCompleted(Lcom/here/android/mpa/search/DiscoveryResultPage;Lcom/here/android/mpa/search/ErrorCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;

.field final synthetic val$data:Lcom/here/android/mpa/search/DiscoveryResultPage;

.field final synthetic val$errorCode:Lcom/here/android/mpa/search/ErrorCode;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;Lcom/here/android/mpa/search/ErrorCode;Lcom/here/android/mpa/search/DiscoveryResultPage;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;

    .prologue
    .line 380
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    iput-object p3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->val$data:Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 383
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    sget-object v3, Lcom/here/android/mpa/search/ErrorCode;->NONE:Lcom/here/android/mpa/search/ErrorCode;

    if-eq v2, v3, :cond_0

    .line 384
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;

    iget-object v2, v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;->this$1:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;

    iget-object v2, v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Search error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->val$errorCode:Lcom/here/android/mpa/search/ErrorCode;

    invoke-virtual {v4}, Lcom/here/android/mpa/search/ErrorCode;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 397
    :goto_0
    return-void

    .line 388
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->val$data:Lcom/here/android/mpa/search/DiscoveryResultPage;

    invoke-virtual {v2}, Lcom/here/android/mpa/search/DiscoveryResultPage;->getPlaceLinks()Ljava/util/List;

    move-result-object v0

    .line 389
    .local v0, "places":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 390
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "returned results: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 391
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;

    iget-object v2, v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;->this$1:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;

    iget-object v2, v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    invoke-virtual {v2, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->addPlaces(Ljava/util/List;)V

    goto :goto_0

    .line 393
    :cond_1
    const-string v1, "No results"

    .line 394
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1$1;->this$2:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;

    iget-object v2, v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3$1;->this$1:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;

    iget-object v2, v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;->this$0:Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    invoke-virtual {v2, v1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 395
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
