.class public Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;
.super Landroid/app/ListFragment;
.source "NavAddressPickerFragment.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;
.implements Lcom/here/android/mpa/common/PositioningManager$OnPositionChangedListener;


# static fields
.field private static final MI_TO_METERS:D = 1690.34

.field private static final SEARCH_RADIUS_METERS:I = 0x2944a

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field protected mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

.field protected mDestinationCoordinates:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;"
        }
    .end annotation
.end field

.field protected mDestinationLabels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10026c
    .end annotation
.end field

.field protected mLastGeocodeRequest:Lcom/here/android/mpa/search/GeocodeRequest;

.field protected mLastSearchRequest:Lcom/here/android/mpa/search/SearchRequest;

.field protected mMainHandler:Landroid/os/Handler;

.field private mMapEngineAvailable:Z

.field mSearchTypeRadioGroup:Landroid/widget/RadioGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100269
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mMapEngineAvailable:Z

    .line 76
    return-void
.end method


# virtual methods
.method protected addDestinations(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "destinationLocations":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/Location;>;"
    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 140
    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationCoordinates:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 141
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/here/android/mpa/search/Location;

    .line 142
    .local v2, "destinationLocation":Lcom/here/android/mpa/search/Location;
    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getLabelForLocation(Lcom/here/android/mpa/search/Location;)Ljava/lang/String;

    move-result-object v3

    .line 143
    .local v3, "label":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-virtual {p0, v2}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getCoordinateForLocation(Lcom/here/android/mpa/search/Location;)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 145
    .local v1, "coord":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationCoordinates:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 148
    .end local v1    # "coord":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v2    # "destinationLocation":Lcom/here/android/mpa/search/Location;
    .end local v3    # "label":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 149
    .local v0, "adapter":Landroid/widget/ArrayAdapter;
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 150
    return-void
.end method

.method protected addPlaces(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/search/PlaceLink;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "placeLocations":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/search/PlaceLink;>;"
    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 154
    iget-object v4, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationCoordinates:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 156
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/here/android/mpa/search/PlaceLink;

    .line 157
    .local v3, "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    invoke-virtual {v3}, Lcom/here/android/mpa/search/PlaceLink;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "label":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    invoke-virtual {v3}, Lcom/here/android/mpa/search/PlaceLink;->getPosition()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v1

    .line 160
    .local v1, "coord":Lcom/here/android/mpa/common/GeoCoordinate;
    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationCoordinates:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    .end local v1    # "coord":Lcom/here/android/mpa/common/GeoCoordinate;
    .end local v2    # "label":Ljava/lang/String;
    .end local v3    # "placeLink":Lcom/here/android/mpa/search/PlaceLink;
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 164
    .local v0, "adapter":Landroid/widget/ArrayAdapter;
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 165
    return-void
.end method

.method protected doAddressSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "queryText"    # Ljava/lang/String;

    .prologue
    .line 410
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$4;-><init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 452
    return-void
.end method

.method protected doPlaceSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "queryText"    # Ljava/lang/String;

    .prologue
    .line 363
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$3;-><init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->addOnInitializedListener(Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 407
    return-void
.end method

.method protected doSearch(Ljava/lang/String;)V
    .locals 2
    .param p1, "queryText"    # Ljava/lang/String;

    .prologue
    .line 341
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mMapEngineAvailable:Z

    if-nez v0, :cond_1

    .line 346
    const-string v0, "Map engine not ready."

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    goto :goto_0

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    if-nez v0, :cond_2

    .line 351
    const-string v0, "No current position."

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    goto :goto_0

    .line 355
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mSearchTypeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f10026a

    if-ne v0, v1, :cond_3

    .line 356
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->doAddressSearch(Ljava/lang/String;)V

    goto :goto_0

    .line 358
    :cond_3
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->doPlaceSearch(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected emptyIfNull(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 206
    if-nez p1, :cond_0

    const-string p1, ""

    .end local p1    # "s":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method protected getCoordinateForLocation(Lcom/here/android/mpa/search/Location;)Lcom/here/android/mpa/common/GeoCoordinate;
    .locals 5
    .param p1, "destinationLocation"    # Lcom/here/android/mpa/search/Location;

    .prologue
    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "destinationCoord":Lcom/here/android/mpa/common/GeoCoordinate;
    const/4 v1, 0x0

    .line 289
    .local v1, "navigationPosition":Lcom/here/android/mpa/search/NavigationPosition;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getAccessPoints()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getAccessPoints()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 290
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getAccessPoints()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "navigationPosition":Lcom/here/android/mpa/search/NavigationPosition;
    check-cast v1, Lcom/here/android/mpa/search/NavigationPosition;

    .line 294
    .restart local v1    # "navigationPosition":Lcom/here/android/mpa/search/NavigationPosition;
    :cond_0
    if-eqz v1, :cond_1

    .line 295
    invoke-virtual {v1}, Lcom/here/android/mpa/search/NavigationPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 299
    :cond_1
    if-nez v0, :cond_2

    .line 300
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v0

    .line 303
    :cond_2
    if-eqz v0, :cond_3

    .line 304
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "text: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v4

    invoke-virtual {v4}, Lcom/here/android/mpa/search/Address;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoCoordinate;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 309
    :goto_0
    return-object v0

    .line 306
    :cond_3
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "couldn\'t build coordinate"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getLabelForLocation(Lcom/here/android/mpa/search/Location;)Ljava/lang/String;
    .locals 13
    .param p1, "location"    # Lcom/here/android/mpa/search/Location;

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v9

    if-nez v9, :cond_1

    .line 172
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->toString()Ljava/lang/String;

    move-result-object v3

    .local v3, "label":Ljava/lang/String;
    :goto_0
    move-object v8, v3

    .line 202
    .end local v3    # "label":Ljava/lang/String;
    :cond_0
    return-object v8

    .line 175
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/search/Location;->getAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v0

    .line 177
    .local v0, "address":Lcom/here/android/mpa/search/Address;
    invoke-virtual {v0}, Lcom/here/android/mpa/search/Address;->getText()Ljava/lang/String;

    move-result-object v8

    .line 179
    .local v8, "text":Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 183
    invoke-virtual {v0}, Lcom/here/android/mpa/search/Address;->getHouseNumber()Ljava/lang/String;

    move-result-object v4

    .line 184
    .local v4, "number":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/here/android/mpa/search/Address;->getStreet()Ljava/lang/String;

    move-result-object v6

    .line 185
    .local v6, "street":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/here/android/mpa/search/Address;->getCity()Ljava/lang/String;

    move-result-object v1

    .line 186
    .local v1, "city":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/here/android/mpa/search/Address;->getState()Ljava/lang/String;

    move-result-object v5

    .line 188
    .local v5, "state":Ljava/lang/String;
    const-string v9, "%s %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p0, v4}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-virtual {p0, v6}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 189
    .local v7, "streetAddress":Ljava/lang/String;
    const-string v9, "%s %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-virtual {p0, v5}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->emptyIfNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, "cityState":Ljava/lang/String;
    invoke-static {v7}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 192
    const-string v9, "%s, %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "label":Ljava/lang/String;
    goto :goto_0

    .line 193
    .end local v3    # "label":Ljava/lang/String;
    :cond_2
    invoke-static {v7}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 194
    move-object v3, v7

    .restart local v3    # "label":Ljava/lang/String;
    goto :goto_0

    .line 195
    .end local v3    # "label":Ljava/lang/String;
    :cond_3
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 196
    move-object v3, v2

    .restart local v3    # "label":Ljava/lang/String;
    goto/16 :goto_0

    .line 198
    .end local v3    # "label":Ljava/lang/String;
    :cond_4
    const-string v3, "(unknown address)"

    .restart local v3    # "label":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method protected hideKeyboard()V
    .locals 3

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 457
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    invoke-virtual {v1}, Lorg/droidparts/widget/ClearableEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 458
    return-void
.end method

.method public initializePosition()V
    .locals 2

    .prologue
    .line 118
    invoke-static {}, Lcom/here/android/mpa/common/PositioningManager;->getInstance()Lcom/here/android/mpa/common/PositioningManager;

    move-result-object v0

    .line 119
    .local v0, "positioningManager":Lcom/here/android/mpa/common/PositioningManager;
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/common/PositioningManager;->addListener(Ljava/lang/ref/WeakReference;)V

    .line 120
    invoke-virtual {v0}, Lcom/here/android/mpa/common/PositioningManager;->isActive()Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    sget-object v1, Lcom/here/android/mpa/common/PositioningManager$LocationMethod;->GPS_NETWORK:Lcom/here/android/mpa/common/PositioningManager$LocationMethod;

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/common/PositioningManager;->start(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;)Z

    .line 124
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 262
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    check-cast v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 264
    .local v1, "info":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationCoordinates:Ljava/util/ArrayList;

    iget v4, v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 266
    .local v0, "destinationCoord":Lcom/here/android/mpa/common/GeoCoordinate;
    if-nez v0, :cond_0

    .line 267
    const-string v3, "Unable to find coordinate."

    invoke-virtual {p0, v3}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showError(Ljava/lang/String;)V

    .line 281
    :goto_0
    return v2

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->hideKeyboard()V

    .line 273
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 281
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0

    .line 275
    :pswitch_0
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v3

    invoke-virtual {p0, v3, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->startNavigation(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V

    goto :goto_0

    .line 278
    :pswitch_1
    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    invoke-virtual {v3}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v3

    invoke-virtual {p0, v3, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->startRouteBrowser(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x7f10041a
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->getInstance()Lcom/here/android/mpa/common/MapEngine;

    move-result-object v1

    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lcom/here/android/mpa/common/MapEngine;->init(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 84
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 86
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/here/android/mpa/common/MapEngine;->getInstance()Lcom/here/android/mpa/common/MapEngine;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, Lcom/here/android/mpa/common/MapEngine;->init(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)V

    .line 88
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 89
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationCoordinates:Ljava/util/ArrayList;

    .line 92
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x1090003

    const v4, 0x1020014

    iget-object v5, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mMainHandler:Landroid/os/Handler;

    .line 96
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 100
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListFragment;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 101
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 102
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f110005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 103
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 212
    const v1, 0x7f030092

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 213
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 215
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    new-instance v2, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$1;-><init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;)V

    invoke-virtual {v1, v2}, Lorg/droidparts/widget/ClearableEditText;->setListener(Lorg/droidparts/widget/ClearableEditText$Listener;)V

    .line 222
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 245
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 246
    return-void
.end method

.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 107
    sget-object v0, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-ne p1, v0, :cond_0

    .line 108
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "MapEngine initialized."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->initializePosition()V

    .line 114
    :goto_0
    return-void

    .line 111
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MapEngine init completed with error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 257
    invoke-virtual {p1, p2}, Landroid/widget/ListView;->showContextMenuForChild(Landroid/view/View;)Z

    .line 258
    return-void
.end method

.method public onPositionFixChanged(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/PositioningManager$LocationStatus;)V
    .locals 3
    .param p1, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p2, "locationStatus"    # Lcom/here/android/mpa/common/PositioningManager$LocationStatus;

    .prologue
    .line 135
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPositionFixChanged: method:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 136
    return-void
.end method

.method public onPositionUpdated(Lcom/here/android/mpa/common/PositioningManager$LocationMethod;Lcom/here/android/mpa/common/GeoPosition;Z)V
    .locals 3
    .param p1, "locationMethod"    # Lcom/here/android/mpa/common/PositioningManager$LocationMethod;
    .param p2, "geoPosition"    # Lcom/here/android/mpa/common/GeoPosition;
    .param p3, "b"    # Z

    .prologue
    .line 128
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPositionUpdated method: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/here/android/mpa/common/GeoPosition;->getCoordinate()Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoCoordinate;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 129
    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mCurrentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mMapEngineAvailable:Z

    .line 131
    return-void
.end method

.method public onQueryTypeCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedid"    # I

    .prologue
    .line 335
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    invoke-virtual {v1}, Lorg/droidparts/widget/ClearableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "queryText":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->doSearch(Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 241
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->refreshUI()V

    .line 242
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I
    .annotation build Lbutterknife/OnTextChanged;
        value = {
            0x7f10026c
        }
    .end annotation

    .prologue
    .line 329
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 331
    .local v0, "queryText":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->doSearch(Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 227
    invoke-super {p0, p1, p2}, Landroid/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 228
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 229
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mSearchTypeRadioGroup:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment$2;-><init>(Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 235
    return-void
.end method

.method public refreshUI()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mDestinationLabels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->showKeyboard()V

    .line 252
    :cond_0
    return-void
.end method

.method protected showError(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 467
    return-void
.end method

.method protected showKeyboard()V
    .locals 3

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 462
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->mEditTextDestinationQuery:Lorg/droidparts/widget/ClearableEditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 463
    return-void
.end method

.method public startNavigation(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 2
    .param p1, "start"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "end"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->createIntentWithCoords(Landroid/content/Context;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Landroid/content/Intent;

    move-result-object v0

    .line 314
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->startActivity(Landroid/content/Intent;)V

    .line 315
    return-void
.end method

.method public startRouteBrowser(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)V
    .locals 3
    .param p1, "start"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "end"    # Lcom/here/android/mpa/common/GeoCoordinate;

    .prologue
    .line 318
    invoke-static {p1, p2}, Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;->newInstance(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;

    move-result-object v1

    .line 320
    .local v1, "routeBrowserFragment":Lcom/navdy/client/debug/navdebug/NavRouteBrowserFragment;
    invoke-virtual {p0}, Lcom/navdy/client/debug/navdebug/NavAddressPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 321
    .local v0, "ft":Landroid/app/FragmentTransaction;
    const v2, 0x7f1000c6

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 322
    const/16 v2, 0x1003

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 323
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 324
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 325
    return-void
.end method
