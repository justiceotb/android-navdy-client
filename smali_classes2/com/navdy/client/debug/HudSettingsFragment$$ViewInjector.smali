.class public Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "HudSettingsFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/HudSettingsFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/HudSettingsFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f100255

    const-string v2, "field \'autoBrightness\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .local v0, "view":Landroid/view/View;
    move-object v1, v0

    .line 11
    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/navdy/client/debug/HudSettingsFragment;->autoBrightness:Landroid/widget/CheckBox;

    .line 12
    new-instance v1, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/HudSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 20
    const v1, 0x7f100257

    const-string v2, "field \'brightness\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    check-cast v0, Landroid/widget/SeekBar;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->brightness:Landroid/widget/SeekBar;

    .line 22
    const v1, 0x7f100256

    const-string v2, "field \'brightnessTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->brightnessTextView:Landroid/widget/TextView;

    .line 24
    const v1, 0x7f10025a

    const-string v2, "field \'ledBrightness\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/SeekBar;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightness:Landroid/widget/SeekBar;

    .line 26
    const v1, 0x7f100259

    const-string v2, "field \'ledBrightnessTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightnessTextView:Landroid/widget/TextView;

    .line 28
    const v1, 0x7f10025b

    const-string v2, "field \'enableGesture\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "view":Landroid/view/View;
    move-object v1, v0

    .line 29
    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/navdy/client/debug/HudSettingsFragment;->enableGesture:Landroid/widget/CheckBox;

    .line 30
    new-instance v1, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$2;-><init>(Lcom/navdy/client/debug/HudSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const v1, 0x7f10025c

    const-string v2, "field \'enablePreview\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 39
    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/navdy/client/debug/HudSettingsFragment;->enablePreview:Landroid/widget/CheckBox;

    .line 40
    new-instance v1, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$3;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$3;-><init>(Lcom/navdy/client/debug/HudSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    const v1, 0x7f100261

    const-string v2, "field \'mapTilt\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 49
    check-cast v0, Landroid/widget/SeekBar;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->mapTilt:Landroid/widget/SeekBar;

    .line 50
    const v1, 0x7f10025f

    const-string v2, "field \'mapZoomLevel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 51
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/SeekBar;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->mapZoomLevel:Landroid/widget/SeekBar;

    .line 52
    const v1, 0x7f10025e

    const-string v2, "field \'zoomLevelTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 53
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->zoomLevelTextView:Landroid/widget/TextView;

    .line 54
    const v1, 0x7f100260

    const-string v2, "field \'tiltLevelTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 55
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->tiltLevelTextView:Landroid/widget/TextView;

    .line 56
    const v1, 0x7f10025d

    const-string v2, "field \'enableVideoLoop\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "view":Landroid/view/View;
    move-object v1, v0

    .line 57
    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/navdy/client/debug/HudSettingsFragment;->enableVideoLoop:Landroid/widget/CheckBox;

    .line 58
    new-instance v1, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$4;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$4;-><init>(Lcom/navdy/client/debug/HudSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v1, 0x7f100252

    const-string v2, "field \'voiceSearchLabel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 67
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->voiceSearchLabel:Landroid/widget/TextView;

    .line 68
    const v1, 0x7f100253

    const-string v2, "field \'enableSpecialVoiceSearch\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 69
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/CheckBox;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/HudSettingsFragment;->enableSpecialVoiceSearch:Landroid/widget/CheckBox;

    .line 70
    const v1, 0x7f100262

    const-string v2, "method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 71
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$5;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/HudSettingsFragment$$ViewInjector$5;-><init>(Lcom/navdy/client/debug/HudSettingsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/HudSettingsFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/HudSettingsFragment;

    .prologue
    const/4 v0, 0x0

    .line 82
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->autoBrightness:Landroid/widget/CheckBox;

    .line 83
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->brightness:Landroid/widget/SeekBar;

    .line 84
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->brightnessTextView:Landroid/widget/TextView;

    .line 85
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightness:Landroid/widget/SeekBar;

    .line 86
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->ledBrightnessTextView:Landroid/widget/TextView;

    .line 87
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enableGesture:Landroid/widget/CheckBox;

    .line 88
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enablePreview:Landroid/widget/CheckBox;

    .line 89
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->mapTilt:Landroid/widget/SeekBar;

    .line 90
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->mapZoomLevel:Landroid/widget/SeekBar;

    .line 91
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->zoomLevelTextView:Landroid/widget/TextView;

    .line 92
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->tiltLevelTextView:Landroid/widget/TextView;

    .line 93
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enableVideoLoop:Landroid/widget/CheckBox;

    .line 94
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->voiceSearchLabel:Landroid/widget/TextView;

    .line 95
    iput-object v0, p0, Lcom/navdy/client/debug/HudSettingsFragment;->enableSpecialVoiceSearch:Landroid/widget/CheckBox;

    .line 96
    return-void
.end method
