.class public final enum Lcom/navdy/client/debug/util/S3Constants$BuildSource;
.super Ljava/lang/Enum;
.source "S3Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/util/S3Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BuildSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/util/S3Constants$BuildSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

.field public static final enum BETA:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

.field public static final enum RELEASE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

.field public static final enum STABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

.field public static final enum UNSTABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;


# instance fields
.field private bucket:Ljava/lang/String;

.field private buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

.field private labelResourceId:I

.field private prefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    const-string v1, "STABLE"

    const-string v3, "navdy-builds"

    const-string v4, "OTA"

    sget-object v5, Lcom/navdy/client/debug/util/S3Constants$BuildType;->eng:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    const v6, 0x7f080539

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/navdy/client/debug/util/S3Constants$BuildType;I)V

    sput-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->STABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    .line 35
    new-instance v3, Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    const-string v4, "UNSTABLE"

    const-string v6, "navdy-builds"

    const-string v7, "OTA/unstable"

    sget-object v8, Lcom/navdy/client/debug/util/S3Constants$BuildType;->eng:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    const v9, 0x7f080537

    move v5, v10

    invoke-direct/range {v3 .. v9}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/navdy/client/debug/util/S3Constants$BuildType;I)V

    sput-object v3, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->UNSTABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    .line 36
    new-instance v3, Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    const-string v4, "BETA"

    const-string v6, "navdy-prod-release"

    const-string v7, "OTA/beta"

    sget-object v8, Lcom/navdy/client/debug/util/S3Constants$BuildType;->user:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    const v9, 0x7f080536

    move v5, v11

    invoke-direct/range {v3 .. v9}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/navdy/client/debug/util/S3Constants$BuildType;I)V

    sput-object v3, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->BETA:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    .line 37
    new-instance v3, Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    const-string v4, "RELEASE"

    const-string v6, "navdy-prod-release"

    const-string v7, "OTA/release"

    sget-object v8, Lcom/navdy/client/debug/util/S3Constants$BuildType;->user:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    const v9, 0x7f080538

    move v5, v12

    invoke-direct/range {v3 .. v9}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/navdy/client/debug/util/S3Constants$BuildType;I)V

    sput-object v3, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->RELEASE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->STABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->UNSTABLE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v10

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->BETA:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v11

    sget-object v1, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->RELEASE:Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    aput-object v1, v0, v12

    sput-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->$VALUES:[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/navdy/client/debug/util/S3Constants$BuildType;I)V
    .locals 0
    .param p3, "bucket"    # Ljava/lang/String;
    .param p4, "prefix"    # Ljava/lang/String;
    .param p5, "type"    # Lcom/navdy/client/debug/util/S3Constants$BuildType;
    .param p6, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/client/debug/util/S3Constants$BuildType;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput-object p4, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->prefix:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->bucket:Ljava/lang/String;

    .line 65
    iput-object p5, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    .line 66
    iput p6, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->labelResourceId:I

    .line 67
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->$VALUES:[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/util/S3Constants$BuildSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    return-object v0
.end method


# virtual methods
.method public getBucket()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->bucket:Ljava/lang/String;

    return-object v0
.end method

.method public getBuildType()Lcom/navdy/client/debug/util/S3Constants$BuildType;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->buildType:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    return-object v0
.end method

.method public getLabelResourceId()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->labelResourceId:I

    return v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->prefix:Ljava/lang/String;

    return-object v0
.end method
