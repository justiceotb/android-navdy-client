.class public Lcom/navdy/client/debug/PlaceAutocompleteAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PlaceAutocompleteAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/navdy/client/debug/PlaceDebugAutocomplete;",
        ">;",
        "Landroid/widget/Filterable;"
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private mPlaceFilter:Lcom/google/android/gms/location/places/AutocompleteFilter;

.field private mResultList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/debug/PlaceDebugAutocomplete;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .param p3, "bounds"    # Lcom/google/android/gms/maps/model/LatLngBounds;
    .param p4, "filter"    # Lcom/google/android/gms/location/places/AutocompleteFilter;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 86
    iput-object p3, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 87
    iput-object p4, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mPlaceFilter:Lcom/google/android/gms/location/places/AutocompleteFilter;

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/PlaceAutocompleteAdapter;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mResultList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/PlaceAutocompleteAdapter;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mResultList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/PlaceAutocompleteAdapter;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->getAutocomplete(Ljava/lang/CharSequence;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private getAutocomplete(Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .locals 12
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/debug/PlaceDebugAutocomplete;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 179
    iget-object v7, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v7, :cond_2

    .line 180
    sget-object v7, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Starting autocomplete query for: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 184
    sget-object v7, Lcom/google/android/gms/location/places/Places;->GeoDataApi:Lcom/google/android/gms/location/places/GeoDataApi;

    iget-object v8, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 186
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget-object v11, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mPlaceFilter:Lcom/google/android/gms/location/places/AutocompleteFilter;

    invoke-interface {v7, v8, v9, v10, v11}, Lcom/google/android/gms/location/places/GeoDataApi;->getAutocompletePredictions(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v4

    .line 191
    .local v4, "results":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;>;"
    const-wide/16 v8, 0x3c

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 192
    invoke-virtual {v4, v8, v9, v7}, Lcom/google/android/gms/common/api/PendingResult;->await(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/Result;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;

    .line 195
    .local v0, "autocompletePredictions":Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v5

    .line 196
    .local v5, "status":Lcom/google/android/gms/common/api/Status;
    invoke-virtual {v5}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v7

    if-nez v7, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error contacting API: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/Status;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    .line 198
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 199
    sget-object v7, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error getting autocomplete prediction API call: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/Status;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 200
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;->release()V

    move-object v3, v6

    .line 225
    .end local v0    # "autocompletePredictions":Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;
    .end local v4    # "results":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;>;"
    .end local v5    # "status":Lcom/google/android/gms/common/api/Status;
    :goto_0
    return-object v3

    .line 204
    .restart local v0    # "autocompletePredictions":Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;
    .restart local v4    # "results":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;>;"
    .restart local v5    # "status":Lcom/google/android/gms/common/api/Status;
    :cond_0
    sget-object v7, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Query completed. Received "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " predictions."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 210
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 211
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/gms/location/places/AutocompletePrediction;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;->getCount()I

    move-result v7

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 212
    .local v3, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/debug/PlaceDebugAutocomplete;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 213
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/location/places/AutocompletePrediction;

    .line 215
    .local v2, "prediction":Lcom/google/android/gms/location/places/AutocompletePrediction;
    new-instance v7, Lcom/navdy/client/debug/PlaceDebugAutocomplete;

    invoke-interface {v2}, Lcom/google/android/gms/location/places/AutocompletePrediction;->getPlaceId()Ljava/lang/String;

    move-result-object v8

    .line 216
    invoke-interface {v2, v6}, Lcom/google/android/gms/location/places/AutocompletePrediction;->getFullText(Landroid/text/style/CharacterStyle;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/navdy/client/debug/PlaceDebugAutocomplete;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 215
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 220
    .end local v2    # "prediction":Lcom/google/android/gms/location/places/AutocompletePrediction;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;->release()V

    goto :goto_0

    .line 224
    .end local v0    # "autocompletePredictions":Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/gms/location/places/AutocompletePrediction;>;"
    .end local v3    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/debug/PlaceDebugAutocomplete;>;"
    .end local v4    # "results":Lcom/google/android/gms/common/api/PendingResult;, "Lcom/google/android/gms/common/api/PendingResult<Lcom/google/android/gms/location/places/AutocompletePredictionBuffer;>;"
    .end local v5    # "status":Lcom/google/android/gms/common/api/Status;
    :cond_2
    sget-object v7, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "Google API client is not connected for autocomplete query."

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    move-object v3, v6

    .line 225
    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mResultList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mResultList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter$1;-><init>(Lcom/navdy/client/debug/PlaceAutocompleteAdapter;)V

    return-object v0
.end method

.method public getItem(I)Lcom/navdy/client/debug/PlaceDebugAutocomplete;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mResultList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/PlaceDebugAutocomplete;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->getItem(I)Lcom/navdy/client/debug/PlaceDebugAutocomplete;

    move-result-object v0

    return-object v0
.end method

.method public setBounds(Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 0
    .param p1, "bounds"    # Lcom/google/android/gms/maps/model/LatLngBounds;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 111
    return-void
.end method

.method public setGoogleApiClient(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .locals 1
    .param p1, "googleApiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;

    .prologue
    .line 97
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 98
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 104
    :goto_0
    return-void

    .line 100
    :cond_1
    iput-object p1, p0, Lcom/navdy/client/debug/PlaceAutocompleteAdapter;->mGoogleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    goto :goto_0
.end method
