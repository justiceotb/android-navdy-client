.class public Lcom/navdy/client/ota/model/UpdateInfo;
.super Ljava/lang/Object;
.source "UpdateInfo.java"


# static fields
.field private static final DEFAULT_RELEASE_NOTES_LANGUAGE_TAG:Ljava/lang/String; = "en"

.field private static final META_DATA_RELEASE_NOTES_TAG:Ljava/lang/String; = "release_notes"

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public description:Ljava/lang/String;

.field public fromVersion:I

.field public incremental:Z

.field public metaData:Ljava/lang/String;

.field public size:J

.field public url:Ljava/lang/String;

.field public version:I

.field public versionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/ota/model/UpdateInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractReleaseNotes(Lcom/navdy/client/ota/model/UpdateInfo;)Ljava/lang/String;
    .locals 8
    .param p0, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    const/4 v5, 0x0

    .line 50
    sget-object v6, Lcom/navdy/client/ota/model/UpdateInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Parsing meta data to get release notes"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 51
    if-nez p0, :cond_0

    .line 52
    sget-object v6, Lcom/navdy/client/ota/model/UpdateInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "updateInfo is null"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    move-object v4, v5

    .line 74
    :goto_0
    return-object v4

    .line 55
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/ota/model/UpdateInfo;->metaData:Ljava/lang/String;

    .line 56
    .local v2, "metaData":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 57
    sget-object v6, Lcom/navdy/client/ota/model/UpdateInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Meta data node present"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 59
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 60
    .local v1, "json":Lorg/json/JSONObject;
    const-string v6, "release_notes"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 61
    .local v3, "releaseNotes":Lorg/json/JSONObject;
    const/4 v4, 0x0

    .line 62
    .local v4, "releaseNotesText":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 63
    const-string v6, "en"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    goto :goto_0

    .line 66
    :cond_1
    sget-object v6, Lcom/navdy/client/ota/model/UpdateInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Release notes node not found"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v3    # "releaseNotes":Lorg/json/JSONObject;
    .end local v4    # "releaseNotesText":Ljava/lang/String;
    :goto_1
    move-object v4, v5

    .line 74
    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Lorg/json/JSONException;
    sget-object v6, Lcom/navdy/client/ota/model/UpdateInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Error parsing the JSON metadata "

    invoke-virtual {v6, v7, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 72
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    sget-object v6, Lcom/navdy/client/ota/model/UpdateInfo;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Meta data node not present"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getFormattedVersionName(Lcom/navdy/client/ota/model/UpdateInfo;)Ljava/lang/String;
    .locals 2
    .param p0, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/navdy/client/ota/model/UpdateInfo;->versionName:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/navdy/client/ota/model/UpdateInfo;->versionName:Ljava/lang/String;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 81
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 34
    instance-of v1, p1, Lcom/navdy/client/ota/model/UpdateInfo;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/navdy/client/ota/model/UpdateInfo;

    .line 37
    .local v0, "other":Lcom/navdy/client/ota/model/UpdateInfo;
    iget v1, v0, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    iget v2, p0, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/navdy/client/ota/model/UpdateInfo;->versionName:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/client/ota/model/UpdateInfo;->versionName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/navdy/client/ota/model/UpdateInfo;->incremental:Z

    iget-boolean v2, p0, Lcom/navdy/client/ota/model/UpdateInfo;->incremental:Z

    if-ne v1, v2, :cond_0

    iget v1, v0, Lcom/navdy/client/ota/model/UpdateInfo;->fromVersion:I

    iget v2, p0, Lcom/navdy/client/ota/model/UpdateInfo;->fromVersion:I

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/navdy/client/ota/model/UpdateInfo;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/navdy/client/ota/model/UpdateInfo;->url:Ljava/lang/String;

    .line 39
    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 41
    .end local v0    # "other":Lcom/navdy/client/ota/model/UpdateInfo;
    :goto_0
    return v1

    .line 39
    .restart local v0    # "other":Lcom/navdy/client/ota/model/UpdateInfo;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 41
    .end local v0    # "other":Lcom/navdy/client/ota/model/UpdateInfo;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method
