.class public final Lcom/navdy/client/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_email:I = 0x7f100101

.field public static final account_name:I = 0x7f100102

.field public static final account_photo:I = 0x7f100100

.field public static final action0:I = 0x7f100319

.field public static final action_bar:I = 0x7f100099

.field public static final action_bar_activity_content:I = 0x7f100000

.field public static final action_bar_container:I = 0x7f100098

.field public static final action_bar_root:I = 0x7f100094

.field public static final action_bar_spinner:I = 0x7f100001

.field public static final action_bar_subtitle:I = 0x7f100077

.field public static final action_bar_title:I = 0x7f100076

.field public static final action_container:I = 0x7f100316

.field public static final action_context_bar:I = 0x7f10009a

.field public static final action_divider:I = 0x7f10031d

.field public static final action_image:I = 0x7f100317

.field public static final action_menu_divider:I = 0x7f100002

.field public static final action_menu_presenter:I = 0x7f100003

.field public static final action_mode_bar:I = 0x7f100096

.field public static final action_mode_bar_stub:I = 0x7f100095

.field public static final action_mode_close_button:I = 0x7f100078

.field public static final action_text:I = 0x7f100318

.field public static final actions:I = 0x7f100325

.field public static final active_trip_card:I = 0x7f1000b0

.field public static final activity_chooser_view_content:I = 0x7f100079

.field public static final activity_contact_zendesk_root:I = 0x7f1000c5

.field public static final activity_network_no_connectivity:I = 0x7f1002ef

.field public static final activity_request_fragment:I = 0x7f1000d4

.field public static final activity_request_list_add_icon:I = 0x7f100409

.field public static final activity_request_view_root:I = 0x7f1000de

.field public static final add:I = 0x7f10002b

.field public static final addFavoriteLabel:I = 0x7f1000e7

.field public static final addHome:I = 0x7f1000e6

.field public static final addHomeLabel:I = 0x7f1000e8

.field public static final addWork:I = 0x7f1000e5

.field public static final addWorkLabel:I = 0x7f1000e9

.field public static final add_to_favorites_button:I = 0x7f100121

.field public static final address_input:I = 0x7f10010d

.field public static final address_label:I = 0x7f10014f

.field public static final adjust_height:I = 0x7f10005c

.field public static final adjust_width:I = 0x7f10005d

.field public static final advanced:I = 0x7f10038c

.field public static final alertTitle:I = 0x7f10008d

.field public static final alignBounds:I = 0x7f100055

.field public static final alignMargins:I = 0x7f100056

.field public static final all:I = 0x7f100019

.field public static final allow_calls_icon:I = 0x7f1002ca

.field public static final allow_calls_switch:I = 0x7f1002cb

.field public static final allow_facebook_icon:I = 0x7f1002d4

.field public static final allow_facebook_messenger_icon:I = 0x7f1002dc

.field public static final allow_facebook_messenger_switch:I = 0x7f1002dd

.field public static final allow_facebook_switch:I = 0x7f1002d5

.field public static final allow_fuel_icon:I = 0x7f1002cc

.field public static final allow_fuel_switch:I = 0x7f1002cd

.field public static final allow_gestures_group:I = 0x7f1003a7

.field public static final allow_gestures_switch:I = 0x7f1003a6

.field public static final allow_gestures_switch_group:I = 0x7f1003a4

.field public static final allow_glances_switch:I = 0x7f1002bc

.field public static final allow_gmail_icon:I = 0x7f1002d6

.field public static final allow_gmail_switch:I = 0x7f1002d7

.field public static final allow_google_calendar_icon:I = 0x7f1002d8

.field public static final allow_google_calendar_switch:I = 0x7f1002d9

.field public static final allow_hangouts_icon:I = 0x7f1002da

.field public static final allow_hangouts_switch:I = 0x7f1002db

.field public static final allow_music_icon:I = 0x7f1002ce

.field public static final allow_music_switch:I = 0x7f1002cf

.field public static final allow_slack_icon:I = 0x7f1002de

.field public static final allow_slack_switch:I = 0x7f1002df

.field public static final allow_sms_icon:I = 0x7f1002e0

.field public static final allow_sms_switch:I = 0x7f1002e1

.field public static final allow_traffic_icon:I = 0x7f1002d0

.field public static final allow_traffic_switch:I = 0x7f1002d1

.field public static final allow_twitter_icon:I = 0x7f1002e2

.field public static final allow_twitter_switch:I = 0x7f1002e3

.field public static final allow_whatsapp_icon:I = 0x7f1002e4

.field public static final allow_whatsapp_switch:I = 0x7f1002e5

.field public static final already_installed:I = 0x7f1001c6

.field public static final always:I = 0x7f100043

.field public static final always_button:I = 0x7f10013d

.field public static final app_icon:I = 0x7f1000df

.field public static final app_list:I = 0x7f1000f7

.field public static final app_list_empty_view:I = 0x7f1000f8

.field public static final app_name:I = 0x7f1000e0

.field public static final app_version:I = 0x7f10021d

.field public static final app_version_title:I = 0x7f1003d9

.field public static final article_attachment_row_filename_text:I = 0x7f10034a

.field public static final article_attachment_row_filesize_text:I = 0x7f10034b

.field public static final article_title:I = 0x7f100349

.field public static final attach_image:I = 0x7f10039a

.field public static final attach_photo:I = 0x7f100399

.field public static final attach_photo_icon:I = 0x7f10039b

.field public static final attach_photo_icon_2:I = 0x7f10039c

.field public static final attach_photo_icon_3:I = 0x7f10039d

.field public static final attach_photo_icon_4:I = 0x7f10039e

.field public static final attach_photo_icon_5:I = 0x7f10039f

.field public static final attachment_delete:I = 0x7f1003f8

.field public static final attachment_image:I = 0x7f1003f6

.field public static final attachment_inline_container:I = 0x7f1003f9

.field public static final attachment_inline_image:I = 0x7f1003fa

.field public static final attachment_inline_progressbar:I = 0x7f1003fb

.field public static final attachment_progress:I = 0x7f1003f7

.field public static final audio_output_device_name:I = 0x7f100134

.field public static final audio_settings_description:I = 0x7f100370

.field public static final audio_settings_speech_level:I = 0x7f100377

.field public static final auto:I = 0x7f100032

.field public static final autoBrightness:I = 0x7f100255

.field public static final auto_on:I = 0x7f100389

.field public static final auto_on_title:I = 0x7f10038a

.field public static final auto_recalc:I = 0x7f1003c0

.field public static final auto_recalc_title:I = 0x7f1003c1

.field public static final auto_trains:I = 0x7f1003c8

.field public static final back:I = 0x7f100184

.field public static final backBtn:I = 0x7f1001f2

.field public static final back_button:I = 0x7f100165

.field public static final background:I = 0x7f100150

.field public static final badge:I = 0x7f10011f

.field public static final basic:I = 0x7f10001a

.field public static final beginning:I = 0x7f10005a

.field public static final belvedere_dialog_listview:I = 0x7f1000fb

.field public static final belvedere_dialog_row_image:I = 0x7f1000fc

.field public static final belvedere_dialog_row_text:I = 0x7f1000fd

.field public static final bg_view_pager:I = 0x7f1001e9

.field public static final blue_text:I = 0x7f10016c

.field public static final bluetooth:I = 0x7f100374

.field public static final bluetooth_device_list_row:I = 0x7f10015a

.field public static final bluetooth_recycler_view:I = 0x7f100164

.field public static final bottom:I = 0x7f100033

.field public static final bottom_bar:I = 0x7f100197

.field public static final bottom_bar_divider:I = 0x7f100198

.field public static final bottom_card:I = 0x7f10016b

.field public static final bottom_item_divider:I = 0x7f10035f

.field public static final bottom_padding:I = 0x7f10020c

.field public static final bottom_separator:I = 0x7f100155

.field public static final bottom_sheet_container:I = 0x7f1000ab

.field public static final bottom_sheet_linear_layout:I = 0x7f100126

.field public static final box_photo:I = 0x7f100182

.field public static final brightness:I = 0x7f100257

.field public static final brightnessLabel:I = 0x7f100254

.field public static final brightnessTextView:I = 0x7f100256

.field public static final btn_cancel:I = 0x7f10010b

.field public static final btn_close:I = 0x7f10012e

.field public static final btn_confirm_audio_turned_up:I = 0x7f100130

.field public static final btn_done:I = 0x7f10010c

.field public static final btn_learn_more:I = 0x7f1003a9

.field public static final btn_submit:I = 0x7f1003ed

.field public static final btn_test_audio:I = 0x7f100371

.field public static final build_type:I = 0x7f1003d4

.field public static final build_type_desc:I = 0x7f1003d6

.field public static final button:I = 0x7f100132

.field public static final button10:I = 0x7f10024e

.field public static final button2:I = 0x7f100339

.field public static final button3:I = 0x7f10010e

.field public static final button4:I = 0x7f100219

.field public static final button7:I = 0x7f10024b

.field public static final button8:I = 0x7f10024c

.field public static final button9:I = 0x7f10024d

.field public static final buttonPanel:I = 0x7f100080

.field public static final button_add_response:I = 0x7f1002a1

.field public static final button_attachment:I = 0x7f10029c

.field public static final button_layout_lens_ok:I = 0x7f1001cf

.field public static final button_layout_lens_too_high:I = 0x7f1001ca

.field public static final button_layout_lens_too_low_dot:I = 0x7f1001d4

.field public static final button_login:I = 0x7f1002a6

.field public static final button_refresh:I = 0x7f1002a2

.field public static final button_send:I = 0x7f10029d

.field public static final button_text:I = 0x7f100138

.field public static final button_toggle_navigation:I = 0x7f1000cd

.field public static final button_update:I = 0x7f1002aa

.field public static final buy_one:I = 0x7f1001f0

.field public static final calendar_name:I = 0x7f100105

.field public static final calendar_owner:I = 0x7f100106

.field public static final camera_warnings:I = 0x7f100382

.field public static final cancel:I = 0x7f1001dc

.field public static final cancel_action:I = 0x7f10031a

.field public static final cancel_button:I = 0x7f10013b

.field public static final car_info:I = 0x7f100387

.field public static final car_selector:I = 0x7f100203

.field public static final card:I = 0x7f100146

.field public static final card_first_line:I = 0x7f100153

.field public static final card_row:I = 0x7f100151

.field public static final card_second_line:I = 0x7f100154

.field public static final card_sub_illustration_text:I = 0x7f1002f4

.field public static final card_third_line:I = 0x7f1002f6

.field public static final category_title:I = 0x7f10034c

.field public static final center:I = 0x7f100034

.field public static final center_horizontal:I = 0x7f100035

.field public static final center_on_driver_button:I = 0x7f1000ae

.field public static final center_vertical:I = 0x7f100036

.field public static final chains:I = 0x7f10001b

.field public static final changing:I = 0x7f100044

.field public static final check_1:I = 0x7f1001bd

.field public static final check_2:I = 0x7f1001bf

.field public static final check_3:I = 0x7f1001c1

.field public static final check_4:I = 0x7f1001e3

.field public static final check_text_1:I = 0x7f1001be

.field public static final check_text_2:I = 0x7f1001c0

.field public static final check_text_3:I = 0x7f1001c2

.field public static final check_text_4:I = 0x7f1001e4

.field public static final checkbox:I = 0x7f100090

.field public static final checkmark:I = 0x7f100104

.field public static final chevron:I = 0x7f1000b8

.field public static final chronometer:I = 0x7f100322

.field public static final circle:I = 0x7f100047

.field public static final cla:I = 0x7f100188

.field public static final cla_description:I = 0x7f10018e

.field public static final cla_illustration:I = 0x7f10018c

.field public static final cla_title:I = 0x7f10018d

.field public static final clip_horizontal:I = 0x7f10003f

.field public static final clip_vertical:I = 0x7f100040

.field public static final close:I = 0x7f100131

.field public static final collapseActionView:I = 0x7f100061

.field public static final color:I = 0x7f100103

.field public static final compact:I = 0x7f100393

.field public static final compact_ui_linear_layout:I = 0x7f100390

.field public static final connectionStateIndicator:I = 0x7f100226

.field public static final connection_failure_banner:I = 0x7f1000f0

.field public static final connection_failure_banner_body:I = 0x7f1000f2

.field public static final connection_failure_banner_button:I = 0x7f1000f4

.field public static final connection_failure_banner_icon:I = 0x7f1000f3

.field public static final connection_failure_banner_subtext:I = 0x7f1000f6

.field public static final connection_failure_banner_text:I = 0x7f1000f5

.field public static final connection_status:I = 0x7f100263

.field public static final connection_success_banner:I = 0x7f1000ea

.field public static final connection_success_banner_body:I = 0x7f1000ec

.field public static final connection_success_banner_icon:I = 0x7f1000ed

.field public static final connection_success_banner_subtext:I = 0x7f1000ef

.field public static final connection_success_banner_text:I = 0x7f1000ee

.field public static final connection_type_bt:I = 0x7f1002fb

.field public static final connection_type_http:I = 0x7f1002fd

.field public static final connection_type_tcp:I = 0x7f1002fc

.field public static final contact:I = 0x7f100057

.field public static final contact_fragment_attachments:I = 0x7f10022b

.field public static final contact_fragment_container:I = 0x7f100228

.field public static final contact_fragment_description:I = 0x7f10022a

.field public static final contact_fragment_email:I = 0x7f100229

.field public static final contact_fragment_progress:I = 0x7f10022c

.field public static final contact_spinner_list_item:I = 0x7f100108

.field public static final contact_support:I = 0x7f1001db

.field public static final contact_title:I = 0x7f100127

.field public static final contact_us:I = 0x7f1003e6

.field public static final contact_us_button:I = 0x7f1000d7

.field public static final contact_us_icon:I = 0x7f1003e7

.field public static final contact_us_title:I = 0x7f1003e8

.field public static final container:I = 0x7f1000c6

.field public static final contentPanel:I = 0x7f100083

.field public static final context_menu_screen:I = 0x7f100230

.field public static final continue_anyway:I = 0x7f1001f5

.field public static final copyright_label:I = 0x7f100225

.field public static final crop_image:I = 0x7f10010a

.field public static final curr_speed_limit:I = 0x7f1000d1

.field public static final curr_speed_nav:I = 0x7f1000d0

.field public static final current_device:I = 0x7f100227

.field public static final current_road:I = 0x7f1000cf

.field public static final custom:I = 0x7f10008a

.field public static final customPanel:I = 0x7f100089

.field public static final custom_dialog:I = 0x7f1001d7

.field public static final custom_dialog_desc:I = 0x7f1001d9

.field public static final custom_dialog_title:I = 0x7f1001d8

.field public static final dark:I = 0x7f100072

.field public static final dash:I = 0x7f1001ea

.field public static final dash_screen:I = 0x7f10022e

.field public static final date:I = 0x7f100064

.field public static final decor_content_parent:I = 0x7f100097

.field public static final default_activity_button:I = 0x7f10007c

.field public static final demote_common_words:I = 0x7f100069

.field public static final demote_rfc822_hostnames:I = 0x7f10006a

.field public static final desc:I = 0x7f10012f

.field public static final desc1:I = 0x7f1001a2

.field public static final desc2:I = 0x7f1001a5

.field public static final desc2b:I = 0x7f1001a9

.field public static final desc3:I = 0x7f1001ac

.field public static final desc4:I = 0x7f1001af

.field public static final desc5:I = 0x7f1001b2

.field public static final desc7:I = 0x7f1001b5

.field public static final desc8:I = 0x7f1001b8

.field public static final desc9:I = 0x7f1001bb

.field public static final description:I = 0x7f1003d2

.field public static final design_bottom_sheet:I = 0x7f100114

.field public static final design_menu_item_action_area:I = 0x7f10011b

.field public static final design_menu_item_action_area_stub:I = 0x7f10011a

.field public static final design_menu_item_text:I = 0x7f100119

.field public static final design_navigation_view:I = 0x7f100118

.field public static final details_bottom_sheet:I = 0x7f10011d

.field public static final details_place_title:I = 0x7f100123

.field public static final device_main_title:I = 0x7f10015b

.field public static final device_name:I = 0x7f100141

.field public static final device_picker_button_add_new_device:I = 0x7f100235

.field public static final device_picker_edittext_add_new_device:I = 0x7f100234

.field public static final device_picker_textview_current_device:I = 0x7f100233

.field public static final dial_click:I = 0x7f100248

.field public static final dial_left:I = 0x7f100246

.field public static final dial_long_click:I = 0x7f100247

.field public static final dial_long_press:I = 0x7f1003b4

.field public static final dial_long_press_desc:I = 0x7f1003b8

.field public static final dial_long_press_google_now:I = 0x7f1003b6

.field public static final dial_long_press_group:I = 0x7f1003b5

.field public static final dial_long_press_place_search:I = 0x7f1003b7

.field public static final dial_right:I = 0x7f100249

.field public static final dialog:I = 0x7f100143

.field public static final dialog_bullet_list:I = 0x7f10015c

.field public static final dialog_detail:I = 0x7f10013a

.field public static final dialog_spacer:I = 0x7f100140

.field public static final dialog_spacer_2:I = 0x7f100160

.field public static final dialog_title:I = 0x7f100139

.field public static final disableHome:I = 0x7f100020

.field public static final display_info:I = 0x7f100220

.field public static final display_ui_scaling:I = 0x7f100391

.field public static final display_ui_scaling_group:I = 0x7f100392

.field public static final dist_to_next_maneuver:I = 0x7f1000ca

.field public static final divider_four:I = 0x7f1003da

.field public static final divider_one:I = 0x7f100396

.field public static final divider_three:I = 0x7f1003a0

.field public static final divider_two:I = 0x7f100398

.field public static final done:I = 0x7f100215

.field public static final done_button:I = 0x7f100145

.field public static final done_cancel_bar:I = 0x7f100109

.field public static final download_status:I = 0x7f1000da

.field public static final download_version:I = 0x7f1003cc

.field public static final drawer_layout:I = 0x7f1002b2

.field public static final drive_name_input:I = 0x7f100336

.field public static final drop_pin:I = 0x7f100149

.field public static final duration:I = 0x7f100159

.field public static final duration_card:I = 0x7f100136

.field public static final edit:I = 0x7f100152

.field public static final edit_favorites_button:I = 0x7f100122

.field public static final edit_query:I = 0x7f10009b

.field public static final email:I = 0x7f100058

.field public static final enable_button:I = 0x7f10015e

.field public static final enable_gesture:I = 0x7f10025b

.field public static final enable_preview:I = 0x7f10025c

.field public static final enable_video_loop:I = 0x7f10025d

.field public static final end:I = 0x7f100037

.field public static final end_padder:I = 0x7f100329

.field public static final end_trip:I = 0x7f1000b2

.field public static final enterAlways:I = 0x7f100026

.field public static final enterAlwaysCollapsed:I = 0x7f100027

.field public static final enter_email:I = 0x7f100171

.field public static final enter_email_address:I = 0x7f100395

.field public static final enter_email_til:I = 0x7f100170

.field public static final enter_make:I = 0x7f100209

.field public static final enter_make_til:I = 0x7f100208

.field public static final enter_model:I = 0x7f10020b

.field public static final enter_model_til:I = 0x7f10020a

.field public static final enter_name:I = 0x7f10016f

.field public static final enter_name_til:I = 0x7f10016e

.field public static final enter_year:I = 0x7f100207

.field public static final enter_year_til:I = 0x7f100206

.field public static final estimated_time_arrival:I = 0x7f1000d2

.field public static final eta_dist_n_arriv_container:I = 0x7f1000ba

.field public static final exitUntilCollapsed:I = 0x7f100028

.field public static final expand_activities_button:I = 0x7f10007a

.field public static final expanded_menu:I = 0x7f10008f

.field public static final fab:I = 0x7f1000e4

.field public static final fabs:I = 0x7f10035a

.field public static final favorite_fabs_film_and_labels:I = 0x7f1000e2

.field public static final favorites_fragment_recycler_view:I = 0x7f1002bb

.field public static final features_recycler_view:I = 0x7f1003a3

.field public static final ferries:I = 0x7f1003c5

.field public static final file_transfer_title:I = 0x7f100236

.field public static final fill:I = 0x7f100041

.field public static final fill_horizontal:I = 0x7f100042

.field public static final fill_vertical:I = 0x7f100038

.field public static final film:I = 0x7f1000e3

.field public static final fist:I = 0x7f100243

.field public static final fist_offscreen:I = 0x7f100244

.field public static final fist_with_index:I = 0x7f100242

.field public static final fixed:I = 0x7f100074

.field public static final flashlight:I = 0x7f100216

.field public static final fluctuator:I = 0x7f100340

.field public static final footer_description:I = 0x7f1003f0

.field public static final footer_icon:I = 0x7f1003ee

.field public static final footer_title:I = 0x7f1003ef

.field public static final fragment_contact_zendesk_attachment:I = 0x7f100416

.field public static final fragment_contact_zendesk_menu_done:I = 0x7f100417

.field public static final fragment_container:I = 0x7f1000d5

.field public static final fragment_help_menu_contact:I = 0x7f100419

.field public static final fragment_help_menu_search:I = 0x7f100418

.field public static final fragment_homescreen_textiew:I = 0x7f1002ba

.field public static final gauge:I = 0x7f100341

.field public static final gestures_switch_desc:I = 0x7f1003a8

.field public static final glances_switch_desc:I = 0x7f1002bd

.field public static final go_to_bluetooth_settings_button:I = 0x7f10015f

.field public static final go_to_location_settings_button:I = 0x7f100142

.field public static final google_bottom_padding:I = 0x7f1002ec

.field public static final green_arrow:I = 0x7f1000eb

.field public static final guided_tour:I = 0x7f1003e3

.field public static final guided_tour_icon:I = 0x7f1003e4

.field public static final guided_tour_title:I = 0x7f1003e5

.field public static final hand:I = 0x7f1001ed

.field public static final header:I = 0x7f1003ba

.field public static final header_bg:I = 0x7f1000fe

.field public static final header_container:I = 0x7f10035c

.field public static final help:I = 0x7f100217

.field public static final help_center:I = 0x7f1003dd

.field public static final help_center_icon:I = 0x7f1003de

.field public static final help_center_title:I = 0x7f1003df

.field public static final help_section_action_button:I = 0x7f100342

.field public static final help_section_loading_progress:I = 0x7f100343

.field public static final here_bottom_padding:I = 0x7f1002ea

.field public static final highlight:I = 0x7f100107

.field public static final highways:I = 0x7f1003c3

.field public static final home:I = 0x7f100004

.field public static final homeAsUp:I = 0x7f100021

.field public static final home_fragment_frame:I = 0x7f1002e9

.field public static final home_fragment_google_map_fragment:I = 0x7f1002ed

.field public static final home_fragment_here_map:I = 0x7f1002eb

.field public static final home_fragment_recycler_view:I = 0x7f1002ee

.field public static final homescreen_relative_layout:I = 0x7f1002b6

.field public static final homescreen_toolbar:I = 0x7f1002b4

.field public static final horizontal:I = 0x7f100053

.field public static final html:I = 0x7f100065

.field public static final hud:I = 0x7f100168

.field public static final hud_ui_view_pager:I = 0x7f1001eb

.field public static final hud_version:I = 0x7f1003d8

.field public static final hud_voice_search_label:I = 0x7f100252

.field public static final hybrid:I = 0x7f10005e

.field public static final hybrid_map_screen:I = 0x7f10022f

.field public static final i_have_medium:I = 0x7f100218

.field public static final i_have_medium_image:I = 0x7f10021b

.field public static final i_have_medium_text:I = 0x7f10021a

.field public static final icon:I = 0x7f10007e

.field public static final icon_car:I = 0x7f1001c4

.field public static final icon_gesture:I = 0x7f1003a5

.field public static final icon_group:I = 0x7f100326

.field public static final icon_install_complete_line1:I = 0x7f10019b

.field public static final icon_install_complete_line2:I = 0x7f10019c

.field public static final icon_install_complete_line3:I = 0x7f10019d

.field public static final icon_install_complete_line4:I = 0x7f10019e

.field public static final icon_navdy:I = 0x7f1001c3

.field public static final icon_only:I = 0x7f10006f

.field public static final icon_phone:I = 0x7f1001c5

.field public static final icon_uri:I = 0x7f100049

.field public static final ifRoom:I = 0x7f100062

.field public static final illustration:I = 0x7f1000b1

.field public static final illustration2:I = 0x7f1001e1

.field public static final illustration_distance_container:I = 0x7f1002f3

.field public static final illustration_layout:I = 0x7f100156

.field public static final image:I = 0x7f10007b

.field public static final img1:I = 0x7f1001a0

.field public static final img2:I = 0x7f1001a3

.field public static final img2b:I = 0x7f1001a7

.field public static final img3:I = 0x7f1001aa

.field public static final img4:I = 0x7f1001ad

.field public static final img5:I = 0x7f1001b0

.field public static final img7:I = 0x7f1001b3

.field public static final img8:I = 0x7f1001b6

.field public static final img9:I = 0x7f1001b9

.field public static final include:I = 0x7f10020d

.field public static final index_entity_types:I = 0x7f10006b

.field public static final info:I = 0x7f100323

.field public static final input:I = 0x7f100144

.field public static final input_email:I = 0x7f100298

.field public static final input_message:I = 0x7f10029a

.field public static final input_name:I = 0x7f100297

.field public static final input_password:I = 0x7f1002a5

.field public static final input_subject:I = 0x7f100299

.field public static final inputs:I = 0x7f10014b

.field public static final install_video:I = 0x7f1001e7

.field public static final installation_video:I = 0x7f1003e0

.field public static final installation_video_icon:I = 0x7f1003e1

.field public static final installation_video_title:I = 0x7f1003e2

.field public static final instant_message:I = 0x7f100059

.field public static final instruction_card:I = 0x7f1001bc

.field public static final instruction_card2:I = 0x7f1001e2

.field public static final intent_action:I = 0x7f10004a

.field public static final intent_activity:I = 0x7f10004b

.field public static final intent_data:I = 0x7f10004c

.field public static final intent_data_id:I = 0x7f10004d

.field public static final intent_extra_data:I = 0x7f10004e

.field public static final item_touch_helper_previous_elevation:I = 0x7f100005

.field public static final key:I = 0x7f1002f9

.field public static final label_author:I = 0x7f1002ac

.field public static final label_date:I = 0x7f1002ad

.field public static final label_input:I = 0x7f10014d

.field public static final label_label:I = 0x7f10014c

.field public static final label_last_updated:I = 0x7f10029f

.field public static final label_message:I = 0x7f100294

.field public static final label_text:I = 0x7f10014e

.field public static final label_title:I = 0x7f1002a8

.field public static final label_version:I = 0x7f1002a9

.field public static final lane_info:I = 0x7f1000cb

.field public static final largeLabel:I = 0x7f100112

.field public static final large_icon_uri:I = 0x7f10004f

.field public static final ledBrightness:I = 0x7f10025a

.field public static final ledBrightnessLabel:I = 0x7f100258

.field public static final ledBrightnessTextView:I = 0x7f100259

.field public static final left:I = 0x7f100039

.field public static final left_bottom_padding:I = 0x7f100190

.field public static final left_icon:I = 0x7f1002af

.field public static final left_title:I = 0x7f10035d

.field public static final lens_ok:I = 0x7f1001cd

.field public static final lens_ok_dot:I = 0x7f1001ce

.field public static final lens_ok_img:I = 0x7f1001d0

.field public static final lens_ok_title:I = 0x7f1001d1

.field public static final lens_too_high:I = 0x7f1001c8

.field public static final lens_too_high_dot:I = 0x7f1001c9

.field public static final lens_too_high_img:I = 0x7f1001cb

.field public static final lens_too_high_title:I = 0x7f1001cc

.field public static final lens_too_low:I = 0x7f1001d2

.field public static final lens_too_low_dot:I = 0x7f1001d3

.field public static final lens_too_low_img:I = 0x7f1001d5

.field public static final lens_too_low_title:I = 0x7f1001d6

.field public static final light:I = 0x7f100073

.field public static final limit_cell_data_switch:I = 0x7f1003ad

.field public static final limit_cell_data_title:I = 0x7f1003ae

.field public static final line:I = 0x7f100048

.field public static final line1:I = 0x7f100327

.field public static final line3:I = 0x7f100328

.field public static final linear_layout:I = 0x7f10019a

.field public static final list:I = 0x7f10027d

.field public static final listMode:I = 0x7f10001d

.field public static final listView:I = 0x7f10022d

.field public static final list_attachments:I = 0x7f1002ae

.field public static final list_feedback_messages:I = 0x7f1002a3

.field public static final list_item:I = 0x7f10007d

.field public static final list_top:I = 0x7f100291

.field public static final listingRoutesLabel:I = 0x7f10027c

.field public static final loadSavedRoutes:I = 0x7f100276

.field public static final loading_view:I = 0x7f1000d6

.field public static final local_nav_navigate:I = 0x7f10041b

.field public static final local_nav_route_info:I = 0x7f10041a

.field public static final log_output:I = 0x7f100110

.field public static final log_scrollview:I = 0x7f10010f

.field public static final maneuver_detail_list:I = 0x7f1000cc

.field public static final maneuver_info:I = 0x7f1000c7

.field public static final maneuver_row_action:I = 0x7f1002ff

.field public static final maneuver_row_angle:I = 0x7f100302

.field public static final maneuver_row_cur_street_name:I = 0x7f10030a

.field public static final maneuver_row_cur_street_number:I = 0x7f100309

.field public static final maneuver_row_distance_from_prev:I = 0x7f100306

.field public static final maneuver_row_distance_from_start:I = 0x7f100305

.field public static final maneuver_row_distance_to_next:I = 0x7f100307

.field public static final maneuver_row_icon:I = 0x7f100300

.field public static final maneuver_row_instruction:I = 0x7f100308

.field public static final maneuver_row_next_street_image:I = 0x7f10030d

.field public static final maneuver_row_next_street_name:I = 0x7f10030c

.field public static final maneuver_row_next_street_number:I = 0x7f10030b

.field public static final maneuver_row_road_elements:I = 0x7f100312

.field public static final maneuver_row_signpost_icon:I = 0x7f100310

.field public static final maneuver_row_signpost_label_direction:I = 0x7f100311

.field public static final maneuver_row_signpost_number:I = 0x7f10030e

.field public static final maneuver_row_signpost_text:I = 0x7f10030f

.field public static final maneuver_row_start_time:I = 0x7f100313

.field public static final maneuver_row_traffic_direction:I = 0x7f100303

.field public static final maneuver_row_transport_mode:I = 0x7f100304

.field public static final maneuver_row_turn:I = 0x7f100301

.field public static final manual_entry:I = 0x7f100204

.field public static final map:I = 0x7f100147

.field public static final mapTilt:I = 0x7f100261

.field public static final mapZoomLevel:I = 0x7f10025f

.field public static final map_container:I = 0x7f100250

.field public static final map_fragment:I = 0x7f1000ad

.field public static final map_relative_layout:I = 0x7f1000ac

.field public static final map_touch_listener:I = 0x7f10014a

.field public static final map_view:I = 0x7f100314

.field public static final masked:I = 0x7f100401

.field public static final master_layout:I = 0x7f100394

.field public static final match_global_nicknames:I = 0x7f10006c

.field public static final media_actions:I = 0x7f10031c

.field public static final medium_mount:I = 0x7f1001f9

.field public static final medium_mount_description:I = 0x7f100200

.field public static final medium_mount_illustration:I = 0x7f1001fe

.field public static final medium_mount_pill:I = 0x7f100201

.field public static final medium_mount_title:I = 0x7f1001ff

.field public static final medium_tall_mount:I = 0x7f1001a6

.field public static final medium_worked:I = 0x7f1001de

.field public static final menu_app_version:I = 0x7f1003f3

.field public static final menu_audio:I = 0x7f10040b

.field public static final menu_calendar:I = 0x7f10040c

.field public static final menu_car:I = 0x7f10040a

.field public static final menu_debug:I = 0x7f100413

.field public static final menu_delete:I = 0x7f100421

.field public static final menu_display_version:I = 0x7f1003f4

.field public static final menu_edit:I = 0x7f100420

.field public static final menu_general:I = 0x7f100410

.field public static final menu_hud_version:I = 0x7f100415

.field public static final menu_legal:I = 0x7f100412

.field public static final menu_messaging:I = 0x7f10040e

.field public static final menu_navigation:I = 0x7f10040d

.field public static final menu_save:I = 0x7f10041e

.field public static final menu_screen:I = 0x7f100231

.field public static final menu_serial:I = 0x7f1003f2

.field public static final menu_serial_version:I = 0x7f1003f5

.field public static final menu_software_update:I = 0x7f10040f

.field public static final menu_spacer:I = 0x7f100414

.field public static final menu_stop:I = 0x7f10041f

.field public static final menu_support:I = 0x7f100411

.field public static final menu_version:I = 0x7f1003f1

.field public static final middle:I = 0x7f10005b

.field public static final mini:I = 0x7f100046

.field public static final more:I = 0x7f1003bb

.field public static final more_or_less:I = 0x7f1003d3

.field public static final more_routes_relative_layout:I = 0x7f1000a8

.field public static final mount_illustration_view_pager:I = 0x7f1001c7

.field public static final multiply:I = 0x7f10002c

.field public static final my_toolbar:I = 0x7f1000a9

.field public static final nav_autocomplete_places:I = 0x7f10024f

.field public static final nav_button:I = 0x7f1002f5

.field public static final nav_picker_button_search:I = 0x7f100268

.field public static final nav_picker_edittext_destination:I = 0x7f100267

.field public static final nav_picker_edittext_destination_demo:I = 0x7f10026c

.field public static final nav_view:I = 0x7f1002b3

.field public static final navdy_apps:I = 0x7f1002d3

.field public static final navdy_apps_title:I = 0x7f1002d2

.field public static final navdy_display:I = 0x7f1002b5

.field public static final navdy_display_title:I = 0x7f1003d7

.field public static final navdy_features:I = 0x7f1002c9

.field public static final navdy_features_title:I = 0x7f1002c8

.field public static final navdy_logo:I = 0x7f1001ef

.field public static final navigation_button_pause:I = 0x7f100270

.field public static final navigation_button_resume:I = 0x7f100271

.field public static final navigation_button_simulate:I = 0x7f10026f

.field public static final navigation_button_start:I = 0x7f10026e

.field public static final navigation_button_stop:I = 0x7f100272

.field public static final navigation_destination_label:I = 0x7f10026d

.field public static final navigation_fragment:I = 0x7f1000b9

.field public static final navigation_header_container:I = 0x7f100117

.field public static final navigation_info:I = 0x7f1000ce

.field public static final navigation_start:I = 0x7f100406

.field public static final navigation_stop:I = 0x7f100408

.field public static final need_to_review:I = 0x7f10019f

.field public static final never:I = 0x7f100045

.field public static final new_box:I = 0x7f100172

.field public static final new_box_cheveron:I = 0x7f100174

.field public static final new_box_description:I = 0x7f100176

.field public static final new_box_illustration:I = 0x7f100173

.field public static final new_box_plus_mounts:I = 0x7f100177

.field public static final new_box_plus_mounts_cheveron:I = 0x7f100179

.field public static final new_box_plus_mounts_description:I = 0x7f10017b

.field public static final new_box_plus_mounts_illustration:I = 0x7f100178

.field public static final new_box_plus_mounts_title:I = 0x7f10017a

.field public static final new_box_title:I = 0x7f100175

.field public static final next:I = 0x7f100183

.field public static final next_image:I = 0x7f100196

.field public static final next_maneuver_turn:I = 0x7f1000c9

.field public static final next_road_name:I = 0x7f1000c8

.field public static final next_step:I = 0x7f10016d

.field public static final next_text:I = 0x7f100195

.field public static final none:I = 0x7f10001c

.field public static final normal:I = 0x7f10001e

.field public static final north_up_button:I = 0x7f1000af

.field public static final notification_background:I = 0x7f100324

.field public static final notification_glances:I = 0x7f1002e6

.field public static final notification_glances_icon:I = 0x7f1002e7

.field public static final notification_main_column:I = 0x7f10031f

.field public static final notification_main_column_container:I = 0x7f10031e

.field public static final obd2:I = 0x7f100187

.field public static final obd2_description:I = 0x7f10018b

.field public static final obd2_illustration:I = 0x7f100189

.field public static final obd2_title:I = 0x7f10018a

.field public static final obd_crime_photo:I = 0x7f100213

.field public static final obd_location:I = 0x7f100193

.field public static final obd_locator:I = 0x7f100388

.field public static final obd_locator_description:I = 0x7f100194

.field public static final obd_locator_title:I = 0x7f100205

.field public static final obd_on:I = 0x7f10038d

.field public static final obd_setting_list_item_description:I = 0x7f10038f

.field public static final obd_title:I = 0x7f10038e

.field public static final obd_watch_video:I = 0x7f100212

.field public static final offline_banner:I = 0x7f1000aa

.field public static final ok:I = 0x7f100385

.field public static final ok_button:I = 0x7f10013c

.field public static final old_box:I = 0x7f10017c

.field public static final old_box_cheveron:I = 0x7f10017e

.field public static final old_box_description:I = 0x7f100180

.field public static final old_box_illustration:I = 0x7f10017d

.field public static final old_box_title:I = 0x7f10017f

.field public static final omnibox_title_section:I = 0x7f10006d

.field public static final omnibox_url_section:I = 0x7f10006e

.field public static final ota_container:I = 0x7f1003c9

.field public static final other_glances:I = 0x7f1002e8

.field public static final packed:I = 0x7f100017

.field public static final padding:I = 0x7f100148

.field public static final pagination:I = 0x7f1001ec

.field public static final pair_bluetooth_text:I = 0x7f100167

.field public static final parallax:I = 0x7f10003d

.field public static final parent:I = 0x7f100014

.field public static final parentPanel:I = 0x7f100082

.field public static final pause:I = 0x7f100266

.field public static final pending_trip_card:I = 0x7f10032a

.field public static final permission_screen:I = 0x7f100386

.field public static final photo:I = 0x7f100169

.field public static final photo_hint:I = 0x7f10016a

.field public static final photo_hint_icon:I = 0x7f10032b

.field public static final photo_hint_text:I = 0x7f10032c

.field public static final pick_a_make:I = 0x7f100210

.field public static final pick_a_model:I = 0x7f100211

.field public static final pick_a_year:I = 0x7f10020f

.field public static final pin:I = 0x7f10003e

.field public static final place:I = 0x7f100120

.field public static final place_autocomplete_clear_button:I = 0x7f10032f

.field public static final place_autocomplete_powered_by_google:I = 0x7f100331

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f100333

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f100334

.field public static final place_autocomplete_progress:I = 0x7f100332

.field public static final place_autocomplete_search_button:I = 0x7f10032d

.field public static final place_autocomplete_search_input:I = 0x7f10032e

.field public static final place_autocomplete_separator:I = 0x7f100330

.field public static final place_details_address:I = 0x7f100124

.field public static final place_details_contact_first_line:I = 0x7f100128

.field public static final place_details_contact_second_line:I = 0x7f100129

.field public static final place_details_cost:I = 0x7f10012d

.field public static final place_details_distance_first_line:I = 0x7f100125

.field public static final place_details_hours_first_line:I = 0x7f10012a

.field public static final place_details_hours_second_line:I = 0x7f10012b

.field public static final place_details_map_view:I = 0x7f10011e

.field public static final place_details_place_type:I = 0x7f10012c

.field public static final place_title:I = 0x7f10033a

.field public static final plain:I = 0x7f100066

.field public static final play:I = 0x7f100265

.field public static final play_sequence:I = 0x7f100293

.field public static final preference_speech_delay:I = 0x7f10037b

.field public static final preference_voice:I = 0x7f100378

.field public static final prev:I = 0x7f100264

.field public static final primary_button:I = 0x7f10013f

.field public static final problem_input:I = 0x7f1003a1

.field public static final problem_types:I = 0x7f100397

.field public static final profile_card:I = 0x7f1003db

.field public static final progress:I = 0x7f1000f9

.field public static final progress_bar:I = 0x7f100161

.field public static final progress_bar2:I = 0x7f1003ce

.field public static final progress_bar_background_1:I = 0x7f100402

.field public static final progress_bar_background_2:I = 0x7f100404

.field public static final progress_bar_progress_1:I = 0x7f100403

.field public static final progress_bar_progress_2:I = 0x7f100405

.field public static final progress_circular:I = 0x7f100006

.field public static final progress_container:I = 0x7f10023c

.field public static final progress_data:I = 0x7f1003d0

.field public static final progress_horizontal:I = 0x7f100007

.field public static final progress_label:I = 0x7f1000fa

.field public static final progress_percentage:I = 0x7f1003cf

.field public static final project_spinner:I = 0x7f1003ea

.field public static final radio:I = 0x7f100092

.field public static final radioaddresses:I = 0x7f10026a

.field public static final radioplaces:I = 0x7f10026b

.field public static final read_aloud:I = 0x7f1002c1

.field public static final read_aloud_and_show_content:I = 0x7f1002c7

.field public static final read_aloud_and_show_content_icon:I = 0x7f1002c6

.field public static final read_aloud_and_show_content_layout:I = 0x7f1002c5

.field public static final read_aloud_icon:I = 0x7f1002c0

.field public static final read_aloud_layout:I = 0x7f1002bf

.field public static final recyclerView:I = 0x7f100251

.field public static final recycler_view:I = 0x7f100315

.field public static final red_arrow:I = 0x7f1000f1

.field public static final refresh:I = 0x7f10027e

.field public static final relative_layout_report_a_problem_default:I = 0x7f1003dc

.field public static final release_notes_title:I = 0x7f1003d1

.field public static final replies_list:I = 0x7f1003b9

.field public static final request_list_fragment_progress:I = 0x7f100273

.field public static final resetMapZoom:I = 0x7f100262

.field public static final resetRoute:I = 0x7f100279

.field public static final reset_scale:I = 0x7f100281

.field public static final retry_pairing_button:I = 0x7f10015d

.field public static final retry_view_button:I = 0x7f1002f2

.field public static final retry_view_container:I = 0x7f1002f0

.field public static final retry_view_text:I = 0x7f1002f1

.field public static final rfc822:I = 0x7f100067

.field public static final right:I = 0x7f10003a

.field public static final right_bottom_padding:I = 0x7f100191

.field public static final right_button:I = 0x7f10036e

.field public static final right_chevron:I = 0x7f1002f7

.field public static final right_icon:I = 0x7f1002b0

.field public static final right_side:I = 0x7f100320

.field public static final right_side_image:I = 0x7f10035e

.field public static final rma_dialog_root:I = 0x7f100008

.field public static final rma_dont_ask_again:I = 0x7f100009

.field public static final rma_feedback_button:I = 0x7f10000a

.field public static final rma_feedback_issue_cancel_button:I = 0x7f100284

.field public static final rma_feedback_issue_edittext:I = 0x7f100282

.field public static final rma_feedback_issue_progress:I = 0x7f100283

.field public static final rma_feedback_issue_send_button:I = 0x7f100285

.field public static final rma_store_button:I = 0x7f10000b

.field public static final root_view:I = 0x7f1000d8

.field public static final route_calculation_fastest:I = 0x7f1003be

.field public static final route_calculation_shortest:I = 0x7f1003bf

.field public static final route_name:I = 0x7f10033d

.field public static final route_preferences:I = 0x7f1003c2

.field public static final route_title:I = 0x7f10033c

.field public static final route_traffic_level:I = 0x7f10033e

.field public static final routing:I = 0x7f1003bc

.field public static final routing_group:I = 0x7f1003bd

.field public static final row_request_container:I = 0x7f100353

.field public static final row_request_date:I = 0x7f100355

.field public static final row_request_description:I = 0x7f100354

.field public static final row_request_unread_indicator:I = 0x7f100352

.field public static final satellite:I = 0x7f10005f

.field public static final saveSelected:I = 0x7f10041d

.field public static final screen:I = 0x7f10002d

.field public static final screen_center:I = 0x7f10020e

.field public static final scroll:I = 0x7f100029

.field public static final scrollIndicatorDown:I = 0x7f100088

.field public static final scrollIndicatorUp:I = 0x7f100084

.field public static final scrollView:I = 0x7f100085

.field public static final scroll_view:I = 0x7f100199

.field public static final scrollable:I = 0x7f100075

.field public static final search:I = 0x7f1002b1

.field public static final search_badge:I = 0x7f10009d

.field public static final search_bar:I = 0x7f10009c

.field public static final search_button:I = 0x7f10009e

.field public static final search_close_btn:I = 0x7f1000a3

.field public static final search_edit_frame:I = 0x7f10009f

.field public static final search_edit_text:I = 0x7f10036f

.field public static final search_go_btn:I = 0x7f1000a5

.field public static final search_mag_icon:I = 0x7f1000a0

.field public static final search_plate:I = 0x7f1000a1

.field public static final search_recycler_view:I = 0x7f100359

.field public static final search_row:I = 0x7f100360

.field public static final search_row_details:I = 0x7f100364

.field public static final search_row_distance:I = 0x7f100365

.field public static final search_row_image:I = 0x7f100361

.field public static final search_row_price:I = 0x7f100366

.field public static final search_row_title:I = 0x7f100363

.field public static final search_services:I = 0x7f100367

.field public static final search_src_text:I = 0x7f1000a2

.field public static final search_toolbar:I = 0x7f100358

.field public static final search_type_radio_group:I = 0x7f100269

.field public static final search_voice_btn:I = 0x7f1000a6

.field public static final secondary_button:I = 0x7f10013e

.field public static final section_group:I = 0x7f100356

.field public static final section_title:I = 0x7f100357

.field public static final select_dialog_listview:I = 0x7f1000a7

.field public static final select_mount_to_continue:I = 0x7f1001f8

.field public static final selectedRoute:I = 0x7f10027b

.field public static final separator:I = 0x7f1002f8

.field public static final sequence_delay_choice_layout:I = 0x7f100292

.field public static final serial_label:I = 0x7f100221

.field public static final serial_no:I = 0x7f100222

.field public static final service_atm_icon:I = 0x7f10036a

.field public static final service_food_icon:I = 0x7f100369

.field public static final service_gas_icon:I = 0x7f100368

.field public static final service_parking_icon:I = 0x7f10036b

.field public static final service_separator:I = 0x7f10036c

.field public static final setDestination:I = 0x7f100278

.field public static final setMockedLocation:I = 0x7f100277

.field public static final setting_list_item_description:I = 0x7f10038b

.field public static final settings:I = 0x7f100384

.field public static final settings_container:I = 0x7f1003cb

.field public static final settings_general_limit_cell_data_desc:I = 0x7f1003af

.field public static final settings_general_smart_suggestions_desc:I = 0x7f1003ac

.field public static final short_mount:I = 0x7f1001da

.field public static final short_mount_card:I = 0x7f1001e6

.field public static final short_mount_description:I = 0x7f1001fc

.field public static final short_mount_illustration:I = 0x7f1001fa

.field public static final short_mount_pill:I = 0x7f1001fd

.field public static final short_mount_title:I = 0x7f1001fb

.field public static final short_worked:I = 0x7f1001dd

.field public static final shortcut:I = 0x7f100091

.field public static final showCustom:I = 0x7f100022

.field public static final showHome:I = 0x7f100023

.field public static final showTitle:I = 0x7f100024

.field public static final show_content:I = 0x7f1002c4

.field public static final show_content_icon:I = 0x7f1002c3

.field public static final show_content_layout:I = 0x7f1002c2

.field public static final sim_start:I = 0x7f100407

.field public static final simulateSelected:I = 0x7f10041c

.field public static final simulationStatus:I = 0x7f10027a

.field public static final size:I = 0x7f1002fa

.field public static final slide_down:I = 0x7f10027f

.field public static final slide_up:I = 0x7f100280

.field public static final smallLabel:I = 0x7f100111

.field public static final smart_bluetooth:I = 0x7f100373

.field public static final smart_bluetooth_container:I = 0x7f100372

.field public static final smart_suggestions_switch:I = 0x7f1003aa

.field public static final smart_suggestions_title:I = 0x7f1003ab

.field public static final snackbar_action:I = 0x7f100116

.field public static final snackbar_text:I = 0x7f100115

.field public static final snap:I = 0x7f10002a

.field public static final spacer:I = 0x7f100081

.field public static final speaker:I = 0x7f100375

.field public static final speech_label:I = 0x7f10037c

.field public static final speech_level:I = 0x7f10037d

.field public static final speech_settings:I = 0x7f100376

.field public static final speed_warnings:I = 0x7f100381

.field public static final splash:I = 0x7f1002b9

.field public static final splash_connect:I = 0x7f100286

.field public static final splash_page:I = 0x7f1002fe

.field public static final split_action_bar:I = 0x7f10000c

.field public static final spread:I = 0x7f100015

.field public static final spread_inside:I = 0x7f100018

.field public static final src_atop:I = 0x7f10002e

.field public static final src_in:I = 0x7f10002f

.field public static final src_over:I = 0x7f100030

.field public static final standard:I = 0x7f100070

.field public static final start:I = 0x7f10003b

.field public static final startSimulation:I = 0x7f100274

.field public static final start_install:I = 0x7f1001f1

.field public static final status_bar_latest_event_content:I = 0x7f10031b

.field public static final status_bar_spacer:I = 0x7f1000ff

.field public static final status_image:I = 0x7f100133

.field public static final stopSimutaion:I = 0x7f100275

.field public static final strut:I = 0x7f1003cd

.field public static final submenuarrow:I = 0x7f100093

.field public static final submit_area:I = 0x7f1000a4

.field public static final subtitle:I = 0x7f100181

.field public static final subtitle2:I = 0x7f1003ca

.field public static final subtitle_group:I = 0x7f100192

.field public static final subtitles:I = 0x7f1001e8

.field public static final summary:I = 0x7f100383

.field public static final sweetch:I = 0x7f1003e9

.field public static final swipe_container:I = 0x7f100163

.field public static final swipe_left:I = 0x7f10023f

.field public static final swipe_right:I = 0x7f100241

.field public static final tabMode:I = 0x7f10001f

.field public static final tab_layout:I = 0x7f1002b7

.field public static final tab_line:I = 0x7f100162

.field public static final tabs:I = 0x7f100202

.field public static final tabs_cover:I = 0x7f1000e1

.field public static final tall_or_medium_mount_card:I = 0x7f1001e5

.field public static final tall_worked:I = 0x7f1001df

.field public static final tap_to_select_problem_input:I = 0x7f1003a2

.field public static final terrain:I = 0x7f100060

.field public static final test_glance:I = 0x7f1002be

.field public static final test_textview_current_sim_speed:I = 0x7f100232

.field public static final text:I = 0x7f10000d

.field public static final text1:I = 0x7f100050

.field public static final text2:I = 0x7f100051

.field public static final textSpacerNoButtons:I = 0x7f100087

.field public static final textSpacerNoTitle:I = 0x7f100086

.field public static final textView:I = 0x7f100245

.field public static final textView1:I = 0x7f100335

.field public static final textView2:I = 0x7f10024a

.field public static final textView3:I = 0x7f100337

.field public static final textView4:I = 0x7f100338

.field public static final text_headline:I = 0x7f1002a4

.field public static final text_input_password_toggle:I = 0x7f10011c

.field public static final text_section:I = 0x7f100362

.field public static final text_view_pager:I = 0x7f1001ee

.field public static final textinput_counter:I = 0x7f10000e

.field public static final textinput_error:I = 0x7f10000f

.field public static final thing_proto:I = 0x7f100052

.field public static final tiltLevelTextView:I = 0x7f100260

.field public static final time:I = 0x7f100321

.field public static final title:I = 0x7f10007f

.field public static final title1:I = 0x7f1001a1

.field public static final title2:I = 0x7f1001a4

.field public static final title2b:I = 0x7f1001a8

.field public static final title3:I = 0x7f1001ab

.field public static final title4:I = 0x7f1001ae

.field public static final title5:I = 0x7f1001b1

.field public static final title7:I = 0x7f1001b4

.field public static final title8:I = 0x7f1001b7

.field public static final title9:I = 0x7f1001ba

.field public static final titleDividerNoCustom:I = 0x7f10008e

.field public static final title_template:I = 0x7f10008c

.field public static final toll_roads:I = 0x7f1003c4

.field public static final toolbar_search_relative_layout:I = 0x7f10036d

.field public static final toolbar_spacer:I = 0x7f100166

.field public static final top:I = 0x7f10003c

.field public static final topPanel:I = 0x7f10008b

.field public static final touchDetection:I = 0x7f10035b

.field public static final touch_outside:I = 0x7f100113

.field public static final transfer_button:I = 0x7f10023b

.field public static final transfer_message:I = 0x7f10023e

.field public static final transfer_progress:I = 0x7f10023d

.field public static final transfer_size100mb:I = 0x7f10023a

.field public static final transfer_size10mb:I = 0x7f100239

.field public static final transfer_size1mb:I = 0x7f100238

.field public static final transfer_size_select:I = 0x7f100237

.field public static final transition_current_scene:I = 0x7f100010

.field public static final transition_scene_layoutid_cache:I = 0x7f100011

.field public static final trip_address_first_line:I = 0x7f1000b4

.field public static final trip_address_second_line:I = 0x7f10033b

.field public static final trip_card_arrival_time:I = 0x7f1000c2

.field public static final trip_card_bottom_sheet:I = 0x7f1000b5

.field public static final trip_card_bottom_sheet_calculating:I = 0x7f1000b6

.field public static final trip_card_bottom_sheet_calculating_text:I = 0x7f1000b7

.field public static final trip_card_bottom_sheet_error:I = 0x7f1000c4

.field public static final trip_card_distance:I = 0x7f1000bf

.field public static final trip_card_eta:I = 0x7f1000bc

.field public static final trip_name:I = 0x7f1000b3

.field public static final trip_overview_arrival_time_relative_layout:I = 0x7f1000c1

.field public static final trip_overview_arrival_title:I = 0x7f1000c3

.field public static final trip_overview_distance_relative_layout:I = 0x7f1000be

.field public static final trip_overview_distance_title:I = 0x7f1000c0

.field public static final trip_overview_estimates:I = 0x7f10033f

.field public static final trip_overview_eta_relative_layout:I = 0x7f1000bb

.field public static final trip_overview_eta_title:I = 0x7f1000bd

.field public static final tunnels:I = 0x7f1003c6

.field public static final turn_by_turn_instructions:I = 0x7f10037f

.field public static final txt_description:I = 0x7f1003ec

.field public static final txt_title:I = 0x7f1003eb

.field public static final txt_voice_name:I = 0x7f10037a

.field public static final unit_system:I = 0x7f1003b0

.field public static final unit_system_group:I = 0x7f1003b1

.field public static final unit_system_imperial:I = 0x7f1003b3

.field public static final unit_system_metric:I = 0x7f1003b2

.field public static final unpaved_roads:I = 0x7f1003c7

.field public static final unstable_version_title:I = 0x7f1003d5

.field public static final up:I = 0x7f100012

.field public static final update_button:I = 0x7f10021f

.field public static final update_status:I = 0x7f10021e

.field public static final url:I = 0x7f100068

.field public static final useLogo:I = 0x7f100025

.field public static final use_special_hud_voice_commands:I = 0x7f100253

.field public static final version_label:I = 0x7f10021c

.field public static final vertical:I = 0x7f100054

.field public static final video_duration:I = 0x7f100137

.field public static final video_view:I = 0x7f1000d9

.field public static final view_article_attachment_list:I = 0x7f1000dd

.field public static final view_article_content_webview:I = 0x7f1000db

.field public static final view_article_progress:I = 0x7f1000dc

.field public static final view_article_toolbar_holder:I = 0x7f1000d3

.field public static final view_header:I = 0x7f1002a7

.field public static final view_offset_helper:I = 0x7f100013

.field public static final view_pager:I = 0x7f10018f

.field public static final view_photo:I = 0x7f100214

.field public static final view_request_agent_avatar_imageview:I = 0x7f100345

.field public static final view_request_agent_comment_date:I = 0x7f100344

.field public static final view_request_agent_name_textview:I = 0x7f100346

.field public static final view_request_agent_response_attachment_container:I = 0x7f100348

.field public static final view_request_agent_response_textview:I = 0x7f100347

.field public static final view_request_attachment_container:I = 0x7f10028b

.field public static final view_request_comment_attachment_bth:I = 0x7f10028f

.field public static final view_request_comment_attachment_container:I = 0x7f100289

.field public static final view_request_comment_container:I = 0x7f10028c

.field public static final view_request_comment_edittext:I = 0x7f10028d

.field public static final view_request_comment_list:I = 0x7f100287

.field public static final view_request_comment_send_bth:I = 0x7f10028e

.field public static final view_request_end_user_avatar_imageview:I = 0x7f10034e

.field public static final view_request_end_user_comment_date:I = 0x7f10034d

.field public static final view_request_end_user_name_textview:I = 0x7f10034f

.field public static final view_request_end_user_response_attachment_container:I = 0x7f100351

.field public static final view_request_end_user_response_textview:I = 0x7f100350

.field public static final view_request_fragment_progress:I = 0x7f100290

.field public static final view_request_separator_1:I = 0x7f10028a

.field public static final view_request_separator_2:I = 0x7f100288

.field public static final viewpager:I = 0x7f1002b8

.field public static final vin:I = 0x7f100224

.field public static final vin_label:I = 0x7f100223

.field public static final visible:I = 0x7f100400

.field public static final voice_label:I = 0x7f100379

.field public static final volume:I = 0x7f100135

.field public static final watch:I = 0x7f1001e0

.field public static final watch_image:I = 0x7f100186

.field public static final watch_text:I = 0x7f100185

.field public static final watched:I = 0x7f100158

.field public static final watched_layout:I = 0x7f100157

.field public static final web_update_details:I = 0x7f1002ab

.field public static final web_view:I = 0x7f1003fc

.field public static final welcome_message:I = 0x7f100380

.field public static final what_to_play:I = 0x7f10037e

.field public static final wide:I = 0x7f100071

.field public static final withText:I = 0x7f100063

.field public static final wrap:I = 0x7f100016

.field public static final wrap_content:I = 0x7f100031

.field public static final wrapper_attachments:I = 0x7f10029b

.field public static final wrapper_feedback:I = 0x7f100296

.field public static final wrapper_feedback_scroll:I = 0x7f100295

.field public static final wrapper_messages:I = 0x7f10029e

.field public static final wrapper_messages_buttons:I = 0x7f1002a0

.field public static final wrong_mount_description:I = 0x7f1001f7

.field public static final wrong_mount_title:I = 0x7f1001f6

.field public static final x_slider:I = 0x7f100240

.field public static final you_will_need_the_mount_kit_description:I = 0x7f1001f4

.field public static final you_will_need_the_mount_kit_title:I = 0x7f1001f3

.field public static final zd_toolbar:I = 0x7f1003fe

.field public static final zd_toolbar_container:I = 0x7f1003fd

.field public static final zd_toolbar_shadow:I = 0x7f1003ff

.field public static final zoomLevelTextView:I = 0x7f10025e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
