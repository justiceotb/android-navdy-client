.class public final enum Lcom/navdy/service/library/events/audio/MusicCollectionType;
.super Ljava/lang/Enum;
.source "MusicCollectionType.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCollectionType;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final enum COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final enum COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final enum COLLECTION_TYPE_AUDIOBOOKS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final enum COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final enum COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final enum COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v1, "COLLECTION_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v1, "COLLECTION_TYPE_PLAYLISTS"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/audio/MusicCollectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v1, "COLLECTION_TYPE_ARTISTS"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/audio/MusicCollectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v1, "COLLECTION_TYPE_ALBUMS"

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/service/library/events/audio/MusicCollectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v1, "COLLECTION_TYPE_PODCASTS"

    invoke-direct {v0, v1, v8, v8}, Lcom/navdy/service/library/events/audio/MusicCollectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    const-string v1, "COLLECTION_TYPE_AUDIOBOOKS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicCollectionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_AUDIOBOOKS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PLAYLISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ARTISTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_ALBUMS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_PODCASTS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_AUDIOBOOKS:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicCollectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->value:I

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/MusicCollectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/MusicCollectionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->value:I

    return v0
.end method
