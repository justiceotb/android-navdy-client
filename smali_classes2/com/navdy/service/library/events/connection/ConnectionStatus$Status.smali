.class public final enum Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;
.super Ljava/lang/Enum;
.source "ConnectionStatus.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/ConnectionStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

.field public static final enum CONNECTION_FOUND:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

.field public static final enum CONNECTION_LOST:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

.field public static final enum CONNECTION_PAIRED_DEVICES_CHANGED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

.field public static final enum CONNECTION_PAIRING:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

.field public static final enum CONNECTION_SEARCH_FINISHED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

.field public static final enum CONNECTION_SEARCH_STARTED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 108
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const-string v1, "CONNECTION_PAIRING"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRING:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 112
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const-string v1, "CONNECTION_SEARCH_STARTED"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_STARTED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 116
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const-string v1, "CONNECTION_SEARCH_FINISHED"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_FINISHED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 120
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const-string v1, "CONNECTION_FOUND"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_FOUND:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 124
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const-string v1, "CONNECTION_LOST"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_LOST:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 128
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const-string v1, "CONNECTION_PAIRED_DEVICES_CHANGED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRED_DEVICES_CHANGED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    .line 103
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRING:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_STARTED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_SEARCH_FINISHED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_FOUND:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_LOST:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->CONNECTION_PAIRED_DEVICES_CHANGED:Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    aput-object v1, v0, v7

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->$VALUES:[Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 133
    iput p3, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->value:I

    .line 134
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 103
    const-class v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->$VALUES:[Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStatus$Status;->value:I

    return v0
.end method
