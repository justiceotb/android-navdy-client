.class public final Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisconnectRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/DisconnectRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/connection/DisconnectRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public forget:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/connection/DisconnectRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/connection/DisconnectRequest;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 59
    if-nez p1, :cond_0

    .line 61
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;->forget:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/connection/DisconnectRequest;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/connection/DisconnectRequest;-><init>(Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;Lcom/navdy/service/library/events/connection/DisconnectRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;->build()Lcom/navdy/service/library/events/connection/DisconnectRequest;

    move-result-object v0

    return-object v0
.end method

.method public forget(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;
    .locals 0
    .param p1, "forget"    # Ljava/lang/Boolean;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;->forget:Ljava/lang/Boolean;

    .line 69
    return-object p0
.end method
