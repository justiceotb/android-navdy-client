.class public final enum Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
.super Ljava/lang/Enum;
.source "DriverProfilePreferences.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FeatureMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

.field public static final enum FEATURE_MODE_BETA:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

.field public static final enum FEATURE_MODE_EXPERIMENTAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

.field public static final enum FEATURE_MODE_RELEASE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 519
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    const-string v1, "FEATURE_MODE_RELEASE"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_RELEASE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 520
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    const-string v1, "FEATURE_MODE_BETA"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_BETA:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 521
    new-instance v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    const-string v1, "FEATURE_MODE_EXPERIMENTAL"

    invoke-direct {v0, v1, v3, v5}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_EXPERIMENTAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 517
    new-array v0, v5, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_RELEASE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_BETA:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_EXPERIMENTAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 525
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 526
    iput p3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->value:I

    .line 527
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 517
    const-class v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
    .locals 1

    .prologue
    .line 517
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->$VALUES:[Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 531
    iget v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->value:I

    return v0
.end method
