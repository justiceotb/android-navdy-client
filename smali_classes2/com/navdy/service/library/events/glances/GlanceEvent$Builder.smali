.class public final Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GlanceEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/glances/GlanceEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/glances/GlanceEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public actions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;"
        }
    .end annotation
.end field

.field public glanceData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

.field public id:Ljava/lang/String;

.field public postTime:Ljava/lang/Long;

.field public provider:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 122
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/glances/GlanceEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/glances/GlanceEvent;

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 126
    if-nez p1, :cond_0

    .line 133
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 128
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;->provider:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider:Ljava/lang/String;

    .line 129
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id:Ljava/lang/String;

    .line 130
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;->postTime:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime:Ljava/lang/Long;

    .line 131
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;->glanceData:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/glances/GlanceEvent;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData:Ljava/util/List;

    .line 132
    iget-object v0, p1, Lcom/navdy/service/library/events/glances/GlanceEvent;->actions:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/glances/GlanceEvent;->access$100(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public actions(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;",
            ">;)",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceActions;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->actions:Ljava/util/List;

    .line 180
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/glances/GlanceEvent;
    .locals 2

    .prologue
    .line 185
    new-instance v0, Lcom/navdy/service/library/events/glances/GlanceEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/glances/GlanceEvent;-><init>(Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;Lcom/navdy/service/library/events/glances/GlanceEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->build()Lcom/navdy/service/library/events/glances/GlanceEvent;

    move-result-object v0

    return-object v0
.end method

.method public glanceData(Ljava/util/List;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;)",
            "Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;"
        }
    .end annotation

    .prologue
    .line 171
    .local p1, "glanceData":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceData:Ljava/util/List;

    .line 172
    return-object p0
.end method

.method public glanceType(Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .locals 0
    .param p1, "glanceType"    # Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->glanceType:Lcom/navdy/service/library/events/glances/GlanceEvent$GlanceType;

    .line 140
    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->id:Ljava/lang/String;

    .line 156
    return-object p0
.end method

.method public postTime(Ljava/lang/Long;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .locals 0
    .param p1, "postTime"    # Ljava/lang/Long;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->postTime:Ljava/lang/Long;

    .line 164
    return-object p0
.end method

.method public provider(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/service/library/events/glances/GlanceEvent$Builder;->provider:Ljava/lang/String;

    .line 148
    return-object p0
.end method
