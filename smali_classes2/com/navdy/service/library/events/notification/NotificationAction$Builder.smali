.class public final Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/notification/NotificationAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationAction;",
        ">;"
    }
.end annotation


# instance fields
.field public actionId:Ljava/lang/Integer;

.field public handler:Ljava/lang/String;

.field public iconResource:Ljava/lang/Integer;

.field public iconResourceFocused:Ljava/lang/Integer;

.field public id:Ljava/lang/Integer;

.field public label:Ljava/lang/String;

.field public labelId:Ljava/lang/Integer;

.field public notificationId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationAction;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/notification/NotificationAction;

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 146
    if-nez p1, :cond_0

    .line 155
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->handler:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->handler:Ljava/lang/String;

    .line 148
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->id:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->id:Ljava/lang/Integer;

    .line 149
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->notificationId:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->notificationId:Ljava/lang/Integer;

    .line 150
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->actionId:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->actionId:Ljava/lang/Integer;

    .line 151
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->labelId:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->labelId:Ljava/lang/Integer;

    .line 152
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->label:Ljava/lang/String;

    .line 153
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResource:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->iconResource:Ljava/lang/Integer;

    .line 154
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationAction;->iconResourceFocused:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->iconResourceFocused:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public actionId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "actionId"    # Ljava/lang/Integer;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->actionId:Ljava/lang/Integer;

    .line 187
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/notification/NotificationAction;
    .locals 2

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->checkRequiredFields()V

    .line 226
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationAction;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/notification/NotificationAction;-><init>(Lcom/navdy/service/library/events/notification/NotificationAction$Builder;Lcom/navdy/service/library/events/notification/NotificationAction$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationAction;

    move-result-object v0

    return-object v0
.end method

.method public handler(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "handler"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->handler:Ljava/lang/String;

    .line 162
    return-object p0
.end method

.method public iconResource(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "iconResource"    # Ljava/lang/Integer;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->iconResource:Ljava/lang/Integer;

    .line 212
    return-object p0
.end method

.method public iconResourceFocused(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "iconResourceFocused"    # Ljava/lang/Integer;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->iconResourceFocused:Ljava/lang/Integer;

    .line 220
    return-object p0
.end method

.method public id(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "id"    # Ljava/lang/Integer;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->id:Ljava/lang/Integer;

    .line 171
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->label:Ljava/lang/String;

    .line 203
    return-object p0
.end method

.method public labelId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "labelId"    # Ljava/lang/Integer;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->labelId:Ljava/lang/Integer;

    .line 195
    return-object p0
.end method

.method public notificationId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/notification/NotificationAction$Builder;
    .locals 0
    .param p1, "notificationId"    # Ljava/lang/Integer;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationAction$Builder;->notificationId:Ljava/lang/Integer;

    .line 179
    return-object p0
.end method
