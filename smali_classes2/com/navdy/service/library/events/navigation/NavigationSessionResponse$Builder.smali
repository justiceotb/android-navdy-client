.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationSessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public routeId:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 89
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 93
    if-nez p1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 95
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 96
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 97
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->routeId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->checkRequiredFields()V

    .line 135
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;

    move-result-object v0

    return-object v0
.end method

.method public pendingSessionState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;
    .locals 0
    .param p1, "pendingSessionState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 121
    return-object p0
.end method

.method public routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->routeId:Ljava/lang/String;

    .line 129
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 105
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 113
    return-object p0
.end method
