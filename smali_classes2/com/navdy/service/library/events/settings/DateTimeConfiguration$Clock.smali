.class public final enum Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;
.super Ljava/lang/Enum;
.source "DateTimeConfiguration.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/settings/DateTimeConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Clock"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

.field public static final enum CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

.field public static final enum CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 127
    new-instance v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    const-string v1, "CLOCK_24_HOUR"

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 131
    new-instance v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    const-string v1, "CLOCK_12_HOUR"

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 122
    new-array v0, v4, [Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    sget-object v1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_12_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    aput-object v1, v0, v2

    sput-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->$VALUES:[Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 136
    iput p3, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->value:I

    .line 137
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 122
    const-class v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->$VALUES:[Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->value:I

    return v0
.end method
