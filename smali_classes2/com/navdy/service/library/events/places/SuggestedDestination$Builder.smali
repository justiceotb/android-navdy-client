.class public final Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SuggestedDestination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/SuggestedDestination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/SuggestedDestination;",
        ">;"
    }
.end annotation


# instance fields
.field public destination:Lcom/navdy/service/library/events/destination/Destination;

.field public duration_traffic:Ljava/lang/Integer;

.field public type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/SuggestedDestination;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/SuggestedDestination;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 79
    if-nez p1, :cond_0

    .line 83
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 81
    iget-object v0, p1, Lcom/navdy/service/library/events/places/SuggestedDestination;->duration_traffic:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->duration_traffic:Ljava/lang/Integer;

    .line 82
    iget-object v0, p1, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/SuggestedDestination;
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->checkRequiredFields()V

    .line 109
    new-instance v0, Lcom/navdy/service/library/events/places/SuggestedDestination;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;Lcom/navdy/service/library/events/places/SuggestedDestination$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->build()Lcom/navdy/service/library/events/places/SuggestedDestination;

    move-result-object v0

    return-object v0
.end method

.method public destination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 87
    return-object p0
.end method

.method public duration_traffic(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;
    .locals 0
    .param p1, "duration_traffic"    # Ljava/lang/Integer;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->duration_traffic:Ljava/lang/Integer;

    .line 95
    return-object p0
.end method

.method public type(Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;
    .locals 0
    .param p1, "type"    # Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .line 103
    return-object p0
.end method
