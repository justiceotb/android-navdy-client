.class public final Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AutoCompleteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/AutoCompleteRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/AutoCompleteRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public maxResults:Ljava/lang/Integer;

.field public partialSearch:Ljava/lang/String;

.field public searchArea:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/AutoCompleteRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 81
    if-nez p1, :cond_0

    .line 85
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->partialSearch:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->partialSearch:Ljava/lang/String;

    .line 83
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->searchArea:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->searchArea:Ljava/lang/Integer;

    .line 84
    iget-object v0, p1, Lcom/navdy/service/library/events/places/AutoCompleteRequest;->maxResults:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->maxResults:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/AutoCompleteRequest;
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->checkRequiredFields()V

    .line 114
    new-instance v0, Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/AutoCompleteRequest;-><init>(Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;Lcom/navdy/service/library/events/places/AutoCompleteRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->build()Lcom/navdy/service/library/events/places/AutoCompleteRequest;

    move-result-object v0

    return-object v0
.end method

.method public maxResults(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;
    .locals 0
    .param p1, "maxResults"    # Ljava/lang/Integer;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->maxResults:Ljava/lang/Integer;

    .line 108
    return-object p0
.end method

.method public partialSearch(Ljava/lang/String;)Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;
    .locals 0
    .param p1, "partialSearch"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->partialSearch:Ljava/lang/String;

    .line 92
    return-object p0
.end method

.method public searchArea(Ljava/lang/Integer;)Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;
    .locals 0
    .param p1, "searchArea"    # Ljava/lang/Integer;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/navdy/service/library/events/places/AutoCompleteRequest$Builder;->searchArea:Ljava/lang/Integer;

    .line 100
    return-object p0
.end method
