.class public final Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PlacesSearchResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/PlacesSearchResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Ljava/lang/String;

.field public category:Ljava/lang/String;

.field public destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

.field public distance:Ljava/lang/Double;

.field public label:Ljava/lang/String;

.field public navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 108
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchResult;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 112
    if-nez p1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->label:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;->address:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->address:Ljava/lang/String;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;->category:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->category:Ljava/lang/String;

    .line 118
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->distance:Ljava/lang/Double;

    goto :goto_0
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->address:Ljava/lang/String;

    .line 150
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/places/PlacesSearchResult;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/PlacesSearchResult;-><init>(Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;Lcom/navdy/service/library/events/places/PlacesSearchResult$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchResult;

    move-result-object v0

    return-object v0
.end method

.method public category(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    .locals 0
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->category:Ljava/lang/String;

    .line 158
    return-object p0
.end method

.method public destinationLocation(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    .locals 0
    .param p1, "destinationLocation"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->destinationLocation:Lcom/navdy/service/library/events/location/Coordinate;

    .line 142
    return-object p0
.end method

.method public distance(Ljava/lang/Double;)Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    .locals 0
    .param p1, "distance"    # Ljava/lang/Double;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->distance:Ljava/lang/Double;

    .line 166
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->label:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public navigationPosition(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;
    .locals 0
    .param p1, "navigationPosition"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResult$Builder;->navigationPosition:Lcom/navdy/service/library/events/location/Coordinate;

    .line 134
    return-object p0
.end method
