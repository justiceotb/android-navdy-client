.class public final Lcom/navdy/service/library/events/ui/ShowScreen$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ShowScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/ui/ShowScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/ui/ShowScreen;",
        ">;"
    }
.end annotation


# instance fields
.field public arguments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public screen:Lcom/navdy/service/library/events/ui/Screen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/ui/ShowScreen;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/ui/ShowScreen;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 67
    if-nez p1, :cond_0

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    iput-object v0, p0, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen:Lcom/navdy/service/library/events/ui/Screen;

    .line 69
    iget-object v0, p1, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/ui/ShowScreen;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->arguments:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public arguments(Ljava/util/List;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;)",
            "Lcom/navdy/service/library/events/ui/ShowScreen$Builder;"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "arguments":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->arguments:Ljava/util/List;

    .line 79
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/ui/ShowScreen;
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->checkRequiredFields()V

    .line 85
    new-instance v0, Lcom/navdy/service/library/events/ui/ShowScreen;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/ui/ShowScreen;-><init>(Lcom/navdy/service/library/events/ui/ShowScreen$Builder;Lcom/navdy/service/library/events/ui/ShowScreen$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v0

    return-object v0
.end method

.method public screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;
    .locals 0
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen:Lcom/navdy/service/library/events/ui/Screen;

    .line 74
    return-object p0
.end method
