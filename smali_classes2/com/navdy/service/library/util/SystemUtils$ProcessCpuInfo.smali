.class public Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;
.super Ljava/lang/Object;
.source "SystemUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/util/SystemUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProcessCpuInfo"
.end annotation


# instance fields
.field private cpu:I

.field private name:Ljava/lang/String;

.field private pid:I

.field private thread:Ljava/lang/String;

.field private tid:I


# direct methods
.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "tid"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "thread"    # Ljava/lang/String;
    .param p5, "cpu"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->pid:I

    .line 32
    iput p2, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->tid:I

    .line 33
    iput-object p3, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->name:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->thread:Ljava/lang/String;

    .line 35
    iput p5, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->cpu:I

    .line 36
    return-void
.end method


# virtual methods
.method public getCpu()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->cpu:I

    return v0
.end method

.method public getPid()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->pid:I

    return v0
.end method

.method public getProcessName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getThreadName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->thread:Ljava/lang/String;

    return-object v0
.end method

.method public getTid()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/navdy/service/library/util/SystemUtils$ProcessCpuInfo;->tid:I

    return v0
.end method
