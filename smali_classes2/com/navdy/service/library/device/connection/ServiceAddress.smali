.class public Lcom/navdy/service/library/device/connection/ServiceAddress;
.super Ljava/lang/Object;
.source "ServiceAddress.java"


# instance fields
.field private final address:Ljava/lang/String;

.field private final protocol:Ljava/lang/String;

.field private final service:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "device"    # Ljava/lang/String;
    .param p2, "service"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->address:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->service:Ljava/lang/String;

    .line 14
    const-string v0, "NA"

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->protocol:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "device"    # Ljava/lang/String;
    .param p2, "service"    # Ljava/lang/String;
    .param p3, "protocol"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->address:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->service:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->protocol:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 34
    if-ne p0, p1, :cond_1

    const/4 v1, 0x1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 35
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/navdy/service/library/device/connection/ServiceAddress;

    .line 39
    .local v0, "that":Lcom/navdy/service/library/device/connection/ServiceAddress;
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->address:Ljava/lang/String;

    iget-object v3, v0, Lcom/navdy/service/library/device/connection/ServiceAddress;->address:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 40
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->service:Ljava/lang/String;

    iget-object v3, v0, Lcom/navdy/service/library/device/connection/ServiceAddress;->service:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->protocol:Ljava/lang/String;

    iget-object v2, v0, Lcom/navdy/service/library/device/connection/ServiceAddress;->protocol:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getService()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->service:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 47
    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->address:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 48
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->service:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 49
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->protocol:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 50
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->address:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->service:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/service/library/device/connection/ServiceAddress;->protocol:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
