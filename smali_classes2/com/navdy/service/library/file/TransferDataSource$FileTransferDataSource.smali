.class Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;
.super Lcom/navdy/service/library/file/TransferDataSource;
.source "TransferDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/file/TransferDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileTransferDataSource"
.end annotation


# instance fields
.field mFile:Ljava/io/File;


# direct methods
.method constructor <init>(Ljava/io/File;)V
    .locals 0
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/service/library/file/TransferDataSource;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mFile:Ljava/io/File;

    .line 32
    return-void
.end method


# virtual methods
.method public checkSum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mFile:Ljava/io/File;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->hashForFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public checkSum(J)Ljava/lang/String;
    .locals 1
    .param p1, "length"    # J

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mFile:Ljava/io/File;

    invoke-static {v0, p1, p2}, Lcom/navdy/service/library/util/IOUtils;->hashForFile(Ljava/io/File;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mFile:Ljava/io/File;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public read([B)I
    .locals 6
    .param p1, "b"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    .local v0, "f":Ljava/io/RandomAccessFile;
    const/4 v2, -0x1

    .line 41
    .local v2, "res":I
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v4, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mFile:Ljava/io/File;

    const-string v5, "r"

    invoke-direct {v1, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    .end local v0    # "f":Ljava/io/RandomAccessFile;
    .local v1, "f":Ljava/io/RandomAccessFile;
    :try_start_1
    iget-wide v4, p0, Lcom/navdy/service/library/file/TransferDataSource$FileTransferDataSource;->mCurOffset:J

    invoke-virtual {v1, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 43
    invoke-virtual {v1, p1}, Ljava/io/RandomAccessFile;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .line 45
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move-object v0, v1

    .end local v1    # "f":Ljava/io/RandomAccessFile;
    .restart local v0    # "f":Ljava/io/RandomAccessFile;
    move v3, v2

    .line 46
    .end local v2    # "res":I
    .local v3, "res":I
    :goto_0
    return v3

    .line 45
    .end local v3    # "res":I
    .restart local v2    # "res":I
    :catchall_0
    move-exception v4

    :goto_1
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    move v3, v2

    .line 46
    .end local v2    # "res":I
    .restart local v3    # "res":I
    goto :goto_0

    .line 45
    .end local v0    # "f":Ljava/io/RandomAccessFile;
    .end local v3    # "res":I
    .restart local v1    # "f":Ljava/io/RandomAccessFile;
    .restart local v2    # "res":I
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "f":Ljava/io/RandomAccessFile;
    .restart local v0    # "f":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method
