.class public Lcom/navdy/service/library/network/TCPSocketFactory;
.super Ljava/lang/Object;
.source "TCPSocketFactory.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketFactory;


# instance fields
.field private final host:Ljava/lang/String;

.field private final port:I


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/ServiceAddress;)V
    .locals 2
    .param p1, "address"    # Lcom/navdy/service/library/device/connection/ServiceAddress;

    .prologue
    .line 20
    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getService()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/network/TCPSocketFactory;-><init>(Ljava/lang/String;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/navdy/service/library/network/TCPSocketFactory;->host:Ljava/lang/String;

    .line 16
    iput p2, p0, Lcom/navdy/service/library/network/TCPSocketFactory;->port:I

    .line 17
    return-void
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/network/SocketAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/navdy/service/library/network/TCPSocketAdapter;

    iget-object v1, p0, Lcom/navdy/service/library/network/TCPSocketFactory;->host:Ljava/lang/String;

    iget v2, p0, Lcom/navdy/service/library/network/TCPSocketFactory;->port:I

    invoke-direct {v0, v1, v2}, Lcom/navdy/service/library/network/TCPSocketAdapter;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method
