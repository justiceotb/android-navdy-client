.class public Lcom/nimbusds/jose/crypto/ECDSASigner;
.super Lcom/nimbusds/jose/crypto/ECDSAProvider;
.source "ECDSASigner.java"

# interfaces
.implements Lcom/nimbusds/jose/JWSSigner;


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# instance fields
.field private final privateKey:Ljava/security/interfaces/ECPrivateKey;


# direct methods
.method public constructor <init>(Lcom/nimbusds/jose/jwk/ECKey;)V
    .locals 2
    .param p1, "ecJWK"    # Lcom/nimbusds/jose/jwk/ECKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/ECKey;->getCurve()Lcom/nimbusds/jose/jwk/ECKey$Curve;

    move-result-object v0

    invoke-static {v0}, Lcom/nimbusds/jose/crypto/ECDSA;->resolveAlgorithm(Lcom/nimbusds/jose/jwk/ECKey$Curve;)Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nimbusds/jose/crypto/ECDSAProvider;-><init>(Lcom/nimbusds/jose/JWSAlgorithm;)V

    .line 78
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/ECKey;->isPrivate()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Lcom/nimbusds/jose/JOSEException;

    const-string v1, "The EC JWK doesn\'t contain a private part"

    invoke-direct {v0, v1}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    invoke-virtual {p1}, Lcom/nimbusds/jose/jwk/ECKey;->toECPrivateKey()Ljava/security/interfaces/ECPrivateKey;

    move-result-object v0

    iput-object v0, p0, Lcom/nimbusds/jose/crypto/ECDSASigner;->privateKey:Ljava/security/interfaces/ECPrivateKey;

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/ECPrivateKey;)V
    .locals 1
    .param p1, "privateKey"    # Ljava/security/interfaces/ECPrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {p1}, Lcom/nimbusds/jose/crypto/ECDSA;->resolveAlgorithm(Ljava/security/interfaces/ECKey;)Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/nimbusds/jose/crypto/ECDSAProvider;-><init>(Lcom/nimbusds/jose/JWSAlgorithm;)V

    .line 58
    iput-object p1, p0, Lcom/nimbusds/jose/crypto/ECDSASigner;->privateKey:Ljava/security/interfaces/ECPrivateKey;

    .line 59
    return-void
.end method


# virtual methods
.method public bridge synthetic getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/ECDSAProvider;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v0

    return-object v0
.end method

.method public getPrivateKey()Ljava/security/interfaces/ECPrivateKey;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/ECDSASigner;->privateKey:Ljava/security/interfaces/ECPrivateKey;

    return-object v0
.end method

.method public sign(Lcom/nimbusds/jose/JWSHeader;[B)Lcom/nimbusds/jose/util/Base64URL;
    .locals 8
    .param p1, "header"    # Lcom/nimbusds/jose/JWSHeader;
    .param p2, "signingInput"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSHeader;->getAlgorithm()Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v0

    .line 103
    .local v0, "alg":Lcom/nimbusds/jose/JWSAlgorithm;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDSASigner;->supportedJWSAlgorithms()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 104
    new-instance v6, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDSASigner;->supportedJWSAlgorithms()Ljava/util/Set;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/nimbusds/jose/crypto/AlgorithmSupportMessage;->unsupportedJWSAlgorithm(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 112
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDSASigner;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v6

    invoke-virtual {v6}, Lcom/nimbusds/jose/jca/JCAContext;->getProvider()Ljava/security/Provider;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/nimbusds/jose/crypto/ECDSA;->getSignerAndVerifier(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object v1

    .line 113
    .local v1, "dsa":Ljava/security/Signature;
    iget-object v6, p0, Lcom/nimbusds/jose/crypto/ECDSASigner;->privateKey:Ljava/security/interfaces/ECPrivateKey;

    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ECDSASigner;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v7

    invoke-virtual {v7}, Lcom/nimbusds/jose/jca/JCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;Ljava/security/SecureRandom;)V

    .line 114
    invoke-virtual {v1, p2}, Ljava/security/Signature;->update([B)V

    .line 115
    invoke-virtual {v1}, Ljava/security/Signature;->sign()[B
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 122
    .local v3, "jcaSignature":[B
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWSHeader;->getAlgorithm()Lcom/nimbusds/jose/JWSAlgorithm;

    move-result-object v6

    invoke-static {v6}, Lcom/nimbusds/jose/crypto/ECDSA;->getSignatureByteArrayLength(Lcom/nimbusds/jose/JWSAlgorithm;)I

    move-result v5

    .line 123
    .local v5, "rsByteArrayLength":I
    invoke-static {v3, v5}, Lcom/nimbusds/jose/crypto/ECDSA;->transcodeSignatureToConcat([BI)[B

    move-result-object v4

    .line 124
    .local v4, "jwsSignature":[B
    invoke-static {v4}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v6

    return-object v6

    .line 117
    .end local v1    # "dsa":Ljava/security/Signature;
    .end local v3    # "jcaSignature":[B
    .end local v4    # "jwsSignature":[B
    .end local v5    # "rsByteArrayLength":I
    :catch_0
    move-exception v2

    .line 119
    .local v2, "e":Ljava/security/GeneralSecurityException;
    :goto_0
    new-instance v6, Lcom/nimbusds/jose/JOSEException;

    invoke-virtual {v2}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 117
    .end local v2    # "e":Ljava/security/GeneralSecurityException;
    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public bridge synthetic supportedJWSAlgorithms()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/ECDSAProvider;->supportedJWSAlgorithms()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
