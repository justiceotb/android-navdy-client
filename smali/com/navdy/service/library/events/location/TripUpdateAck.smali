.class public final Lcom/navdy/service/library/events/location/TripUpdateAck;
.super Lcom/squareup/wire/Message;
.source "TripUpdateAck.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_SEQUENCE_NUMBER:Ljava/lang/Integer;

.field public static final DEFAULT_TRIP_NUMBER:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final sequence_number:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final trip_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/TripUpdateAck;->DEFAULT_TRIP_NUMBER:Ljava/lang/Long;

    .line 21
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/location/TripUpdateAck;->DEFAULT_SEQUENCE_NUMBER:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;

    .prologue
    .line 41
    iget-object v0, p1, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->trip_number:Ljava/lang/Long;

    iget-object v1, p1, Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;->sequence_number:Ljava/lang/Integer;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/location/TripUpdateAck;-><init>(Ljava/lang/Long;Ljava/lang/Integer;)V

    .line 42
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/location/TripUpdateAck;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;Lcom/navdy/service/library/events/location/TripUpdateAck$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/location/TripUpdateAck$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/location/TripUpdateAck;-><init>(Lcom/navdy/service/library/events/location/TripUpdateAck$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "trip_number"    # Ljava/lang/Long;
    .param p2, "sequence_number"    # Ljava/lang/Integer;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->trip_number:Ljava/lang/Long;

    .line 37
    iput-object p2, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->sequence_number:Ljava/lang/Integer;

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    if-ne p1, p0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/location/TripUpdateAck;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 49
    check-cast v0, Lcom/navdy/service/library/events/location/TripUpdateAck;

    .line 50
    .local v0, "o":Lcom/navdy/service/library/events/location/TripUpdateAck;
    iget-object v3, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->trip_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/TripUpdateAck;->trip_number:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/TripUpdateAck;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->sequence_number:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/TripUpdateAck;->sequence_number:Ljava/lang/Integer;

    .line 51
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/location/TripUpdateAck;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 56
    iget v0, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->hashCode:I

    .line 57
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 58
    iget-object v2, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->trip_number:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->trip_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 59
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->sequence_number:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->sequence_number:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 60
    iput v0, p0, Lcom/navdy/service/library/events/location/TripUpdateAck;->hashCode:I

    .line 62
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 58
    goto :goto_0
.end method
