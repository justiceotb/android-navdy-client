.class public final Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;
.super Lcom/squareup/wire/Message;
.source "PhotoUpdatesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

.field public static final DEFAULT_START:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final photoType:Lcom/navdy/service/library/events/photo/PhotoType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final start:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->DEFAULT_START:Ljava/lang/Boolean;

    .line 16
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;

    .prologue
    .line 36
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->start:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;-><init>(Ljava/lang/Boolean;Lcom/navdy/service/library/events/photo/PhotoType;)V

    .line 37
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/navdy/service/library/events/photo/PhotoType;)V
    .locals 0
    .param p1, "start"    # Ljava/lang/Boolean;
    .param p2, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    .line 32
    iput-object p2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 33
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v1

    .line 43
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    .line 45
    .local v0, "o":Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 46
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51
    iget v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->hashCode:I

    .line 52
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 53
    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 54
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/photo/PhotoType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 55
    iput v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->hashCode:I

    .line 57
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 53
    goto :goto_0
.end method
