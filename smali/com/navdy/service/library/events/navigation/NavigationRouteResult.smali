.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
.super Lcom/squareup/wire/Message;
.source "NavigationRouteResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_DURATION:Ljava/lang/Integer;

.field public static final DEFAULT_DURATION_TRAFFIC:Ljava/lang/Integer;

.field public static final DEFAULT_GEOPOINTS_OBSOLETE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_LENGTH:Ljava/lang/Integer;

.field public static final DEFAULT_ROUTEID:Ljava/lang/String; = ""

.field public static final DEFAULT_ROUTELATLONGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_VIA:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final duration:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final duration_traffic:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final geoPoints_OBSOLETE:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/location/Coordinate;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final length:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final routeId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final routeLatLongs:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->PACKED:Lcom/squareup/wire/Message$Label;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public final via:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->DEFAULT_GEOPOINTS_OBSOLETE:Ljava/util/List;

    .line 24
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->DEFAULT_LENGTH:Ljava/lang/Integer;

    .line 25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->DEFAULT_DURATION:Ljava/lang/Integer;

    .line 26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->DEFAULT_DURATION_TRAFFIC:Ljava/lang/Integer;

    .line 27
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->DEFAULT_ROUTELATLONGS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;)V
    .locals 10
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;

    .prologue
    .line 100
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->routeId:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->label:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->geoPoints_OBSOLETE:Ljava/util/List;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->length:Ljava/lang/Integer;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration:Ljava/lang/Integer;

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration_traffic:Ljava/lang/Integer;

    iget-object v7, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->routeLatLongs:Ljava/util/List;

    iget-object v8, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->via:Ljava/lang/String;

    iget-object v9, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->address:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 102
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteResult$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "routeId"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p4, "length"    # Ljava/lang/Integer;
    .param p5, "duration"    # Ljava/lang/Integer;
    .param p6, "duration_traffic"    # Ljava/lang/Integer;
    .param p8, "via"    # Ljava/lang/String;
    .param p9, "address"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 87
    .local p3, "geoPoints_OBSOLETE":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    .local p7, "routeLatLongs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    .line 90
    invoke-static {p3}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->geoPoints_OBSOLETE:Ljava/util/List;

    .line 91
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    .line 92
    iput-object p5, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    .line 93
    iput-object p6, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    .line 94
    invoke-static {p7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    .line 95
    iput-object p8, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    .line 96
    iput-object p9, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    .line 97
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    if-ne p1, p0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v1

    .line 107
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 108
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 109
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    .line 110
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->geoPoints_OBSOLETE:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->geoPoints_OBSOLETE:Ljava/util/List;

    .line 111
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    .line 112
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    .line 113
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    .line 114
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    .line 115
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    .line 116
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    .line 117
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 122
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->hashCode:I

    .line 123
    .local v0, "result":I
    if-nez v0, :cond_2

    .line 124
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 125
    :goto_0
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 126
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->geoPoints_OBSOLETE:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->geoPoints_OBSOLETE:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 127
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 128
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v4, v2

    .line 129
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v4, v2

    .line 130
    mul-int/lit8 v2, v0, 0x25

    iget-object v4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->hashCode()I

    move-result v3

    :cond_0
    add-int v0, v2, v3

    .line 131
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 132
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int v0, v2, v1

    .line 133
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->hashCode:I

    .line 135
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 124
    goto :goto_0

    :cond_4
    move v2, v1

    .line 125
    goto :goto_1

    :cond_5
    move v2, v3

    .line 126
    goto :goto_2

    :cond_6
    move v2, v1

    .line 127
    goto :goto_3

    :cond_7
    move v2, v1

    .line 128
    goto :goto_4

    :cond_8
    move v2, v1

    .line 129
    goto :goto_5

    :cond_9
    move v2, v1

    .line 131
    goto :goto_6
.end method
