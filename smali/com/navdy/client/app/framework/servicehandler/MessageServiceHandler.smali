.class public Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;
.super Ljava/lang/Object;
.source "MessageServiceHandler.java"


# static fields
.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 22
    return-void
.end method


# virtual methods
.method public onSmsMessageRequest(Lcom/navdy/service/library/events/messaging/SmsMessageRequest;)V
    .locals 8
    .param p1, "request"    # Lcom/navdy/service/library/events/messaging/SmsMessageRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 28
    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[message] sending sms"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 29
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iget-object v1, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->message:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 30
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "[message] sent sms"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v6

    .line 33
    .local v6, "t":Ljava/lang/Throwable;
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 34
    new-instance v3, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;

    sget-object v4, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->number:Ljava/lang/String;

    move-object v2, v0

    :goto_1
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->name:Ljava/lang/String;

    move-object v1, v0

    :goto_2
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/navdy/service/library/events/messaging/SmsMessageRequest;->id:Ljava/lang/String;

    :goto_3
    invoke-direct {v3, v4, v2, v1, v0}, Lcom/navdy/service/library/events/messaging/SmsMessageResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0

    :cond_0
    move-object v2, v7

    goto :goto_1

    :cond_1
    move-object v1, v7

    goto :goto_2

    :cond_2
    move-object v0, v7

    goto :goto_3
.end method
