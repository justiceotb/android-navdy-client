.class public final Lcom/navdy/client/app/framework/location/NavdyLocationManager;
.super Ljava/lang/Object;
.source "NavdyLocationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/location/NavdyLocationManager$CarLocationChangedEvent;,
        Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;
    }
.end annotation


# static fields
.field private static final COUNTRY_CODE_REFRESH_RATE:J

.field private static final FAKE_CAR_LOCATION_ACTION:Ljava/lang/String; = "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION"

.field private static final FAKE_CAR_LOCATION_ACTION_CANCEL:Ljava/lang/String; = "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION_CANCEL"

.field private static final FAKE_PHONE_LOCATION_ACTION:Ljava/lang/String; = "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION"

.field private static final FAKE_PHONE_LOCATION_ACTION_CANCEL:Ljava/lang/String; = "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION_CANCEL"

.field private static final LATITUDE_PREFERENCE:Ljava/lang/String; = "latitude"

.field private static final LOCATION_ACCURACY_PREFERENCE:Ljava/lang/String; = "accuracy"

.field private static final LOCATION_PROVIDER_PREFERENCE:Ljava/lang/String; = "provider"

.field private static final LOCATION_TIME_PREFERENCE:Ljava/lang/String; = "time"

.field private static final LONGITUDE_PREFERENCE:Ljava/lang/String; = "longitude"

.field private static final MAX_AGE_LOCATION_FIX:J = 0x7530L

.field private static final MIN_DISTANCE_LOCATION_CHANGE:F = 25.0f

.field private static final MIN_TIME_LOCATION_CHANGE:I = 0x1388

.field private static final NAVDY_LOCATION_MANAGER_PREFS:Ljava/lang/String; = "navdy_location_manager"

.field private static final SMART_START_LOCATION_LIMIT:I = 0x1f4

.field private static instance:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private static lastKnownCountryCode:Ljava/lang/String;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final navdyLocationManagerLock:Ljava/lang/Object;

.field private static nextCountryCodeUpdate:J


# instance fields
.field private bestProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private currentCarLocation:Landroid/location/Location;

.field private currentPhoneLocation:Landroid/location/Location;

.field private didProperlyInitialize:Z

.field private final fakeLocationReceiver:Landroid/content/BroadcastReceiver;

.field private isInjectingFakeCarLocations:Z

.field private isInjectingFakePhoneLocations:Z

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private listening:Z

.field private final locationListener:Landroid/location/LocationListener;

.field private locationManager:Landroid/location/LocationManager;

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 58
    const-string v0, "US"

    sput-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->lastKnownCountryCode:Ljava/lang/String;

    .line 59
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->nextCountryCodeUpdate:J

    .line 61
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->COUNTRY_CODE_REFRESH_RATE:J

    .line 76
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 97
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    new-instance v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$1;-><init>(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationListener:Landroid/location/LocationListener;

    .line 161
    new-instance v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$2;-><init>(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->fakeLocationReceiver:Landroid/content/BroadcastReceiver;

    .line 231
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 233
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listeners:Ljava/util/List;

    .line 234
    const-string v1, "navdy_location_manager"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->preferences:Landroid/content/SharedPreferences;

    .line 235
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->didProperlyInitialize:Z

    .line 236
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isInjectingFakePhoneLocations:Z

    .line 237
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isInjectingFakeCarLocations:Z

    .line 239
    invoke-direct {p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getLastTripUpdateCoordinates()V

    .line 241
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->initLocationServices()V

    .line 242
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 244
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->registerFakeLocationReceiver(Landroid/content/Context;)V

    .line 245
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isInjectingFakePhoneLocations:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)D
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getLocationScore(Landroid/location/Location;)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isInjectingFakePhoneLocations:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isValidLocation(Landroid/location/Location;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 51
    sput-object p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->lastKnownCountryCode:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1302(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 51
    sput-wide p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->nextCountryCodeUpdate:J

    return-wide p0
.end method

.method static synthetic access$1400()J
    .locals 2

    .prologue
    .line 51
    sget-wide v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->COUNTRY_CODE_REFRESH_RATE:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isRecentAndAccurateEnough(Landroid/location/Location;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)Landroid/location/Location;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->saveLocationToPreferences(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->callListenersForPhoneLocation(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic access$702(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isInjectingFakeCarLocations:Z

    return p1
.end method

.method static synthetic access$802(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentCarLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$900(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .param p1, "x1"    # Landroid/location/Location;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->callListenersForCarLocation(Landroid/location/Location;)V

    return-void
.end method

.method private callListenersForCarLocation(Landroid/location/Location;)V
    .locals 5
    .param p1, "carLocation"    # Landroid/location/Location;

    .prologue
    .line 583
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCoordsFromLocation(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 585
    .local v0, "carCoords":Lcom/navdy/service/library/events/location/Coordinate;
    if-nez v0, :cond_1

    .line 596
    :cond_0
    return-void

    .line 589
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 590
    .local v2, "listenerWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    .line 592
    .local v1, "listener":Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;
    if-eqz v1, :cond_2

    .line 593
    invoke-interface {v1, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;->onCarLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V

    goto :goto_0
.end method

.method private callListenersForPhoneLocation(Landroid/location/Location;)V
    .locals 6
    .param p1, "phoneLocation"    # Landroid/location/Location;

    .prologue
    .line 566
    sget-object v3, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "phone location has changed to"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", calling listeners"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 567
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCoordsFromLocation(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 569
    .local v2, "phoneCoords":Lcom/navdy/service/library/events/location/Coordinate;
    if-nez v2, :cond_1

    .line 580
    :cond_0
    return-void

    .line 573
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 574
    .local v1, "listenerWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    .line 576
    .local v0, "listener":Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;
    if-eqz v0, :cond_2

    .line 577
    invoke-interface {v0, v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;->onPhoneLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V

    goto :goto_0
.end method

.method public static forceLastKnownCountryCodeUpdate()V
    .locals 2

    .prologue
    .line 663
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->nextCountryCodeUpdate:J

    .line 664
    return-void
.end method

.method private getBestAvailableLocationProviders()Ljava/util/List;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 508
    new-instance v1, Landroid/location/Criteria;

    invoke-direct {v1}, Landroid/location/Criteria;-><init>()V

    .line 509
    .local v1, "criteria":Landroid/location/Criteria;
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Landroid/location/Criteria;->setAccuracy(I)V

    .line 510
    invoke-virtual {v1, v6}, Landroid/location/Criteria;->setPowerRequirement(I)V

    .line 511
    invoke-virtual {v1, v5}, Landroid/location/Criteria;->setAltitudeRequired(Z)V

    .line 512
    invoke-virtual {v1, v5}, Landroid/location/Criteria;->setBearingRequired(Z)V

    .line 513
    invoke-virtual {v1, v6}, Landroid/location/Criteria;->setCostAllowed(Z)V

    .line 514
    invoke-virtual {v1, v5}, Landroid/location/Criteria;->setSpeedRequired(Z)V

    .line 517
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 518
    .local v0, "context":Landroid/content/Context;
    const-string v4, "location"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 519
    .local v2, "locationManager":Landroid/location/LocationManager;
    const/4 v3, 0x0

    .line 520
    .local v3, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 521
    invoke-virtual {v2, v1, v6}, Landroid/location/LocationManager;->getProviders(Landroid/location/Criteria;Z)Ljava/util/List;

    move-result-object v3

    .line 524
    :cond_0
    if-nez v3, :cond_1

    .line 525
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 528
    .restart local v3    # "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 529
    const-string v4, "passive"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    :cond_2
    return-object v3
.end method

.method private getCarLocation()Landroid/location/Location;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 411
    sget-object v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 412
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentCarLocation:Landroid/location/Location;

    monitor-exit v1

    return-object v0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getCoordsFromLocation(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;
    .locals 4
    .param p1, "location"    # Landroid/location/Location;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 536
    if-nez p1, :cond_0

    .line 537
    const/4 v0, 0x0

    .line 539
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/app/framework/map/MapUtils;->buildNewCoordinate(DD)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;
    .locals 2

    .prologue
    .line 102
    sget-object v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 103
    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->instance:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->instance:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 106
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->instance:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    monitor-exit v1

    return-object v0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getLastKnownCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 628
    sget-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->lastKnownCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method private getLastTripUpdateCoordinates()V
    .locals 3

    .prologue
    .line 457
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$4;-><init>(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 499
    return-void
.end method

.method private getLocationScore(Landroid/location/Location;)D
    .locals 6
    .param p1, "location"    # Landroid/location/Location;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 624
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method private getPhoneLocation()Landroid/location/Location;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 404
    sget-object v1, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 405
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    monitor-exit v1

    return-object v0

    .line 406
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private isRecentAndAccurateEnough(Landroid/location/Location;)Z
    .locals 12
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const-wide/16 v10, 0x7530

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 543
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Landroid/location/Location;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 562
    :cond_0
    :goto_0
    return v7

    .line 546
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 547
    .local v4, "now":J
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    sub-long v8, v4, v8

    cmp-long v8, v8, v10

    if-gez v8, :cond_2

    move v3, v6

    .line 548
    .local v3, "newLocationIsRecent":Z
    :goto_1
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v8

    const/high16 v9, 0x43fa0000    # 500.0f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_3

    move v2, v6

    .line 550
    .local v2, "newLocationIsAccurate":Z
    :goto_2
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 551
    iget-object v8, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    if-nez v8, :cond_4

    move v7, v6

    .line 552
    goto :goto_0

    .end local v2    # "newLocationIsAccurate":Z
    .end local v3    # "newLocationIsRecent":Z
    :cond_2
    move v3, v7

    .line 547
    goto :goto_1

    .restart local v3    # "newLocationIsRecent":Z
    :cond_3
    move v2, v7

    .line 548
    goto :goto_2

    .line 555
    .restart local v2    # "newLocationIsAccurate":Z
    :cond_4
    iget-object v8, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    invoke-virtual {v8}, Landroid/location/Location;->getAccuracy()F

    move-result v8

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_6

    move v1, v6

    .line 556
    .local v1, "isMoreAccurateThanCurrentLocation":Z
    :goto_3
    iget-object v8, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    invoke-virtual {v8}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    sub-long v8, v4, v8

    cmp-long v8, v8, v10

    if-lez v8, :cond_7

    move v0, v6

    .line 558
    .local v0, "currentLocationIsTooOld":Z
    :goto_4
    if-nez v1, :cond_5

    if-eqz v0, :cond_0

    :cond_5
    move v7, v6

    .line 559
    goto :goto_0

    .end local v0    # "currentLocationIsTooOld":Z
    .end local v1    # "isMoreAccurateThanCurrentLocation":Z
    :cond_6
    move v1, v7

    .line 555
    goto :goto_3

    .restart local v1    # "isMoreAccurateThanCurrentLocation":Z
    :cond_7
    move v0, v7

    .line 556
    goto :goto_4
.end method

.method private isValidLocation(Landroid/location/Location;)Z
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const-wide/16 v2, 0x0

    .line 502
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private registerFakeLocationReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 609
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 610
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 611
    const-string v1, "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION_CANCEL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 612
    const-string v1, "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 613
    const-string v1, "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION_CANCEL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 615
    iget-object v1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->fakeLocationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 616
    return-void
.end method

.method private saveLocationToPreferences(Landroid/location/Location;)V
    .locals 4
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 599
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "latitude"

    .line 600
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "longitude"

    .line 601
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "time"

    .line 602
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "provider"

    .line 603
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accuracy"

    .line 604
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 605
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 606
    return-void
.end method

.method public static updateLastKnownCountryCode(Landroid/location/Location;)V
    .locals 8
    .param p0, "currentLocation"    # Landroid/location/Location;

    .prologue
    .line 637
    if-nez p0, :cond_1

    .line 657
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 641
    .local v2, "now":J
    sget-wide v4, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->nextCountryCodeUpdate:J

    cmp-long v4, v2, v4

    if-ltz v4, :cond_0

    .line 645
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 646
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 647
    .local v1, "latlng":Lcom/google/android/gms/maps/model/LatLng;
    new-instance v4, Lcom/navdy/client/app/framework/location/NavdyLocationManager$5;

    invoke-direct {v4, v0, v2, v3}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$5;-><init>(Lcom/navdy/client/app/framework/models/Destination;J)V

    invoke-static {v1, v0, v4}, Lcom/navdy/client/app/framework/map/MapUtils;->doReverseGeocodingFor(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V
    .locals 11
    .param p1, "navdyLocationListener"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 306
    sget-object v9, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v9

    .line 307
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listeners:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 309
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listening:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 311
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    .line 313
    .local v7, "context":Landroid/content/Context;
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v7, v0}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationManager:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->bestProviders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 318
    .local v1, "provider":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationManager:Landroid/location/LocationManager;

    const-wide/16 v2, 0x1388

    const/high16 v4, 0x41c80000    # 25.0f

    iget-object v5, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationListener:Landroid/location/LocationListener;

    .line 319
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 318
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 324
    .end local v1    # "provider":Ljava/lang/String;
    .end local v7    # "context":Landroid/content/Context;
    :catch_0
    move-exception v8

    .line 325
    .local v8, "t":Ljava/lang/Throwable;
    :try_start_2
    sget-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 328
    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_0
    :goto_1
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 329
    return-void

    .line 321
    .restart local v7    # "context":Landroid/content/Context;
    :cond_1
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listening:Z

    .line 322
    sget-object v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "location listener installed["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->bestProviders:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationListener:Landroid/location/LocationListener;

    invoke-static {v3}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 328
    .end local v7    # "context":Landroid/content/Context;
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0
.end method

.method public getCarCoordinates()Lcom/navdy/service/library/events/location/Coordinate;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 428
    invoke-direct {p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCarLocation()Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCoordsFromLocation(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getPhoneCoordinates()Lcom/navdy/service/library/events/location/Coordinate;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 423
    invoke-direct {p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getPhoneLocation()Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCoordsFromLocation(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartLocation()Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getCoordsFromLocation(Landroid/location/Location;)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getSmartStartLocation()Landroid/location/Location;
    .locals 6
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 366
    sget-object v3, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v3

    .line 367
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentCarLocation:Landroid/location/Location;

    .line 368
    .local v0, "carLocation":Landroid/location/Location;
    iget-object v1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    .line 369
    .local v1, "phoneLocation":Landroid/location/Location;
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 373
    sget-object v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Both car and phone location are null :\'("

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 374
    const/4 v0, 0x0

    .line 398
    .end local v0    # "carLocation":Landroid/location/Location;
    :goto_0
    return-object v0

    .line 369
    .end local v1    # "phoneLocation":Landroid/location/Location;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 378
    .restart local v0    # "carLocation":Landroid/location/Location;
    .restart local v1    # "phoneLocation":Landroid/location/Location;
    :cond_0
    if-nez v0, :cond_1

    .line 379
    sget-object v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Using phone location because car location is null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move-object v0, v1

    .line 380
    goto :goto_0

    .line 382
    :cond_1
    if-nez v1, :cond_2

    .line 383
    sget-object v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Using car location because phone location is null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 388
    :cond_2
    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Landroid/location/Location;Landroid/location/Location;)F

    move-result v2

    const/high16 v3, 0x43fa0000    # 500.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 389
    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 390
    sget-object v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Using phone location because car and phone are more than 500m apart and phone location is the most recent"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    move-object v0, v1

    .line 391
    goto :goto_0

    .line 393
    :cond_3
    sget-object v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Using car location because car and phone are more than 500m apart and car location is the most recent"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 397
    :cond_4
    sget-object v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Using car location because the car and the phone are close to each other."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initLocationServices()V
    .locals 18

    .prologue
    .line 248
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 249
    .local v4, "context":Landroid/content/Context;
    const-string v11, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v4, v11}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v11

    if-nez v11, :cond_1

    const/4 v5, 0x1

    .line 252
    .local v5, "hasPermissions":Z
    :goto_0
    sget-object v14, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v14

    .line 253
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->didProperlyInitialize:Z

    if-nez v11, :cond_4

    if-eqz v5, :cond_4

    .line 254
    const-string v11, "location"

    invoke-virtual {v4, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/location/LocationManager;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationManager:Landroid/location/LocationManager;

    .line 255
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getBestAvailableLocationProviders()Ljava/util/List;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->bestProviders:Ljava/util/List;

    .line 257
    new-instance v3, Ljava/util/ArrayList;

    const/4 v11, 0x3

    invoke-direct {v3, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 259
    .local v3, "candidateLocations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationManager:Landroid/location/LocationManager;

    if-eqz v11, :cond_2

    .line 260
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->bestProviders:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 261
    .local v9, "provider":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationManager:Landroid/location/LocationManager;

    invoke-virtual {v15, v9}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v6

    .line 263
    .local v6, "lastKnownLocation":Landroid/location/Location;
    if-eqz v6, :cond_0

    .line 264
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 302
    .end local v3    # "candidateLocations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    .end local v6    # "lastKnownLocation":Landroid/location/Location;
    .end local v9    # "provider":Ljava/lang/String;
    :catchall_0
    move-exception v11

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v11

    .line 249
    .end local v5    # "hasPermissions":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 271
    .restart local v3    # "candidateLocations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    .restart local v5    # "hasPermissions":Z
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->preferences:Landroid/content/SharedPreferences;

    const-string v15, "provider"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-interface {v11, v15, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 273
    .restart local v9    # "provider":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->preferences:Landroid/content/SharedPreferences;

    const-string v15, "latitude"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v11, v15, v0}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v7

    .line 274
    .local v7, "lat":F
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->preferences:Landroid/content/SharedPreferences;

    const-string v15, "longitude"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v11, v15, v0}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v8

    .line 275
    .local v8, "lng":F
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->preferences:Landroid/content/SharedPreferences;

    const-string v15, "time"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v11, v15, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 276
    .local v12, "time":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->preferences:Landroid/content/SharedPreferences;

    const-string v15, "accuracy"

    const v16, 0x7f7fffff    # Float.MAX_VALUE

    move/from16 v0, v16

    invoke-interface {v11, v15, v0}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v2

    .line 278
    .local v2, "accuracy":F
    new-instance v10, Landroid/location/Location;

    invoke-direct {v10, v9}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 279
    .local v10, "savedLocation":Landroid/location/Location;
    float-to-double v0, v7

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v10, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 280
    float-to-double v0, v8

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v10, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 281
    invoke-virtual {v10, v12, v13}, Landroid/location/Location;->setTime(J)V

    .line 282
    invoke-virtual {v10, v2}, Landroid/location/Location;->setAccuracy(F)V

    .line 284
    sget-object v11, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "initLocationServices, taken from prefs: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 285
    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    new-instance v11, Lcom/navdy/client/app/framework/location/NavdyLocationManager$3;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$3;-><init>(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)V

    invoke-static {v3, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 294
    const/4 v11, 0x0

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/location/Location;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    .line 295
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->saveLocationToPreferences(Landroid/location/Location;)V

    .line 297
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->didProperlyInitialize:Z

    .line 298
    sget-object v11, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Properly initialized, currentPhoneLocation="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentPhoneLocation:Landroid/location/Location;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11, v15}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 302
    .end local v2    # "accuracy":F
    .end local v3    # "candidateLocations":Ljava/util/List;, "Ljava/util/List<Landroid/location/Location;>;"
    .end local v7    # "lat":F
    .end local v8    # "lng":F
    .end local v9    # "provider":Ljava/lang/String;
    .end local v10    # "savedLocation":Landroid/location/Location;
    .end local v12    # "time":J
    :cond_3
    :goto_2
    monitor-exit v14

    .line 303
    return-void

    .line 299
    :cond_4
    if-nez v5, :cond_3

    .line 300
    sget-object v11, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v15, "Error with location permissions!"

    invoke-virtual {v11, v15}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public onCarLocationChanged(Lcom/navdy/client/app/framework/location/NavdyLocationManager$CarLocationChangedEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager$CarLocationChangedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 433
    sget-object v3, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v3

    .line 434
    :try_start_0
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isInjectingFakeCarLocations:Z

    if-eqz v2, :cond_1

    .line 435
    monitor-exit v3

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    iget-object v1, p1, Lcom/navdy/client/app/framework/location/NavdyLocationManager$CarLocationChangedEvent;->coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    .line 441
    .local v1, "coords":Lcom/navdy/service/library/events/location/Coordinate;
    new-instance v0, Landroid/location/Location;

    const-string v2, "gps"

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 442
    .local v0, "carLocation":Landroid/location/Location;
    iget-object v2, v1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 443
    iget-object v2, v1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 444
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 446
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->isValidLocation(Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 447
    sget-object v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new car location: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 448
    sget-object v3, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v3

    .line 449
    :try_start_1
    iput-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentCarLocation:Landroid/location/Location;

    .line 450
    iget-object v2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->currentCarLocation:Landroid/location/Location;

    invoke-static {v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->updateLastKnownCountryCode(Landroid/location/Location;)V

    .line 451
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 452
    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->callListenersForCarLocation(Landroid/location/Location;)V

    goto :goto_0

    .line 437
    .end local v0    # "carLocation":Landroid/location/Location;
    .end local v1    # "coords":Lcom/navdy/service/library/events/location/Coordinate;
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 451
    .restart local v0    # "carLocation":Landroid/location/Location;
    .restart local v1    # "coords":Lcom/navdy/service/library/events/location/Coordinate;
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method public removeListener(Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;)V
    .locals 8
    .param p1, "navdyLocationListener"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    .prologue
    .line 332
    sget-object v5, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->navdyLocationManagerLock:Ljava/lang/Object;

    monitor-enter v5

    .line 333
    :try_start_0
    iget-object v4, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v1

    .line 335
    .local v1, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 336
    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;

    .line 338
    .local v2, "listener":Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;
    if-eqz v2, :cond_1

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 339
    :cond_1
    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 358
    .end local v1    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;>;>;"
    .end local v2    # "listener":Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 343
    .restart local v1    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/location/NavdyLocationManager$OnNavdyLocationChangedListener;>;>;"
    :cond_2
    :try_start_1
    iget-boolean v4, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listening:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listeners:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_4

    .line 344
    sget-object v4, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "stopListeningForLocationChange"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347
    :try_start_2
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 348
    .local v0, "context":Landroid/content/Context;
    const-string v4, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v4}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_3

    .line 350
    iget-object v4, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationManager:Landroid/location/LocationManager;

    iget-object v6, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationListener:Landroid/location/LocationListener;

    invoke-virtual {v4, v6}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 351
    sget-object v4, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "location listener removed:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->locationListener:Landroid/location/LocationListener;

    invoke-static {v7}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 356
    .end local v0    # "context":Landroid/content/Context;
    :cond_3
    :goto_1
    const/4 v4, 0x0

    :try_start_3
    iput-boolean v4, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->listening:Z

    .line 358
    :cond_4
    monitor-exit v5

    .line 359
    return-void

    .line 353
    :catch_0
    move-exception v3

    .line 354
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
