.class Lcom/navdy/client/app/NavdyApplication$2;
.super Ljava/lang/Object;
.source "NavdyApplication.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/NavdyApplication;->registerActivityLifecycleCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/NavdyApplication;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/NavdyApplication;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/NavdyApplication;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/navdy/client/app/NavdyApplication$2;->this$0:Lcom/navdy/client/app/NavdyApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 229
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 275
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 252
    new-instance v0, Lcom/navdy/client/app/NavdyApplication$2$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/NavdyApplication$2$2;-><init>(Lcom/navdy/client/app/NavdyApplication$2;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 263
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 237
    new-instance v0, Lcom/navdy/client/app/NavdyApplication$2$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/NavdyApplication$2$1;-><init>(Lcom/navdy/client/app/NavdyApplication$2;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 248
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 271
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 233
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 267
    return-void
.end method
