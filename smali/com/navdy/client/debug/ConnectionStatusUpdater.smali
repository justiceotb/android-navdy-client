.class Lcom/navdy/client/debug/ConnectionStatusUpdater;
.super Ljava/lang/Object;
.source "ConnectionStatusUpdater.java"


# instance fields
.field mConnectionIndicator:Lcom/navdy/client/debug/view/ConnectionStateIndicator;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100226
    .end annotation
.end field

.field mTextViewCurrentDevice:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100227
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "parentView"    # Landroid/view/View;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 31
    return-void
.end method

.method private updateView()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->getInstance()Lcom/navdy/client/app/framework/DeviceConnection;

    move-result-object v0

    .line 45
    .local v0, "deviceConnection":Lcom/navdy/client/app/framework/DeviceConnection;
    if-eqz v0, :cond_0

    .line 46
    iget-object v1, p0, Lcom/navdy/client/debug/ConnectionStatusUpdater;->mConnectionIndicator:Lcom/navdy/client/debug/view/ConnectionStateIndicator;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->setConnectionStatus(Lcom/navdy/service/library/device/connection/Connection$Status;)V

    .line 47
    iget-object v1, p0, Lcom/navdy/client/debug/ConnectionStatusUpdater;->mConnectionIndicator:Lcom/navdy/client/debug/view/ConnectionStateIndicator;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->getConnectionType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->setConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 48
    iget-object v1, p0, Lcom/navdy/client/debug/ConnectionStatusUpdater;->mTextViewCurrentDevice:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current device: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/client/debug/devicepicker/Device;->prettyName(Lcom/navdy/service/library/device/connection/ConnectionInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    :cond_0
    return-void
.end method


# virtual methods
.method public onDeviceConnectFailureEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectionFailedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectionFailedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->updateView()V

    .line 60
    return-void
.end method

.method public onDeviceConnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->updateView()V

    .line 65
    return-void
.end method

.method public onDeviceConnectingEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectingEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectingEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->updateView()V

    .line 55
    return-void
.end method

.method public onDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->updateView()V

    .line 70
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 40
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 35
    invoke-direct {p0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->updateView()V

    .line 36
    return-void
.end method
