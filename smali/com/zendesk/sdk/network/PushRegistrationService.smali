.class public interface abstract Lcom/zendesk/sdk/network/PushRegistrationService;
.super Ljava/lang/Object;
.source "PushRegistrationService.java"


# virtual methods
.method public abstract registerDevice(Ljava/lang/String;Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;)Lretrofit2/Call;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Lcom/zendesk/sdk/model/push/PushRegistrationResponseWrapper;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/mobile/push_notification_devices.json"
    .end annotation
.end method

.method public abstract unregisterDevice(Ljava/lang/String;Ljava/lang/String;)Lretrofit2/Call;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "id"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lretrofit2/Call",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/DELETE;
        value = "/api/mobile/push_notification_devices/{id}.json"
    .end annotation
.end method
